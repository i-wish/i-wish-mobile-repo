package com.iwish.ws;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Date;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import com.iwish.dto.UserDTO;



public class webServiceConnection {

	final private String webServiceUrl = "http://10.145.31.18080:80/IWish-Enterprise-Application-ws/rest/login?accessToken",accessToken;
	private String response = "";
	String ipAddress;

	
	public webServiceConnection(String accessToken) {
		
		this.accessToken=accessToken;
		connectWebService();

	}

	
	
		
	 public String connectWebService(){
		
		DefaultHttpClient httpClient = new DefaultHttpClient();
		HttpPost httpPost = new HttpPost(webServiceUrl+accessToken );
		HttpResponse httpResponse;
		try {
			httpResponse = httpClient.execute(httpPost);

			HttpEntity httpEntity = httpResponse.getEntity();
			InputStream is;

			is = httpEntity.getContent();
			
			BufferedReader input = new BufferedReader(new InputStreamReader(is));
			String line = null;

			while ((line = input.readLine()) != null) {
				
				response+=line;
			}

			input.close();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return response.toString();
	}
	
	public UserDTO getUserInfo(){
		
		UserDTO userDTO=new UserDTO();
		try {
			
			JSONObject json=new JSONObject(response);
			userDTO.setFirstName(json.getString("firstName"));
			userDTO.setLastName(json.getString("lastName"));
			userDTO.setEmail(json.getString("email"));	
			Date birthDate=new Date(json.getString("birthday"));
			
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return userDTO;
		
	}
}