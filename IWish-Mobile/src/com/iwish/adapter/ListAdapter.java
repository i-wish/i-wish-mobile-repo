package com.iwish.adapter;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.renderscript.RenderScript.RSErrorHandler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView.FindListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.iwish.activities.R;

import com.iwish.controller.WishlistController;
import com.iwish.dto.FriendReservedProduct;
import com.iwish.dto.ProductDTO;
import com.iwish.dto.ProductReserveDTO;
import com.iwish.dto.UserDTO;
import com.iwish.dto.UserReserveProduct;
import com.iwish.fragments.ReserveProductFragment;
import com.iwish.util.DataUtil;

public class ListAdapter extends ArrayAdapter<ProductDTO> {

	private Context context;
	private List<ProductDTO> productDTOs;
	private ProductDTO product;
	private boolean userdata = false;
	private UserDTO user; // gift owner
	private UserDTO friend; // gift reserver
	private ProductDTO productToReserve;
	private View view;
	private ArrayList<FriendReservedProduct> friendReservedProducts;
	private boolean userDidReserve = false;
	private boolean userReserveWithOthers = false;

	private boolean userCanceled = false;
	private boolean reserved = false;
	private boolean userConfirmed = false;
	ListBean list = null;
	int buttonIndex;
	ProgressDialog Pdialog;
	Button cancel;
	

	public ListAdapter(Context context, int resource, int textViewResourceId,
			List<ProductDTO> objects) {
		super(context, resource, textViewResourceId, objects);
		this.context = context;
		productDTOs = (ArrayList<ProductDTO>) objects;

	}

	public ListAdapter(Context context, int resource, int textViewResourceId,
			List<ProductDTO> objects, boolean userdata) {
		super(context, resource, textViewResourceId, objects);
		this.context = context;
		productDTOs = (ArrayList<ProductDTO>) objects;
		this.userdata = userdata;
	}

	public ListAdapter(Context context, int resource, int textViewResourceId,
			List<ProductDTO> objects, boolean userdata, UserDTO user) {
		super(context, resource, textViewResourceId, objects);
		this.context = context;
		productDTOs = (ArrayList<ProductDTO>) objects;
		this.userdata = userdata;
		this.user = user;
	}

	//
	public ListAdapter(Context context, int resource, int textViewResourceId,
			List<ProductDTO> objects,
			List<FriendReservedProduct> friendReservedProducts,
			boolean userdata, UserDTO user, UserDTO friend) {
		super(context, resource, textViewResourceId, objects);
		this.context = context;
		productDTOs = (ArrayList<ProductDTO>) objects;
		this.userdata = userdata;
		this.user = user;
		this.friend = friend;
		this.friendReservedProducts = (ArrayList<FriendReservedProduct>) friendReservedProducts;
	}

	public int getCount() {
		return productDTOs.size();
	}

	public ProductDTO getItem(int position) {
		return productDTOs.get(position);
	}

	public long getItemId(int position) {
		return productDTOs.get(position).getId();
	}

	public void appendMoreData(List<ProductDTO> productDTOs2) {
		this.productDTOs.addAll(productDTOs2);
		notifyDataSetChanged();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		final int productIndex = position;
		final ViewGroup parentView = parent;

		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.friend_wishlist_item,
					parent, false);
			list = new ListBean(convertView);
			list.name = (TextView) convertView
					.findViewById(R.id.wishlist_item_name);
			list.image = (ImageView) convertView
					.findViewById(R.id.wishlist_item_image);
			list.btnReserve = (Button) convertView
					.findViewById(R.id.reserve_alone);
			list.btnReserveWithOthers = (Button) convertView
					.findViewById(R.id.reserve_with_friends);
			list.btnCancelReserve = (Button) convertView
					.findViewById(R.id.cancel_reserve);
			list.btnReserved = (Button) convertView.findViewById(R.id.reserved);
			convertView.setTag(list);

		} else {
			list = (ListBean) convertView.getTag();
		}

		list.name.setText(productDTOs.get(position).getName());
		list.name.setTextColor(Color.BLACK);

		if (productDTOs.get(position).getImage() != null) {
			Bitmap bitmap = BitmapFactory.decodeByteArray(
					productDTOs.get(position).getImage(), 0,
					productDTOs.get(position).getImage().length);
			list.image.setImageBitmap(bitmap);
		}

		Button reserve = list.btnReserve;
		reserve.setVisibility(convertView.VISIBLE);
		Button reserve_with_others = list.btnReserveWithOthers;

		list.btnCancelReserve.setVisibility(View.GONE);
		list.btnReserve.setVisibility(View.GONE);
		list.btnReserved.setVisibility(View.GONE);
		list.btnReserveWithOthers.setVisibility(View.GONE);

		if (!userdata && friendReservedProducts != null) {
			for (FriendReservedProduct friendReservedProduct : friendReservedProducts) {
				if (productDTOs.get(position).getId() == friendReservedProduct
						.getProduct_id()) {
					Log.i("MMMMMMMMMM",
							"" + friendReservedProduct.getParticipatePercent()
									+ "    "
									+ productDTOs.get(position).getName());
					// if another friend reserved whole product .. make button
					// gift reserved .. if I reserved product make button
					// cancel.
					userDidReserve = false;
					userReserveWithOthers = false;
					for (UserDTO userDTO : friendReservedProduct.getUserDTOs()) {
						// if user reserved this product .. enable cancel button
						// and remove other buttons
						if (userDTO.getName().equals(friend.getName())) {
							Log.i("*********", "   **** you reserved  ");
							userDidReserve = true;
						}

					}

					if (friendReservedProduct.getParticipatePercent() == 0.0) {
						// if there is a friend participating in the gift
						// disable reserve alone
						reserve.setVisibility(View.VISIBLE);
						reserve.setEnabled(true);
						reserve_with_others.setVisibility(View.VISIBLE);
						reserve_with_others.setEnabled(true);
						list.btnCancelReserve.setVisibility(View.GONE);
						list.btnReserved.setVisibility(View.GONE);

					} else if (Math.abs(friendReservedProduct
							.getParticipatePercent()) >= 100) {

						Log.i("*********", "   **** participate = 100  ");
						reserve.setVisibility(View.GONE);
						reserve.setEnabled(false);
						reserve_with_others.setVisibility(View.GONE);
						reserve_with_others.setEnabled(false);
						if (userDidReserve) {
							Log.i("*********",
									"   **** you reserved whole product  ");
							list.btnCancelReserve.setVisibility(View.VISIBLE);
						} else {
							Log.i("*********",
									"   **** someone reserved whole product ");
							list.btnReserved.setVisibility(View.VISIBLE);
							list.btnReserved.setEnabled(false);
						}
					} else {
						userReserveWithOthers = true;
						reserve.setVisibility(View.GONE);
						reserve.setEnabled(false);
						if (userDidReserve) {
							Log.i("*********", "   **** you reserved  ");
							reserve_with_others.setVisibility(View.GONE);
							reserve_with_others.setEnabled(false);
							list.btnCancelReserve.setVisibility(View.VISIBLE);
						} else {
							Log.i("*********", "   **** you didn't reserve   ");
							reserve_with_others.setVisibility(View.VISIBLE);
							reserve_with_others.setEnabled(true);
						}
					}
				}
			}
		}

		if (userdata) {
			reserve.setVisibility(View.VISIBLE);
//			Drawable drawable = context.getResources().getDrawable(
//					R.drawable.gift_received_selector);
//			reserve.setBackgroundDrawable(drawable);
			reserve.setText(context.getString(R.string.gift_received));
			if (userConfirmed){
				reserve.setEnabled(false);
			} else {
				reserve.setEnabled(true);
			}
		}

		// change buttons in run time in listview
		if (position == buttonIndex) {
			if (reserved && !userCanceled) {
				list.btnReserve.setVisibility(View.GONE);
				list.btnReserveWithOthers.setVisibility(View.GONE);
				list.btnCancelReserve.setVisibility(View.VISIBLE);
			} else if (!reserved && userCanceled) {
				list.btnCancelReserve.setVisibility(View.GONE);
				list.btnReserveWithOthers.setVisibility(View.VISIBLE);
				if (!userReserveWithOthers) {
					list.btnReserve.setVisibility(View.VISIBLE);
				}
			}
		}
		
		

		reserve.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				view = v;
				product = productDTOs.get(productIndex);
				Log.i("product", " @@@@@@@@@@@@ " + product.getName());
				Log.i("user", "################ " + user.getName());
				if (userdata) {
					Pdialog = ProgressDialog.show(((Activity)context),"", context.getString(R.string.receiving_gift));
					Thread th = new Thread(new GiftReceivedConfirmation());
					th.start();
				} else {
					Pdialog = ProgressDialog.show(((Activity)context),"", context.getString(R.string.reserving));
					Thread th = new Thread(new ReserveGiftThread());
					th.start();
				}
			}
		});

		reserve_with_others.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				
				product = productDTOs.get(productIndex);
				if (!userdata) {
//					Pdialog = ProgressDialog.show(((Activity)context),"", "reserving ...");
					Thread th = new Thread(new FriendsReserveForProductThread());
					th.start();
				}
			}
		});

		list.btnCancelReserve.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Pdialog = ProgressDialog.show(((Activity)context),"", context.getString(R.string.cancel_reserve_in_progress));
				product = productDTOs.get(productIndex);
				Thread th = new Thread(new CancelReservation());
				th.start();
			}
		});

		return convertView;
	}

	static class ListBean {

		View rowView;
		public TextView name;
		public ImageView image;
		public Button btnReserve;
		public Button btnReserveWithOthers;
		public Button btnCancelReserve;
		public Button btnReserved;

		ListBean(View rowView) {
			this.rowView = rowView;
		}
	}

	class ReserveGiftThread implements Runnable {

		@Override
		public void run() {

			WishlistController wishlistController = new WishlistController(
					context);
			final boolean result = wishlistController.reserveGift(product,
					user, friend);

			((Activity) context).runOnUiThread(new Runnable() {

				@Override
				public void run() {
					Pdialog.dismiss();
					if (result) {
						if (view.getId() == R.id.reserve) {
							view.setEnabled(false);
						}
						// list.btnReserve.setVisibility(View.GONE);
						// list.btnReserveWithOthers.setVisibility(View.GONE);
						// list.btnCancelReserve.setVisibility(View.VISIBLE);
						
						reserved = true;
						userCanceled = false;
						buttonIndex = productDTOs.indexOf(product);
						Toast.makeText(context, context.getString(R.string.successful), Toast.LENGTH_LONG)
								.show();
						notifyDataSetChanged();
					}
				}
			});
		}

	}

	class GiftReceivedConfirmation implements Runnable {

		@Override
		public void run() {
			// Log.i("user", user.getId() + "   p = " + product.getId());
			WishlistController wishlistController = new WishlistController(
					context);
			boolean result = wishlistController.confirmGiftReceived(user,
					product);
			if (result) {
				((Activity) context).runOnUiThread(new Runnable() {

					@Override
					public void run() {
						Pdialog.dismiss();
						Toast.makeText(context, context.getString(R.string.received), Toast.LENGTH_LONG).show();
						
					//	productDTOs.remove(product);
						userConfirmed = true;
						notifyDataSetChanged();
					}
				});
			}
		}

	}

	class FriendsReserveForProductThread implements Runnable {

		@Override
		public void run() {
			WishlistController wishlistController = new WishlistController(
					context);
			final ArrayList<UserReserveProduct> userReserveProducts = wishlistController
					.getFriendsReservedProduct(user, product);

			((Activity) context).runOnUiThread(new Runnable() {

				@Override
				public void run() {

					Intent i = ((Activity) context).getIntent();
					i.putExtra(DataUtil.UserReserveProduct, userReserveProducts);
					i.putExtra(DataUtil.PRODUCT_TO_RESERVE, product);
					i.putExtra(DataUtil.USER, user);
					i.putExtra(DataUtil.FRIEND, friend);
					ReserveProductFragment reserveProductFragment = new ReserveProductFragment();
					((Activity) context)
							.getFragmentManager()
							.beginTransaction()
							.replace(R.id.frame_container,
									reserveProductFragment)
							.addToBackStack("reserveFrag").commit();
				}

			});
		}
	}

	class CancelReservation implements Runnable {

		boolean result;

		@Override
		public void run() {
			WishlistController wishlistController = new WishlistController(
					context);
			result = wishlistController
					.cancelReservation(user, product, friend);

			((Activity) context).runOnUiThread(new Runnable() {

				@Override
				public void run() {
					Pdialog.dismiss();
					if (result) {
						Toast.makeText(context, context.getString(R.string.reservation_canceled),
								Toast.LENGTH_LONG).show();

						reserved = false;
						userCanceled = true;
						buttonIndex = productDTOs.indexOf(product);
						// list.btnReserveWithOthers.setVisibility(View.VISIBLE);
						// if (!userReserveWithOthers){
						// list.btnReserve.setVisibility(View.VISIBLE);
						// }
						ArrayList<ProductDTO> temp = new ArrayList<ProductDTO>();
						temp.addAll(productDTOs);
						productDTOs.clear();
						productDTOs.addAll(temp);
						notifyDataSetChanged();
					}
				}
			});
		}

	}
}
