package com.iwish.adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.iwish.activities.R;
import com.iwish.dto.UserDTO;

public class FriendsAdapter extends ArrayAdapter<UserDTO> implements Filterable {

	Context context;
	ArrayList<UserDTO> friends;
	Filter filter;

	public FriendsAdapter(Context context, int resource,
			int textViewResourceId, List<UserDTO> objects) {
		super(context, resource, textViewResourceId, objects);
		this.context = context;
		this.friends = (ArrayList<UserDTO>) objects;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ListBean list = null;
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater
					.inflate(R.layout.friends_item, parent, false);
			list = new ListBean(convertView);
			convertView.setTag(list);

		} else {
			list = (ListBean) convertView.getTag();
		}

		TextView productName = (TextView) convertView
				.findViewById(R.id.friend_name);
		ImageView productImage = (ImageView) convertView
				.findViewById(R.id.friend_image);
		Log.i("#######", "" + friends.get(position).getName());
		productName.setText(friends.get(position).getName());
		productName.setTextColor(Color.BLACK);

		if (friends.get(position).getPicture() != null) {
			Bitmap bitmap = BitmapFactory
					.decodeByteArray(friends.get(position).getPicture(), 0,
							friends.get(position).getPicture().length);
			productImage.setImageBitmap(bitmap);
		}
		return convertView;
	}

	public class ListBean {

		View rowView;

		ListBean(View rowView) {
			this.rowView = rowView;
		}
	}

	
}
