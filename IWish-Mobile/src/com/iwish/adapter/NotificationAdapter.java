package com.iwish.adapter;

import java.util.ArrayList;
import java.util.List;

import com.iwish.activities.R;
import com.iwish.adapter.CategoryAdapter.ViewHolder;
import com.iwish.dto.NotificationDTO;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class NotificationAdapter extends ArrayAdapter<NotificationDTO> {

	ArrayList<NotificationDTO> notifications;
	Context context;

	public NotificationAdapter(Context context, int resource,
			int textViewResourceId, List<NotificationDTO> objects) {
		super(context, resource, textViewResourceId, objects);
		notifications = (ArrayList<NotificationDTO>) objects;
		this.context = context;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ViewHolder viewHolder = null;

		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.notification_item, parent,
					false);
			viewHolder = new ViewHolder(convertView);
			viewHolder.notName = (TextView) convertView
					.findViewById(R.id.notification_name);
			viewHolder.notImage = (ImageView) convertView
					.findViewById(R.id.notification_image);
			viewHolder.notTime = (TextView) convertView
					.findViewById(R.id.notification_time);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		viewHolder.notName.setText(notifications.get(position).getTitle());
		viewHolder.notName.setTextColor(Color.BLACK);
		if (notifications.get(position).getReceiveTime() != null)
			viewHolder.notTime.setText(notifications.get(position)
					.getReceiveTime().toString());
		else 
			viewHolder.notTime.setText("");
		if (notifications.get(position).getPicture() != null) {
			Bitmap bitmap = BitmapFactory.decodeByteArray(
					notifications.get(position).getPicture(), 0, notifications
							.get(position).getPicture().length);
			viewHolder.notImage.setImageBitmap(bitmap);
		}
		if (notifications.get(position).isDelivered()) {
			convertView.setBackgroundResource(R.drawable.notification_seen_bg);
		}
		return convertView;
	}

	static class ViewHolder {

		View rootView;
		public TextView notName;
		public ImageView notImage;
		public TextView notTime;

		ViewHolder(View rootView) {
			this.rootView = rootView;
		}

	}

}
