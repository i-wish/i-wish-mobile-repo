package com.iwish.adapter;

import java.util.ArrayList;
import java.util.List;

import com.iwish.activities.HomeActivity;
import com.iwish.activities.R;
import com.iwish.dto.CategoryDTO;
import com.iwish.fragments.CompaniesFragment;
import com.iwish.util.DataUtil;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class CategoryAdapter extends ArrayAdapter<CategoryDTO> {

	ArrayList<CategoryDTO> categoryDTOs;
	Context context;
	CategoryDTO categoryDTO;

	public CategoryAdapter(Context context, int resource,
			int textViewResourceId, List<CategoryDTO> objects) {
		super(context, resource, textViewResourceId, objects);
		this.categoryDTOs = (ArrayList<CategoryDTO>) objects;
		this.context = context;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ViewHolder viewHolder = null;
		final int productIndex = position;
		
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.category_grid_layout,
					parent, false);
			viewHolder = new ViewHolder(convertView);
			viewHolder.catName = (TextView) convertView
					.findViewById(R.id.category_name);
			viewHolder.catImage = (ImageView) convertView
					.findViewById(R.id.category_image);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		viewHolder.catName.setText(categoryDTOs.get(position).getName());
		viewHolder.catName.setTextColor(Color.BLACK);

		if (categoryDTOs.get(position).getImage() != null) {
			Bitmap bitmap = BitmapFactory.decodeByteArray(
					categoryDTOs.get(position).getImage(), 0,
					categoryDTOs.get(position).getImage().length);
			viewHolder.catImage.setImageBitmap(bitmap);
			Drawable drawable = context.getResources().getDrawable(R.drawable.categories_selector);
			viewHolder.catImage.setBackgroundDrawable(drawable);
		}
		
		
		viewHolder.catImage.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				categoryDTO = categoryDTOs.get(productIndex);
				Intent i = ((Activity)context).getIntent();
				i.putExtra(DataUtil.CATEGORYDTO, categoryDTO);
				CompaniesFragment companiesFragment = new CompaniesFragment();
				((Activity)context)
						.getFragmentManager()
						.beginTransaction()
						.replace(R.id.frame_container, companiesFragment,
								DataUtil.CATEGORY_FRAGMENT_TAG)
						.addToBackStack("category fragment").commit();
			}
		});
			
		return convertView;
	}

	static class ViewHolder {

		View rootView;
		public TextView catName;
		public ImageView catImage;

		ViewHolder(View rootView) {
			this.rootView = rootView;
		}

	}

}
