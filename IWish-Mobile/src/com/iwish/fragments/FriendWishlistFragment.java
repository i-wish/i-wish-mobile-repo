package com.iwish.fragments;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.iwish.activities.HomeActivity;
import com.iwish.activities.R;
import com.iwish.adapter.ListAdapter;
import com.iwish.controller.FriendController;
import com.iwish.controller.WishlistController;
import com.iwish.dto.FriendReservedProduct;
import com.iwish.dto.NotificationDTO;
import com.iwish.dto.ProductDTO;
import com.iwish.dto.UserDTO;
import com.iwish.dto.UserReserveProduct;
import com.iwish.util.CommonUtil;
import com.iwish.util.DataUtil;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

public class FriendWishlistFragment extends Fragment {

	View layout;
	ListView wishlistListView;
	
	List<ProductDTO> userWishList;
	ArrayList<FriendReservedProduct> friendReservedProducts;
	String email;
	ListAdapter adapter;
	UserDTO friend;
	ProductDTO product;
	ArrayList<UserReserveProduct> userReserveProducts;
	LinearLayout noWishes;
	UserDTO userDTO;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		userWishList = new ArrayList<ProductDTO>();
		friendReservedProducts = new ArrayList<FriendReservedProduct>();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		layout = inflater.inflate(R.layout.friend_wishlist_fragment, container,
				false);
		wishlistListView = (ListView) layout
				.findViewById(R.id.friend_wishlist_list);

		noWishes = (LinearLayout) layout
				.findViewById(R.id.no_friend_wishes_layout);
		
		Intent i = getActivity().getIntent();
		if (i.getSerializableExtra(DataUtil.FRIENDSELECTED) != null) {
			friend = (UserDTO) i.getSerializableExtra(DataUtil.FRIENDSELECTED);
		}
		((HomeActivity)getActivity()).title.setText(friend.getName());
		if (userWishList.size() == 0 && friendReservedProducts.size() == 0) {
			if(CommonUtil.checkNetworkConnection(getActivity())){
			FriendWishListTask friendWishListTask = new FriendWishListTask();
			friendWishListTask.execute();
			}else{
				CommonUtil.showConnectionToast(FriendWishlistFragment.this, inflater, container, savedInstanceState);
			}
		}else{
		wishlistListView.setAdapter(new ListAdapter(getActivity(),
				R.layout.product_cell, R.id.product_name, userWishList,
				friendReservedProducts, false, friend, userDTO));
		}

		wishlistListView
				.setOnItemClickListener(new AdapterView.OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> parent, View view,
							int position, long id) {
						product = userWishList.get(position);
						GetFriendsReservedProduct getFriendsReservedProduct = new GetFriendsReservedProduct();
						getFriendsReservedProduct.execute();

					}
				});

		return layout;
	}

	class FriendWishListTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			((HomeActivity) HomeActivity.HOME_CONTEXT).progressBar.setVisibility(View.VISIBLE);
			wishlistListView.setVisibility(layout.GONE);
		}

		@Override
		protected Void doInBackground(Void... params) {

			try{
			FriendController friendController = new FriendController(
					HomeActivity.HOME_CONTEXT);
			userWishList = friendController.getFriendWishlist(friend);

			WishlistController wishlistController = new WishlistController(
					HomeActivity.HOME_CONTEXT);
			friendReservedProducts = wishlistController
					.getFriendReservedProducts(friend);
			}catch(Exception ex){
				ex.printStackTrace();
				friendReservedProducts=null;
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {

			
			if(!FriendWishlistFragment.this.isRemoving()&& !FriendWishlistFragment.this.isHidden()){
			((HomeActivity) HomeActivity.HOME_CONTEXT).progressBar.setVisibility(View.GONE);
			try{
			SharedPreferences pref = HomeActivity.HOME_CONTEXT
					.getSharedPreferences(DataUtil.USER_PREF, 0);
			String userJson = pref.getString(DataUtil.USER_JSON, null);
			Gson json = new GsonBuilder().setDateFormat("MMM dd, yyyy")
					.create();
			Type typeOfT = new TypeToken<UserDTO>() {
			}.getType();
			userDTO = json.fromJson(userJson, typeOfT);

			if (userWishList == null || friendReservedProducts == null){
				Toast.makeText(HomeActivity.HOME_CONTEXT, getString(R.string.loading_error), Toast.LENGTH_LONG).show();
				
			}else if(userWishList!=null && userWishList.size() == 0) {
				noWishes.setVisibility(View.VISIBLE);
			}else if (friendReservedProducts != null && userWishList != null) {
				wishlistListView.setVisibility(layout.VISIBLE);
				wishlistListView.setAdapter(new ListAdapter(getActivity(),
						R.layout.product_cell, R.id.product_name, userWishList,
						friendReservedProducts, false, friend, userDTO));
			}
		}catch(Exception ex){
			ex.printStackTrace();
			Toast.makeText(HomeActivity.HOME_CONTEXT, getString(R.string.loading_error), Toast.LENGTH_LONG).show();
		}}
		}
		

	}

	class GetFriendsReservedProduct extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			((HomeActivity) HomeActivity.HOME_CONTEXT).progressBar.setVisibility(View.VISIBLE);
		}

		@Override
		protected Void doInBackground(Void... params) {
			try{
			WishlistController wishlistController = new WishlistController(
					getActivity().getApplicationContext());
			userReserveProducts = wishlistController.getFriendsReservedProduct(
					friend, product);
			}catch(Exception ex){
				ex.printStackTrace();
				userReserveProducts=null;
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			try{
			if(!FriendWishlistFragment.this.isRemoving() && !FriendWishlistFragment.this.isHidden()){
			((HomeActivity) getActivity()).progressBar.setVisibility(View.INVISIBLE);
			if (userReserveProducts != null) {
				SharedPreferences pref = HomeActivity.HOME_CONTEXT
						.getSharedPreferences(DataUtil.USER_PREF, 0);
				String userJson = pref.getString(DataUtil.USER_JSON, null);
				Gson json = new GsonBuilder().setDateFormat("MMM dd, yyyy")
						.create();
				Type typeOfT = new TypeToken<UserDTO>() {
				}.getType();
				UserDTO user = json.fromJson(userJson, typeOfT);

				Intent i = getActivity().getIntent();
				i.putExtra(DataUtil.UserReserveProduct, userReserveProducts);
				i.putExtra(DataUtil.PRODUCT_TO_RESERVE, product);
				i.putExtra(DataUtil.USER, friend);
				i.putExtra(DataUtil.FRIEND, user);

				ReserveProductFragment reserveProductFragment = new ReserveProductFragment();
				getActivity().getFragmentManager().beginTransaction()
						.replace(R.id.frame_container, reserveProductFragment)
						.addToBackStack("reserveFrag").commit();
			} else {
				Toast.makeText(HomeActivity.HOME_CONTEXT, getString(R.string.loading_error),
						Toast.LENGTH_LONG).show();
			}
			
		}
		}catch(Exception ex){
			Toast.makeText(HomeActivity.HOME_CONTEXT, getString(R.string.loading_error),
					Toast.LENGTH_LONG).show();
			ex.printStackTrace();
		}
		}
		
	}
	
	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		((HomeActivity)getActivity()).progressBar.setVisibility(View.GONE);
	}
	
	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		((HomeActivity)getActivity()).progressBar.setVisibility(View.GONE);
	}
	
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		((HomeActivity)getActivity()).progressBar.setVisibility(View.GONE);
	}
}
