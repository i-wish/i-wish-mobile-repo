package com.iwish.fragments;

import java.util.ArrayList;
import java.util.Collections;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.iwish.activities.HomeActivity;
import com.iwish.activities.R;
import com.iwish.adapter.EventListAdapter;
import com.iwish.controller.EventController;
import com.iwish.dto.UserDTO;
import com.iwish.dto.UserEventDTO;
import com.iwish.util.CommonUtil;
import com.iwish.util.DataUtil;

public class HomeFragment extends Fragment {

	LinearLayout layout;
	ListView eventsListView;
	ArrayList<UserEventDTO> userEventDTO;
	UserDTO userDTO;
	EventListAdapter adapter;
	int eventId;
	LinearLayout list_layout;
	TextView noData;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		userEventDTO = new ArrayList<UserEventDTO>();
		adapter = new EventListAdapter(getActivity(), 0, userEventDTO, true);
		adapter.isFriendsEvent = true;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		layout = (LinearLayout) inflater.inflate(
				R.layout.home_page_fragment_layout, container, false);
		
		((HomeActivity)HomeActivity.HOME_CONTEXT).title.setText(getString(R.string.title_home));

		eventsListView = (ListView) layout.findViewById(R.id.home_list);
		list_layout = (LinearLayout) layout.findViewById(R.id.list_layout);
		noData = (TextView) layout.findViewById(R.id.no_events);

		

		eventsListView.setAdapter(adapter);
		if (userEventDTO.size() == 0) {
			if(CommonUtil.checkNetworkConnection(getActivity())){
			GetFriendsEvents getFriendsTask = new GetFriendsEvents();
			getFriendsTask.execute();
			}else{
				CommonUtil.showConnectionToast(HomeFragment.this, inflater, container, savedInstanceState);
			}
		}else{
				eventsListView.setAdapter(adapter);
		}
		

		eventsListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				EventDetailsFragment eventDetailsFragment = new EventDetailsFragment();
				eventDetailsFragment.event = userEventDTO.get(position);
				eventDetailsFragment.isFriend = true;
				getFragmentManager().beginTransaction()
						.replace(R.id.frame_container, eventDetailsFragment)
						.addToBackStack(null).commit();
			}
		});

		return layout;
	}

	class GetFriendsEvents extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			((HomeActivity)HomeActivity.HOME_CONTEXT).progressBar.setVisibility(View.VISIBLE);
		}

		@Override
		protected Void doInBackground(Void... params) {
			try {

				SharedPreferences pref = getActivity().getSharedPreferences(
						DataUtil.USER_PREF, 0);
				String userJson = pref.getString(DataUtil.USER_JSON, null);
				Gson gson = new GsonBuilder().setDateFormat("MMM dd, yyyy")
						.create();
				userDTO = gson.fromJson(userJson, UserDTO.class);
				EventController eventController = new EventController(
						getActivity().getApplicationContext());
				
					userEventDTO = eventController.getFriendsEvents(userDTO);
					
					
				
			} catch (Exception ex) {
				ex.printStackTrace();
				return null;
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (!HomeFragment.this.isHidden()
					&& !HomeFragment.this.isRemoving()) {
				
				((HomeActivity)HomeActivity.HOME_CONTEXT).progressBar.setVisibility(View.GONE);
				try{
				if (userEventDTO != null) {
					if (userEventDTO.size() == 0) {

						list_layout.setVisibility(View.VISIBLE);
						list_layout.setGravity(Gravity.CENTER);
						eventsListView.setVisibility(View.GONE);
						noData.setVisibility(View.VISIBLE);
					} else if (userEventDTO.size() > 0) {
						list_layout.setVisibility(View.VISIBLE);
						if (userEventDTO.size() > 1)
							Collections.sort(userEventDTO);
						adapter = new EventListAdapter(getActivity()
								.getApplicationContext(), 0, userEventDTO, true);
						eventsListView.setAdapter(adapter);
					}
					

				} else {
					list_layout.setVisibility(View.VISIBLE);
					list_layout.setGravity(Gravity.CENTER);
					eventsListView.setVisibility(View.GONE);
					Toast.makeText(HomeActivity.HOME_CONTEXT, getString(R.string.loading_error), Toast.LENGTH_LONG).show();
				}
			}catch(Exception ex){
				Toast.makeText(HomeActivity.HOME_CONTEXT, getString(R.string.loading_error), Toast.LENGTH_LONG).show();
				ex.printStackTrace();
			}
			}	
		}
	}

	

}
