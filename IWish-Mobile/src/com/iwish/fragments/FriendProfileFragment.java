package com.iwish.fragments;

import com.iwish.activities.R;
import com.iwish.dto.UserDTO;
import com.iwish.util.DataUtil;

import android.app.Fragment;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

public class FriendProfileFragment extends Fragment {

	Button getWishlist;
	Button getEvents;
	TextView friendName;
	TextView friendEmail;
	ImageView friendImage;
	View layout;

	UserDTO friend;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		layout =  inflater.inflate(
				R.layout.friend_profile_fargment_layout, container, false);
		getEvents = (Button) layout.findViewById(R.id.get_events);
		getWishlist = (Button) layout.findViewById(R.id.get_wishlist);
		friendName = (TextView) layout.findViewById(R.id.friend_profile_name);
		friendEmail = (TextView) layout.findViewById(R.id.friendEmail);
		friendImage = (ImageView) layout
				.findViewById(R.id.friend_profile_image);

		Intent i = getActivity().getIntent();
		friend = (UserDTO) i.getSerializableExtra(DataUtil.FRIENDSELECTED);

		friendName.setText(friend.getName());
		if (friend.getEmail().substring(0, 1).matches("[0-9]+")) {
			friendEmail.setText("");
			friendEmail.setTextColor(Color.GRAY);
			friendEmail.setTextSize(15);
		} else {
			friendEmail.setText(friend.getEmail());
		}
		if (friend.getPicture() != null) {
			Bitmap bm = BitmapFactory.decodeByteArray(friend.getPicture(), 0,
					friend.getPicture().length);
			friendImage.setImageBitmap(bm);
		}

		getWishlist.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent i = getActivity().getIntent();
				i.putExtra(DataUtil.FRIEND_EMAIL, friend.getEmail());
				FriendWishlistFragment friendWishlistFragment = new FriendWishlistFragment();
				getActivity()
						.getFragmentManager()
						.beginTransaction()
						.replace(R.id.frame_container, friendWishlistFragment,
								DataUtil.CATEGORY_FRAGMENT_TAG)
						.addToBackStack(null).commit();
			}
		});
		
		getEvents.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				events Friendevents=new events(true,friend);
				
				getFragmentManager().beginTransaction()
				.replace(R.id.frame_container, Friendevents)
				.addToBackStack(null).commit();
				
			}
		});

		return layout;
	}
}
