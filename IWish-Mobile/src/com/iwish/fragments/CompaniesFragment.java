package com.iwish.fragments;

import java.util.ArrayList;
import java.util.Locale;

import com.iwish.activities.HomeActivity;
import com.iwish.activities.R;
import com.iwish.adapter.FriendsAdapter;
import com.iwish.controller.ProductsController;
import com.iwish.dto.CategoryDTO;
import com.iwish.dto.CompanyDTO;
import com.iwish.dto.Page;
import com.iwish.util.CommonUtil;
import com.iwish.util.DataUtil;

import android.app.Activity;
import android.app.Fragment;
import android.app.ActionBar.LayoutParams;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.MeasureSpec;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.Toast;

public class CompaniesFragment extends Fragment {

	LinearLayout companiesLayout;
	ListView companyList;
	// LinearLayout progressLayout;
	ArrayList<CompanyDTO> companyDTOs;
	ArrayList<String> searchFriends;
	CategoryDTO categoryDTO;
	ArrayList<String> companyNames;
	CompanyDTO companyDTO;
	Page page;
	View header;
	EditText companySearch;
	LinearLayout listLayout;

	ArrayAdapter<String> adapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		companyNames = new ArrayList<String>();
		companyDTOs = new ArrayList<CompanyDTO>();

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		((HomeActivity) getActivity()).title.setText(getString(R.string.title_companies));
		companiesLayout = (LinearLayout) inflater.inflate(
				R.layout.companies_fragment_layout, container, false);

		companyList = (ListView) companiesLayout
				.findViewById(R.id.company_listview);
		header = inflater.inflate(R.layout.header, companyList, false);
		companySearch = (EditText) header.findViewById(R.id.friend_search);
		
		
		listLayout = (LinearLayout) companiesLayout
				.findViewById(R.id.companys_layout);

		categoryDTO = (CategoryDTO) getActivity().getIntent()
				.getSerializableExtra(DataUtil.CATEGORYDTO);

		

		adapter = new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_list_item_1, android.R.id.text1,
				companyNames);
		companyList.addHeaderView(header);
		companyList.setAdapter(adapter);
		
		// progressLayout.setVisibility(View.GONE);
		listLayout.setVisibility(View.VISIBLE);
		searchFriends = new ArrayList<String>();
		
		if (companyDTOs.size() == 0) {
			if(CommonUtil.checkNetworkConnection(getActivity())){
			GetCompaniesTask getCompaniesTask = new GetCompaniesTask();
			getCompaniesTask.execute();
			}else{
				CommonUtil.showConnectionToast(CompaniesFragment.this, inflater, container, savedInstanceState);
			}
		} else {

			companyNames = new ArrayList<String>();

			for (int i = 0; i < companyDTOs.size(); i++) {
				companyNames.add(companyDTOs.get(i).getName());
			}

			adapter = new ArrayAdapter<String>(getActivity(),
					android.R.layout.simple_list_item_1, android.R.id.text1,
					companyNames);
			//companyList.addHeaderView(header);
			companyList.setAdapter(adapter);

			searchFriends.addAll(companyNames);
		}

		companySearch.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				String text = companySearch.getText().toString()
						.toLowerCase(Locale.getDefault());
				companyNames.clear();
				if (text.length() == 0) {
					companyNames.addAll(searchFriends);
				} else {
					for (String name : searchFriends) {
						if (name.toLowerCase().contains(text)) {
							companyNames.add(name);
						}
					}
				}
				adapter.notifyDataSetChanged();
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});

		companyList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				GetProductsTask getProductsTask = new GetProductsTask();
				getProductsTask.execute();

				companyDTO = companyDTOs.get(position - 1);
			}

		});

		return companiesLayout;
	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}

	class GetCompaniesTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			
			((HomeActivity) HomeActivity.HOME_CONTEXT).progressBar.setVisibility(View.VISIBLE);
		}

		@Override
		protected Void doInBackground(Void... params) {
			try {

				ProductsController productsController = new ProductsController(
						getActivity().getApplicationContext());
				companyDTOs = productsController.getCompanyDTOs(categoryDTO
						.getId());
			} catch (Exception ex) {
				ex.printStackTrace();
				companyDTOs = null;
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {

			if (!CompaniesFragment.this.isHidden()
					&& !CompaniesFragment.this.isRemoving()) {
				try {
					// progressLayout.setVisibility(View.GONE);
					((HomeActivity) HomeActivity.HOME_CONTEXT).progressBar
							.setVisibility(View.GONE);
					listLayout.setVisibility(View.VISIBLE);

					if (companyDTOs != null) {
						for (int i = 0; i < companyDTOs.size(); i++) {
							companyNames.add(companyDTOs.get(i).getName());
						}
					} else {
						Toast.makeText(HomeActivity.HOME_CONTEXT,
								getString(R.string.loading_error),
								Toast.LENGTH_LONG).show();
					}
					searchFriends.addAll(companyNames);
					adapter.notifyDataSetChanged();
					
				} catch (Exception ex) {
					ex.printStackTrace();
					Toast.makeText(HomeActivity.HOME_CONTEXT,
							getString(R.string.loading_error), Toast.LENGTH_LONG)
							.show();
				}
			}
		}

	}

	class GetProductsTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			((HomeActivity) HomeActivity.HOME_CONTEXT).progressBar.setVisibility(View.VISIBLE);
		}

		@Override
		protected Void doInBackground(Void... params) {
			try {
				ProductsController productsController = new ProductsController(
						getActivity().getApplicationContext());
				page = productsController.getProductDTOs(categoryDTO.getId(),
						companyDTO.getId(), 0, 10);
			} catch (Exception ex) {
				ex.printStackTrace();
				page = null;
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {

			super.onPostExecute(result);

			((HomeActivity) getActivity()).progressBar
					.setVisibility(View.GONE);

			listLayout.setVisibility(View.VISIBLE);
			if (page != null) {
				Intent i = getActivity().getIntent();
				i.putExtra(DataUtil.COMPANYDTO, companyDTO);
				i.putExtra(DataUtil.PRODUCTS, page.getProductDTOs());
				i.putExtra(DataUtil.PRODUCTSSIZE, page.getSize());
				i.putExtra(DataUtil.COMPANYID, companyDTO.getId());
				i.putExtra(DataUtil.CATEGORYID, categoryDTO.getId());

				ProductsFragment productsFragment = new ProductsFragment();
				getActivity()
						.getFragmentManager()
						.beginTransaction()
						.replace(R.id.frame_container, productsFragment,
								DataUtil.CATEGORY_FRAGMENT_TAG)
						.addToBackStack(null).commit();

			} else {
				Toast.makeText(getActivity().getApplicationContext(),
						getString(R.string.loading_error), Toast.LENGTH_LONG)
						.show();
			}

		}

	}

	public static void setListViewHeightBasedOnChildren(ListView listView) {
		ArrayAdapter listAdapter = (ArrayAdapter) listView.getAdapter();
		if (listAdapter == null)
			return;

		int desiredWidth = MeasureSpec.makeMeasureSpec(listView.getWidth(),
				MeasureSpec.UNSPECIFIED);
		int totalHeight = 0;
		View view = null;
		for (int i = 0; i < listAdapter.getCount(); i++) {
			view = listAdapter.getView(i, view, listView);
			if (i == 0)
				view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth,
						LayoutParams.WRAP_CONTENT));

			view.measure(desiredWidth, MeasureSpec.UNSPECIFIED);
			totalHeight += view.getMeasuredHeight();
		}
		ViewGroup.LayoutParams params = listView.getLayoutParams();
		params.height = totalHeight
				+ (listView.getDividerHeight() * (listAdapter.getCount() - 1));
		listView.setLayoutParams(params);
		listView.requestLayout();
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		((HomeActivity) getActivity()).progressBar
				.setVisibility(View.GONE);
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		((HomeActivity) getActivity()).progressBar
				.setVisibility(View.GONE);
	}
}
