package com.iwish.fragments;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.iwish.activities.R;
import com.iwish.adapter.ListAdapter;
import com.iwish.controller.UserWishListController;
import com.iwish.controller.WishlistController;
import com.iwish.dto.ProductDTO;
import com.iwish.dto.UserDTO;
import com.iwish.util.CommonUtil;
import com.iwish.util.DataUtil;
import com.iwish.activities.HomeActivity;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class WishListFragment extends Fragment {

	public List<ProductDTO> userWishList = new ArrayList<ProductDTO>();
	List<ProductDTO> wishesToDelete;
	ListView wishlistListView;
	LinearLayout rootView;
	ListAdapter adapter;
	UserDTO userDTO;
	TextView noWishes;
	LinearLayout noWishesLayout;

	ImageView ic_add;
	ImageView ic_delete;

	boolean selected = false;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		
		rootView = (LinearLayout) inflater.inflate(R.layout.fragment_home,
				container, false);
		((HomeActivity)getActivity()).title.setText(getString(R.string.title_wish_list));
		wishlistListView = (ListView) rootView
				.findViewById(R.id.user_wishlist_list);
		noWishes = (TextView) rootView.findViewById(R.id.no_wishes);
		noWishesLayout = (LinearLayout) rootView.findViewById(R.id.no_wishes_layout);
		userWishList = new ArrayList<ProductDTO>();
		wishesToDelete = new ArrayList<ProductDTO>();
		

		// adapter = new ListAdapter(getActivity(), 0, 0, userWishList);
		// wishlistListView.setAdapter(adapter);

		ic_add = (ImageView) getActivity().findViewById(R.id.add);
		ic_delete = (ImageView) getActivity().findViewById(R.id.delete);

		
		
		
		wishlistListView
				.setOnItemLongClickListener(new OnItemLongClickListener() {

					@Override
					public boolean onItemLongClick(AdapterView<?> parent,
							View view, int position, long id) {

						if (!selected) {
							selected = true;
							ic_delete.setVisibility(view.VISIBLE);
							ic_add.setVisibility(view.INVISIBLE);

						}
						if (!wishesToDelete.contains(userWishList.get(position))) {
							wishesToDelete.add(userWishList.get(position));
							// view.findViewById(R.id.select).setVisibility(
							// view.VISIBLE);
							view.setSelected(true);

						} else {
							wishesToDelete.remove(userWishList.get(position));
							// view.findViewById(R.id.select).setVisibility(
							// view.INVISIBLE);
							view.setSelected(false);
							if (wishesToDelete.isEmpty()) {
								ic_delete.setVisibility(view.GONE);
								ic_add.setVisibility(view.VISIBLE);
								selected = false;
							}
						}

						return true;
					}
				});

		wishlistListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				if (selected) {
					if (!wishesToDelete.contains(userWishList.get(position))) {
						wishesToDelete.add(userWishList.get(position));
						// view.findViewById(R.id.select).setVisibility(view.VISIBLE);
						view.setSelected(true);

					} else {
						wishesToDelete.remove(userWishList.get(position));
						// view.findViewById(R.id.select).setVisibility(
						// view.INVISIBLE);
						view.setSelected(false);
						if (wishesToDelete.isEmpty()) {
							((HomeActivity) getActivity()).ic_delete
									.setVisibility(view.GONE);
							((HomeActivity) getActivity()).ic_add
									.setVisibility(view.VISIBLE);
							selected = false;
						}

					}
				} else {
					// go to wish detail
					Intent i = getActivity().getIntent();
					i.putExtra(DataUtil.PRODUCTDETAILS, userWishList.get(position));

					ProductDetailsFragment productDetailsFragment = new ProductDetailsFragment();
					productDetailsFragment.wishlist = true;
					getActivity()
							.getFragmentManager()
							.beginTransaction()
							.replace(R.id.frame_container, productDetailsFragment,
									DataUtil.CATEGORY_FRAGMENT_TAG)
							.addToBackStack("category fragment").commit();
				}
			}
		});

		ic_add.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				((HomeActivity) getActivity()).mDrawerList.setItemChecked(1,
						true);
				((HomeActivity) getActivity()).mDrawerList.setSelection(1);
				//((HomeActivity) getActivity()).title.setText("Product");
				ic_add.setVisibility(View.GONE);

				CategoriesFragment fragment = new CategoriesFragment();
				FragmentManager fragmentManager = getFragmentManager();
				fragmentManager.beginTransaction()
						.replace(R.id.frame_container, fragment)
						.addToBackStack(null).commit();

			}
		});

		ic_delete.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				AlertDialog.Builder builder = new AlertDialog.Builder(
						getActivity());
				builder.setMessage(getString(R.string.wish_delete_dialog))
						.setPositiveButton(getString(R.string.delete),
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										if(CommonUtil.checkNetworkConnection(getActivity())){
										new DeleteTask().execute();
										}else{
											Toast.makeText(getActivity(), getString(R.string.network_error), Toast.LENGTH_LONG).show();
										}

									}
								})
						.setNegativeButton(getString(R.string.cancel),
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {

									}
								});
				// Create the AlertDialog object and return it
				builder.setTitle(getString(R.string.delete));
				builder.create().show();

			}
		});

		if(CommonUtil.checkNetworkConnection(getActivity())){
		GetWishlistTask getWishlistTask = new GetWishlistTask();
		getWishlistTask.execute();
		}else{
			CommonUtil.showConnectionToast(WishListFragment.this, inflater, container, savedInstanceState);
		}
		return rootView;
	}

	class GetWishlistTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			((HomeActivity)HomeActivity.HOME_CONTEXT).progressBar.setVisibility(View.VISIBLE);

			wishlistListView.setVisibility(rootView.GONE);
		}

		@Override
		protected Void doInBackground(Void... params) {

			try{
			SharedPreferences pref = getActivity().getSharedPreferences(
					DataUtil.USER_PREF, 0);
			String userJson = pref.getString(DataUtil.USER_JSON, null);
			Gson gson = new GsonBuilder().setDateFormat("MMM dd, yyyy")
					.create();
			userDTO = gson.fromJson(userJson, UserDTO.class);
			UserWishListController userWishListController = new UserWishListController(
					getActivity().getApplicationContext());
			WishlistController wishlistController = new WishlistController(
					getActivity().getApplicationContext());

			
				userWishList = userWishListController.getWishList(userDTO);
				if (userWishList != null) {
					if (wishlistController
							.saveWishlist((ArrayList<ProductDTO>) userWishList)) {
						Log.i("wishlist", "Caching done");
					} else {
						Log.i("wishlist", "Caching not done");
					}
				}
			}catch(Exception ex){
				ex.printStackTrace();
				userWishList = null;
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);

			if(!WishListFragment.this.isRemoving()&&!WishListFragment.this.isHidden()){
			((HomeActivity)HomeActivity.HOME_CONTEXT).progressBar.setVisibility(View.GONE);
			if (userWishList == null ){
				Toast.makeText(HomeActivity.HOME_CONTEXT, getString(R.string.loading_error), Toast.LENGTH_LONG).show();
			}else if(userWishList.size() == 0) {
				rootView.setGravity(Gravity.CENTER);
				noWishesLayout.setVisibility(View.VISIBLE);
			}

			else if (userWishList != null) {
				wishlistListView.setVisibility(rootView.VISIBLE);
				adapter = new ListAdapter(
						getActivity(), 0, 0,
						userWishList, true, userDTO);
				wishlistListView.setAdapter(adapter);
			}

		}
		}
	}

	public class DeleteTask extends AsyncTask<Void, Void, Boolean> {
		
		ProgressDialog pDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog=ProgressDialog.show(HomeActivity.HOME_CONTEXT,"", getString(R.string.deleting));
			
		}
		
		@Override
		protected Boolean doInBackground(Void... params) {
			boolean result=false;
			try{
			WishlistController controller = new WishlistController(
					getActivity().getApplicationContext());
			 result = controller.deleteWishes(wishesToDelete);
			if (result) {
				ArrayList<ProductDTO> productDTOs = controller.readWishlist();
				if (productDTOs != null) {
					for (int i = 0; i < productDTOs.size(); i++) {
						for (ProductDTO wish : wishesToDelete) {
							if (wish.getId().equals(productDTOs.get(i).getId())) {
								productDTOs.remove(wish);
							}
						}
					}
					controller.saveWishlist(productDTOs);
				}
			}
			}catch(Exception ex){
				ex.printStackTrace();
				return false;
			}
			return result;

		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			pDialog.dismiss();
			if (result) {
				for (ProductDTO p : wishesToDelete) {
					userWishList.remove(p);

				}

				adapter = new ListAdapter(
						HomeActivity.HOME_CONTEXT, 0, 0,
						userWishList, true, userDTO);
				wishlistListView.setAdapter(adapter);
				// list.getAdapter().notify();
				wishesToDelete = new ArrayList<ProductDTO>();
				selected = false;
				((HomeActivity) HomeActivity.HOME_CONTEXT).ic_delete
						.setVisibility(View.GONE);
				((HomeActivity) HomeActivity.HOME_CONTEXT).ic_add
						.setVisibility(View.VISIBLE);
				Toast.makeText(HomeActivity.HOME_CONTEXT, getString(R.string.wish_deleted), Toast.LENGTH_LONG)
						.show();
			} else {

				Toast.makeText(HomeActivity.HOME_CONTEXT, getString(R.string.wish_delete_error), Toast.LENGTH_LONG)
				.show();
			}
			((HomeActivity) HomeActivity.HOME_CONTEXT).ic_delete.setVisibility(View.GONE);
			
		}

	}

	
}
