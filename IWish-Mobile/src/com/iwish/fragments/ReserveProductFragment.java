package com.iwish.fragments;

import java.util.ArrayList;

import com.iwish.activities.HomeActivity;
import com.iwish.activities.R;
import com.iwish.adapter.ReserveProductAdapter;
import com.iwish.controller.ProductsController;
import com.iwish.controller.WishlistController;
import com.iwish.dto.ProductDTO;
import com.iwish.dto.UserDTO;
import com.iwish.dto.UserReserveProduct;
import com.iwish.util.DataUtil;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

public class ReserveProductFragment extends Fragment {

	View layout;

	private ProductDTO productToReserve;
	private ProductDTO product;
	private UserDTO user;
	private UserDTO friend;
	ArrayList<UserReserveProduct> userReserves;

	private ListView reservesListView;
	private Button reserve;
	private EditText participateEditText;
	private EditText comment;
	public SeekBar seekBarParticipate;
	private TextView productName;
	private ImageView productImage;
	private TextView productDetails;
	private TextView productPrice;

	private TextView storeownerName;
	private TextView storeownerEmail;

	private LinearLayout progressLayout;

	public double participate;
	public int result;
	private String commentStr;

	public double remainingValue;
	public double participateValue;
	View header;
	ReserveProductAdapter adapter;
	boolean userDidReserve = false;
	
	ProgressDialog Pdialog;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		layout = inflater.inflate(R.layout.reserve_fragment_layout, container,
				false);
		reservesListView = (ListView) layout
				.findViewById(R.id.reserving_friends_list);
		progressLayout = (LinearLayout) layout
				.findViewById(R.id.reserve_progress_layout);

		header = inflater.inflate(R.layout.reserve_product_first_item,
				reservesListView, false);

		reserve = (Button) header.findViewById(R.id.btn_reserve);
		participateEditText = (EditText) header
				.findViewById(R.id.editText_participate);
		comment = (EditText) header.findViewById(R.id.comment_of_reserve);
		seekBarParticipate = (SeekBar) header
				.findViewById(R.id.seekBar_participate);
		productName = (TextView) header
				.findViewById(R.id.product_to_reserve_name);
		productImage = (ImageView) header
				.findViewById(R.id.product_to_reserve_image);
		productDetails = (TextView) header
				.findViewById(R.id.product_to_reserve_details);
		productPrice = (TextView) header
				.findViewById(R.id.product_to_reserve_price);
		storeownerName = (TextView) header
				.findViewById(R.id.product_to_reserve_storeowner_name);
		storeownerEmail = (TextView) header
				.findViewById(R.id.product_to_reserve_storeowner_email);

		userReserves = (ArrayList<UserReserveProduct>) getActivity()
				.getIntent().getSerializableExtra(DataUtil.UserReserveProduct);
		product = (ProductDTO) getActivity().getIntent().getSerializableExtra(
				DataUtil.PRODUCT_TO_RESERVE);
		productToReserve = new ProductDTO();
		user = (UserDTO) getActivity().getIntent().getSerializableExtra(
				DataUtil.USER);
		friend = (UserDTO) getActivity().getIntent().getSerializableExtra(
				DataUtil.FRIEND);

		GetProductToReserveImage getProductToReserveImage = new GetProductToReserveImage();
		getProductToReserveImage.execute();

		productName.setText(product.getName());
		productName.setTextColor(Color.BLACK);

		productDetails.setText(product.getDescription());
		productPrice.setText("price: " + product.getPrice());

		if (productToReserve.getStoreOwnerDTO() != null) {

			storeownerName.setText(product.getStoreOwnerDTO().getName());
			storeownerEmail.setText(product.getStoreOwnerDTO().getEmail());
		} else {
			storeownerName.setText("no store owner");
		}
		if (userReserves == null) {
			userReserves = new ArrayList<UserReserveProduct>();
			Toast.makeText(getActivity().getApplicationContext(),
					"Please, check network connection", Toast.LENGTH_LONG)
					.show();
		}

		double participatePercent = 0;
		for (UserReserveProduct userReserveProduct : userReserves) {
			if (userReserveProduct.getParticipate() != null) {
				participatePercent = participatePercent
						+ userReserveProduct.getParticipate();
			}

			if (friend.getName().equals(
					userReserveProduct.getUserDTO().getName())) {
				reserve.setText(getString(R.string.cancel));
				userDidReserve = true;
			}
		}

		// if (Math.abs(participatePercent) <= 0) {
		// participatePercent = 1;
		// }
		Log.i("&&&&&&&&&&&&&", "$$$$$$$$$$$$$$ " + product.getPrice() + "  "
				+ productToReserve.getPrice());
		participateValue = (participatePercent * product.getPrice()) / 100.0;
		remainingValue = product.getPrice() - participateValue;

		seekBarParticipate.setMax((int) remainingValue);

		seekBarParticipate
				.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

					int progressValue;

					@Override
					public void onStopTrackingTouch(SeekBar seekBar) {
						Log.i("participate", "participate value "
								+ progressValue);
						participateEditText.setTextColor(Color.BLACK);
						participateEditText.setText(progressValue + "");
						participateEditText.setTextColor(Color.BLACK);
					}

					@Override
					public void onStartTrackingTouch(SeekBar seekBar) {

					}

					@Override
					public void onProgressChanged(SeekBar seekBar,
							int progress, boolean fromUser) {
						progressValue = progress;
						result = progress;
						participateEditText.setText(progressValue + "");
					}
				});

		reserve.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (!userDidReserve) {
					Pdialog = ProgressDialog.show(getActivity(),"", getString(R.string.reserving));
					if ((participateEditText.getText().length() > 0)) {
						if ((Integer.parseInt(participateEditText.getText()
								.toString())) != result) {

							if ((Integer.parseInt(participateEditText.getText()
									.toString())) <= remainingValue) {
								result = Integer.parseInt(participateEditText
										.getText().toString());
								if (commentStr.length() < 300) {
									ReserveWithOthers reserveWithOthers = new ReserveWithOthers();
									reserveWithOthers.execute();
								} else {
									Toast.makeText(getActivity(),
											getString(R.string.comment_too_long),
											Toast.LENGTH_LONG).show();
								}
							} else {
								Toast.makeText(
										getActivity(),
										getString(R.string.participate_exceeds_max),
										Toast.LENGTH_LONG).show();
							}
						} else {
							if (comment.getText().length() < 300) {
								ReserveWithOthers reserveWithOthers = new ReserveWithOthers();
								reserveWithOthers.execute();
							} else {
								Toast.makeText(getActivity(),
										getString(R.string.comment_too_long),
										Toast.LENGTH_LONG).show();
							}
						}
					}
				} else {
					// user will cancel reserve
					Pdialog = ProgressDialog.show(getActivity(),"", getString(R.string.cancel_reserve_in_progress));
					CancelReservationTask cancelReservationTask = new CancelReservationTask();
					cancelReservationTask.execute();
				}
			}

		});

		// for (UserReserveProduct userReserveProduct : userReserves) {
		// if (userReserveProduct.getUserDTO() != null) {
		// if (friend.getId().intValue() == userReserveProduct
		// .getUserDTO().getId().intValue()) {
		// // in reserve with friends if I already reserved .. change
		// // button into cancel.
		// reserve.setEnabled(false);
		// }
		// }
		// }

		return layout;
	}
	
	public ReserveProductAdapter getReserveProductAdapter(){
		return adapter;
	}

	class ReserveWithOthers extends AsyncTask<Void, Void, Boolean> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			((HomeActivity)getActivity()).backFromServer = false;
			participate = (result * 100) / product.getPrice();
			commentStr = comment.getText().toString();
		}

		@Override
		protected Boolean doInBackground(Void... params) {

			WishlistController wishlistController = new WishlistController(
					getActivity().getApplicationContext());
			return wishlistController.reserveWithOthers(product, user, friend,
					participate, commentStr);
		}

		@Override
		protected void onPostExecute(Boolean result) {
			Pdialog.dismiss();
			if (result) {
				Toast.makeText(getActivity().getApplicationContext(),
						getString(R.string.successful), Toast.LENGTH_LONG).show();
				//reserve.setEnabled(false);
				userDidReserve = true;
				
//				Drawable drawable = getActivity().getApplicationContext()
//						.getResources()
//						.getDrawable(R.drawable.btn_cancel_reserve_selector);
//				reserve.setBackgroundDrawable(drawable);
				reserve.setText(getString(R.string.cancel));
				
				UserReserveProduct userReserveProduct = new UserReserveProduct();
				userReserveProduct.setComment(commentStr);
				userReserveProduct.setParticipate(participate);
				userReserveProduct.setUserDTO(friend);
				
				userReserves.add(userReserveProduct);
				
				double participatePercent = 0;
				for (UserReserveProduct userReserveProduct2 : userReserves) {
					if (userReserveProduct2.getParticipate() != null) {
						participatePercent = participatePercent
								+ userReserveProduct2.getParticipate();
					}
				}

				participateValue = (participatePercent * product.getPrice()) / 100.0;

				remainingValue = product.getPrice() - participateValue;

				seekBarParticipate.setMax((int) remainingValue);

				adapter.notifyDataSetChanged();
				
				((HomeActivity)getActivity()).backFromServer = true;
				
			}
		}

	}

	class GetProductToReserveImage extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			((HomeActivity)getActivity()).backFromServer = false;
			reservesListView.setVisibility(layout.GONE);
			progressLayout.setVisibility(layout.VISIBLE);
		}

		@Override
		protected Void doInBackground(Void... params) {
			ProductsController productsController = new ProductsController(
					getActivity().getApplicationContext());
			productToReserve = productsController.getProductImage(product);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (productToReserve == null) {
				Toast.makeText(getActivity(), "Network Failed",
						Toast.LENGTH_LONG).show();
			} else {
				if (productToReserve.getImage() != null) {
					Bitmap bitmap = BitmapFactory.decodeByteArray(
							productToReserve.getImage(), 0,
							productToReserve.getImage().length);
					productImage.setImageBitmap(bitmap);
				}
				adapter = new ReserveProductAdapter(getActivity(), 0, 0,
						userReserves);
				reservesListView.addHeaderView(header);
				reservesListView.setAdapter(adapter);
			}
			progressLayout.setVisibility(layout.GONE);
			reservesListView.setVisibility(layout.VISIBLE);
			((HomeActivity)getActivity()).backFromServer = true;
		}

	}

	class CancelReservationTask extends AsyncTask<Void, Void, Boolean> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			((HomeActivity)getActivity()).backFromServer = false;
		}

		@Override
		protected Boolean doInBackground(Void... params) {
			WishlistController wishlistController = new WishlistController(
					getActivity().getApplicationContext());
			return wishlistController.cancelReservation(user, product, friend);
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			Pdialog.dismiss();
			if (result) {

				userDidReserve = false;
				Toast.makeText(getActivity(), getString(R.string.reservation_canceled),
						Toast.LENGTH_LONG).show();
				reserve.setText(R.string.reserve);
				
				
				double participatePercent = 0;
				for (UserReserveProduct userReserveProduct : userReserves) {
					if (userReserveProduct.getUserDTO().getName()
							.equals(friend.getName())) {
						userReserves.remove(userReserveProduct);
					}

				}
				for (UserReserveProduct userReserveProduct : userReserves) {
					if (userReserveProduct.getParticipate() != null) {
						participatePercent = participatePercent
								+ userReserveProduct.getParticipate();
					}
				}

				participateValue = (participatePercent * product.getPrice()) / 100.0;

				remainingValue = product.getPrice() - participateValue;

				seekBarParticipate.setMax((int) remainingValue);

				adapter.notifyDataSetChanged();
				// FriendsAdapter adapter2 =
				// (FriendsAdapter)((HeaderViewListAdapter)friends.getAdapter()).getWrappedAdapter();

			} else {
				Toast.makeText(getActivity(),
						"failed to cancel reservation, Please try again",
						Toast.LENGTH_LONG).show();
			}
			
			((HomeActivity)getActivity()).backFromServer = true;
		}

	}

}
