package com.iwish.fragments;

import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.iwish.activities.HomeActivity;
import com.iwish.activities.R;
import com.iwish.activities.R.id;
import com.iwish.activities.R.layout;
import com.iwish.controller.GreetingController;
import com.iwish.dto.EventCommentsDTO;
import com.iwish.dto.GreetingDTO;
import com.iwish.dto.UserDTO;
import com.iwish.dto.UserEventDTO;
import com.iwish.util.CommonUtil;
import com.iwish.util.DataUtil;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class GreetingFragment extends Fragment {

	TextView send, suggest;
	EditText greeting;
	UserDTO user;
	UserEventDTO event;
	List<GreetingDTO> greetingList;
	ProgressDialog Pdialog;

	public GreetingFragment(UserEventDTO event) {
		this.event = event;
	}

	
	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		((HomeActivity)HomeActivity.HOME_CONTEXT).progressBar.setVisibility(View.GONE);
	}
	
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		((HomeActivity)HomeActivity.HOME_CONTEXT).progressBar.setVisibility(View.GONE);
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.activity_greeting, container,
				false);

		String userJson = getActivity().getSharedPreferences(
				DataUtil.USER_PREF, 0).getString(DataUtil.USER_JSON, null);
		Gson json = new GsonBuilder().setDateFormat("MMM dd, yyyy").create();
		user = json.fromJson(userJson, UserDTO.class);

		send = (TextView) rootView.findViewById(R.id.send);
		suggest = (TextView) rootView.findViewById(R.id.suggestion);
		greeting = (EditText) rootView.findViewById(R.id.greeting);
		if(CommonUtil.checkNetworkConnection(getActivity())){
		new greetingSuggestionTask().execute();
		}else{
			CommonUtil.showConnectionToast(GreetingFragment.this, inflater, container, savedInstanceState);
		}

		if (event != null) {
			((HomeActivity)getActivity()).title.setText(event.getName());
			suggest.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {

					if (greetingList != null) {
						String[] greetingArr = new String[greetingList.size()];
						for (int i = 0; i < greetingList.size(); i++) {
							greetingArr[i] = greetingList.get(i)
									.getDescription();
						}
						AlertDialog.Builder builder = new AlertDialog.Builder(
								getActivity());
						builder.setTitle(getString(R.string.greetings_suggestions))
								.setItems(greetingArr,
										new DialogInterface.OnClickListener() {
											public void onClick(
													DialogInterface dialog,
													int which) {
												greeting.setText(greetingList
														.get(which)
														.getDescription());
											}

										}).create();
						builder.show();

					}

				}
			});

		}

		send.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if(CommonUtil.checkNetworkConnection(getActivity())){
				 Pdialog = ProgressDialog.show(getActivity(),"", getString(R.string.sending));
				
				new sendGreeting().execute();
			}else{
				Toast.makeText(getActivity(), getString(R.string.network_error), Toast.LENGTH_LONG).show();
			}
			}
		});

		return rootView;

	}

	private class greetingSuggestionTask extends AsyncTask<Void, Void, String> {

		@Override
		protected String doInBackground(Void... params) {
			try{
			GreetingController controller = new GreetingController(
					getActivity());
			return controller.getGreetingSuggestion(event.getEventTypeDTO());
			}catch (Exception e) {
				e.printStackTrace();
				return null;
			}

		}

		@Override
		protected void onPostExecute(String result) {
			if(!GreetingFragment.this.isHidden()&&!GreetingFragment.this.isRemoving()){
			try {
				if(result==null){
					Toast.makeText(getActivity(), getString(R.string.loading_error),
							Toast.LENGTH_LONG).show();
				}else{
				GreetingController controller = new GreetingController(
						HomeActivity.HOME_CONTEXT);
				greetingList = controller.parseGreetingSuggestions(result);
				}
			} catch (Exception ex) {
				Toast.makeText(getActivity(), getString(R.string.loading_error),
						Toast.LENGTH_LONG).show();
			}
			}

		}

	}

	private class sendGreeting extends AsyncTask<Void, Void, String> {

		@Override
		protected void onPreExecute() {
			
			super.onPreExecute();
			//((HomeActivity) getActivity()).backFromServer = false;
		}

		@Override
		protected String doInBackground(Void... params) {
			try{
			GreetingController controller = new GreetingController(
					getActivity());
			EventCommentsDTO comment = new EventCommentsDTO();
			comment.setContent(greeting.getText().toString());
			comment.setUserEventDTO(event);
			comment.setUser(user);
			return controller.sendGreeting(comment);
			}catch(Exception ex){
				ex.printStackTrace();
				return null;
			}

		}

		@Override
		protected void onPostExecute(String result) {
			Pdialog.dismiss();
			try{
			GreetingController controller = new GreetingController(
					getActivity());
			if (controller.parseSendGreeting(result)) {
				EventDetailsFragment eventDetails = new EventDetailsFragment();
				eventDetails.event = event;
				eventDetails.isFriend = true;
				getFragmentManager().popBackStack();
				/*
				 * getFragmentManager().popBackStack();
				 * getFragmentManager().beginTransaction
				 * ().replace(R.id.frame_container,
				 * eventDetails).addToBackStack(null) .commit();
				 */

			} else {
				Toast.makeText(getActivity(), getString(R.string.sending_failed), Toast.LENGTH_LONG).show();
			}
			}catch(Exception ex){
				Toast.makeText(getActivity(), getString(R.string.sending_failed), Toast.LENGTH_LONG).show();
			}
			
		}

	}
	
	
}
