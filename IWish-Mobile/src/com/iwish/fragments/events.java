package com.iwish.fragments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.iwish.activities.HomeActivity;
import com.iwish.activities.R;
import com.iwish.adapter.EventListAdapter;
import com.iwish.adapter.ListAdapter;
import com.iwish.controller.EventController;
import com.iwish.controller.FriendController;
import com.iwish.dto.UserDTO;
import com.iwish.dto.UserEventDTO;
import com.iwish.util.CommonUtil;
import com.iwish.util.DataUtil;

import android.app.Fragment;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.R.color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;

public class events extends Fragment {

	public List<UserEventDTO> eventList = new ArrayList<UserEventDTO>();
	public List<UserEventDTO> eventsToDelete;
	public EventListAdapter adapter;

	private boolean selected;

	public events eventFragment;

	public ListView list;
	boolean isFriend = false;
	ListView wishlist;
	UserDTO user;
	Button reserve;
	ListView eventsList;
	ImageView ic_addEvent;
	LinearLayout noEventsLayout;

	// LinearLayout progressLayout;

	public events() {
		isFriend = false;
	}

	public events(boolean isFriend) {
		this.isFriend = isFriend;
	}

	public events(boolean isFriend, UserDTO user) {
		this.isFriend = isFriend;
		this.user = user;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.activity_event_list,
				container, false);

		
		eventsToDelete = new ArrayList<UserEventDTO>();
		list = (ListView) rootView.findViewById(R.id.eventList);
		noEventsLayout = (LinearLayout) rootView
				.findViewById(R.id.no_events_layout);

		eventFragment = this;

		if (isFriend) {
			if(eventList.size()==0){
			((HomeActivity) getActivity()).title.setText(user.getName());
			if (CommonUtil.checkNetworkConnection(getActivity())) {
				FriendEventsTask friendEventsTask = new FriendEventsTask();
				friendEventsTask.execute();
			} else {
				CommonUtil.showConnectionToast(events.this, inflater,
						container, savedInstanceState);
			}
			}else{
				list.setVisibility(View.VISIBLE);
				adapter = new EventListAdapter(getActivity()
						.getApplicationContext(), 0, eventList);
				list.setAdapter(adapter);
				
			}
			list.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					EventDetailsFragment eventDetailsFragment = new EventDetailsFragment();
					eventDetailsFragment.event = eventList.get(position);
					eventDetailsFragment.isFriend = true;
					getFragmentManager()
							.beginTransaction()
							.replace(R.id.frame_container, eventDetailsFragment)
							.addToBackStack(null).commit();
				}
			});

		} else {
			((HomeActivity) getActivity()).title
					.setText(getString(R.string.title_event_list));
			ic_addEvent = (ImageView) getActivity().findViewById(R.id.add);
//			ic_addEvent.setVisibility(View.VISIBLE);
			ic_addEvent.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					NewEventFragment newEvent = new NewEventFragment();
					android.app.FragmentManager fragmentManager = getFragmentManager();
					fragmentManager.beginTransaction()
							.replace(R.id.frame_container, newEvent)
							.addToBackStack(null).commit();

				}
			});

			if(eventList.size()==0){
			if (CommonUtil.checkNetworkConnection(getActivity())) {
				new getEvents().execute();
			} else {
				CommonUtil.showConnectionToast(events.this, inflater,
						container, savedInstanceState);
			}
			}else{
				adapter = new EventListAdapter(getActivity()
						.getApplicationContext(), 0, eventList);
				list.setAdapter(adapter);
				
			}
			

			list.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {

					EventDetailsFragment detailsFragment = new EventDetailsFragment();
					detailsFragment.isFriend = false;
					detailsFragment.eventFragment = events.this;
					detailsFragment.event = eventList.get(position);

					android.app.FragmentManager fragmentManager = getFragmentManager();
					fragmentManager.beginTransaction()
							.replace(R.id.frame_container, detailsFragment)
							.addToBackStack(null).commit();

				}

			});

		}
		return rootView;

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	class FriendEventsTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			((HomeActivity) HomeActivity.HOME_CONTEXT).progressBar
					.setVisibility(View.VISIBLE);
			list.setVisibility(View.GONE);
		}

		@Override
		protected Void doInBackground(Void... params) {
			try {
				FriendController friendController = new FriendController(
						getActivity().getApplicationContext());
				eventList = friendController.getFriendEvents(user);
			} catch (Exception ex) {
				ex.printStackTrace();
				eventList = null;
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {

			
			if (!events.this.isHidden() && !events.this.isRemoving()) {
				((HomeActivity) HomeActivity.HOME_CONTEXT).progressBar
				.setVisibility(View.GONE);
				try {
					if (eventList == null) {
						Toast.makeText(HomeActivity.HOME_CONTEXT,
								getString(R.string.loading_error),
								Toast.LENGTH_LONG).show();
					} else if (eventList.size() == 0) {
					
						noEventsLayout.setVisibility(View.VISIBLE);
					}

					else if (eventList != null) {
						list.setVisibility(View.VISIBLE);
						adapter = new EventListAdapter(getActivity()
								.getApplicationContext(), 0, eventList);
						list.setAdapter(adapter);
					}
				} catch (Exception ex) {
					ex.printStackTrace();
					Toast.makeText(HomeActivity.HOME_CONTEXT,
							getString(R.string.loading_error),
							Toast.LENGTH_LONG).show();
				}
			}

		}

	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		if (ic_addEvent != null) {
			ic_addEvent.setVisibility(View.GONE);
		}
		((HomeActivity) getActivity()).progressBar.setVisibility(View.GONE);
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		if (ic_addEvent != null) {
			ic_addEvent.setVisibility(View.GONE);
		}
		((HomeActivity) getActivity()).progressBar.setVisibility(View.GONE);
	}

	class getEvents extends AsyncTask<Void, Void, String> {
		EventController controller = new EventController(getActivity());

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			((HomeActivity) HomeActivity.HOME_CONTEXT).progressBar
					.setVisibility(View.VISIBLE);
			list.setVisibility(View.GONE);
		}

		@Override
		protected String doInBackground(Void... params) {
			try {
				String result = controller.getEvents();
				eventList = controller.parseEvents(result);
				controller.saveEvents((ArrayList<UserEventDTO>) eventList,
						DataUtil.MY_EVENTS);

			} catch (Exception ex) {
				ex.printStackTrace();
				eventList = null;
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {

			((HomeActivity) getActivity()).progressBar.setVisibility(View.GONE);
			ic_addEvent.setVisibility(View.VISIBLE);

			if (!events.this.isRemoving() && !events.this.isHidden()) {
				((HomeActivity) HomeActivity.HOME_CONTEXT).progressBar.setVisibility(View.GONE);
				try {
					if (eventList == null) {
						Toast.makeText(HomeActivity.HOME_CONTEXT,
								getString(R.string.loading_error),
								Toast.LENGTH_LONG).show();
					} else if (eventList.size() == 0) {
						noEventsLayout.setVisibility(View.VISIBLE);
					} else if (eventList != null) {
						list.setVisibility(View.VISIBLE);
						adapter = new EventListAdapter(getActivity(), 0,
								eventList);
						list.setAdapter(adapter);
					}
				} catch (Exception ex) {
					ex.printStackTrace();
					Toast.makeText(HomeActivity.HOME_CONTEXT,
							getString(R.string.loading_error),
							Toast.LENGTH_LONG).show();
				}
			}
		}

	}

}
