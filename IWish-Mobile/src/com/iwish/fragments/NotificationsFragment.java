package com.iwish.fragments;

import java.lang.reflect.Type;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.iwish.activities.R;
import com.iwish.activities.R.layout;
import com.iwish.adapter.NotificationAdapter;
import com.iwish.controller.EventController;
import com.iwish.dto.NotificationDTO;
import com.iwish.dto.UserDTO;
import com.iwish.util.DataUtil;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

public class NotificationsFragment extends Fragment {

	LinearLayout layout;
	ListView notificationList;
	LinearLayout notificationLayout;
	ProgressBar progressBar;
	ArrayList<NotificationDTO> notifications;
	NotificationDTO notificationDTO = null;
	NotificationAdapter adapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		notifications = new ArrayList<NotificationDTO>();
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		layout = (LinearLayout) inflater.inflate(
				R.layout.notification_fragment_layout, container, false);
		notificationList = (ListView) layout
				.findViewById(R.id.notification_list);
		notificationLayout = (LinearLayout) layout
				.findViewById(R.id.notification_layout);
		progressBar = (ProgressBar) layout.findViewById(R.id.progressBar2);

		if (notifications.size() == 0) {
			GetNotificationsTask getNotificationsTask = new GetNotificationsTask();
			getNotificationsTask.execute();
		}

		adapter = new NotificationAdapter(
				getActivity().getApplicationContext(), 0, 0, notifications);
		notificationList.setAdapter(adapter);

		notificationList
				.setOnItemClickListener(new AdapterView.OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> parent, View view,
							int position, long id) {

						notificationDTO = notifications.get(position);
						NotificationReceiveTask notificationReceiveTask = new NotificationReceiveTask();
						notificationReceiveTask.execute();
						
						SharedPreferences pref = getActivity().getSharedPreferences(DataUtil.USER_PREF, 0);
						int userId = pref.getInt(DataUtil.USER_ID, 0);
						EventDetailsFragment eventFragment = new EventDetailsFragment();
						eventFragment.event=notificationDTO.getUserEvent();
						eventFragment.isFriend=(userId==notificationDTO.getUserEvent().getUserDTO().getId())?false:true;
						getFragmentManager().beginTransaction().replace(R.id.frame_container, eventFragment)
						.addToBackStack(null).commit();
						
					}
				});

		return layout;

	}

	class GetNotificationsTask extends AsyncTask<Void, Void, String> {

		@Override
		protected void onPreExecute() {

			notificationLayout.setVisibility(layout.VISIBLE);
			Log.i("pre-execute", "***************");
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(Void... params) {

			SharedPreferences pref = getActivity().getSharedPreferences(
					DataUtil.USER_PREF, 0);
			String userJson = pref.getString(DataUtil.USER_JSON, null);
			Gson json = new GsonBuilder().setDateFormat("MMM dd, yyyy")
					.create();
			Type typeOfT = new TypeToken<UserDTO>() {
			}.getType();
			UserDTO userDTO = json.fromJson(userJson, typeOfT);

			EventController eventController = new EventController(getActivity()
					.getApplicationContext());
			if (checkNetworkConnection()) {
				notifications = eventController.getNotifications(userDTO);
				eventController.saveNotifications(notifications,
						DataUtil.NOTIFICATIONS);
			} else {
				notifications = (ArrayList<NotificationDTO>) eventController
						.readNotifications(DataUtil.NOTIFICATIONS);
			}
			if (notifications == null) {
				return "failed";
			}
			return "success";
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			Log.i("post-execute", "***************" + result);
			notificationLayout.setVisibility(layout.GONE);
			notificationList.setVisibility(layout.VISIBLE);
			if (result.equals("failed")) {
				Toast.makeText(getActivity(), "Network error",
						Toast.LENGTH_LONG).show();

			} else {
				adapter = new NotificationAdapter(getActivity()
						.getApplicationContext(), 0, 0, notifications);
				notificationList.setAdapter(adapter);
			}

		}

	}

	class NotificationReceiveTask extends AsyncTask<Void, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			notificationLayout.setVisibility(layout.VISIBLE);
		}

		@Override
		protected String doInBackground(Void... params) {

			EventController eventController = new EventController(getActivity()
					.getApplicationContext());
			return eventController.notificationReceived(notificationDTO);
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			notificationLayout.setVisibility(layout.GONE);
			notificationList.setVisibility(layout.VISIBLE);
			if (!result.equals("success")) {
				Toast.makeText(getActivity(), "Network Failed",
						Toast.LENGTH_LONG).show();
			}
			adapter = new NotificationAdapter(getActivity()
					.getApplicationContext(), 0, 0, notifications);
			notificationList.setAdapter(adapter);
			EventLoadingFragment loading = new EventLoadingFragment();

			if (notificationDTO.getUserEvent() != null) {
				loading.event_id = notificationDTO.getUserEvent().getId();
				getFragmentManager().beginTransaction()
						.replace(R.id.frame_container, loading)
						.addToBackStack(null).commit();
			}
		}

	}

	public boolean checkNetworkConnection() {
		ConnectivityManager manager = (ConnectivityManager) getActivity()
				.getApplicationContext()
				.getSystemService(
						getActivity().getApplicationContext().CONNECTIVITY_SERVICE);
		NetworkInfo wifiNetworkInfo = manager
				.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		NetworkInfo mobileNetworkInfo = manager
				.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		if (wifiNetworkInfo.isConnected() || mobileNetworkInfo.isConnected()) {
			return true;
		}
		return false;
	}

}
