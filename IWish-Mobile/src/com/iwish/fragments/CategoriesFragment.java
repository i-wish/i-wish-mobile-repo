package com.iwish.fragments;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.iwish.activities.CommonUtilites;
import com.iwish.activities.HomeActivity;
import com.iwish.activities.R;
import com.iwish.controller.ProductsController;
import com.iwish.dto.CategoryDTO;
import com.iwish.util.CommonUtil;
import com.iwish.util.DataUtil;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.util.LruCache;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class CategoriesFragment extends Fragment {

	// private ProgressBar progressBar;
	private LinearLayout categoriesLayout;
	private ArrayList<CategoryDTO> categoryDTOs;
	private GridView categoriesGridView;
	// private LinearLayout progressLayout;
	private LinearLayout gridLayout;
	boolean backFromServer = false;
	private LruCache<String, Bitmap> mMemoryCache;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		categoryDTOs = new ArrayList<CategoryDTO>();

		// Get max available VM memory, exceeding this amount will throw an
		// OutOfMemory exception. Stored in kilobytes as LruCache takes an
		// int in its constructor.
		final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);

		// Use 1/8th of the available memory for this memory cache.
		final int cacheSize = maxMemory / 8;

		mMemoryCache = new LruCache<String, Bitmap>(cacheSize) {
			@Override
			protected int sizeOf(String key, Bitmap bitmap) {
				// The cache size will be measured in kilobytes rather than
				// number of items.
				return bitmap.getByteCount() / 1024;
			}
		};
	}

	public void addBitmapToMemoryCache(String key, Bitmap bitmap) {
		if (getBitmapFromMemCache(key) == null) {
			mMemoryCache.put(key, bitmap);
		}
	}

	public Bitmap getBitmapFromMemCache(String key) {
		return mMemoryCache.get(key);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		categoriesLayout = (LinearLayout) inflater.inflate(
				R.layout.categories_fragment_layout, container, false);
		categoriesGridView = (GridView) categoriesLayout
				.findViewById(R.id.categories_grid_layout);
		gridLayout = (LinearLayout) categoriesLayout
				.findViewById(R.id.grid_layout);

		((HomeActivity) getActivity()).title
				.setText(getString(R.string.title_categories));
		if (categoryDTOs != null && categoryDTOs.size() > 0) {
			CategoryAdapter adapter = new CategoryAdapter(getActivity(),
					R.layout.category_grid_layout, R.id.category_name,
					categoryDTOs);
			categoriesGridView.setAdapter(adapter);

		} else {
			if (CommonUtil.checkNetworkConnection(getActivity()
					.getApplicationContext())) {
				GetCategoriesTask getCategoriesTask = new GetCategoriesTask();
				getCategoriesTask.execute();
			} else {
				CommonUtil.showConnectionToast(CategoriesFragment.this,
						inflater, container, savedInstanceState);
			}
		}

		return categoriesLayout;
	}

	class GetCategoriesTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			((HomeActivity) HomeActivity.HOME_CONTEXT).progressBar
					.setVisibility(View.VISIBLE);
			Log.i("task", "Pre_Execute");
		}

		@Override
		protected Void doInBackground(Void... params) {
			try {
				ProductsController productsController = new ProductsController(
						getActivity().getApplicationContext());
				categoryDTOs = productsController.getCategories();
			} catch (Exception ex) {
				ex.printStackTrace();
				categoryDTOs = null;
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			if (!CategoriesFragment.this.isRemoving()
					&& !CategoriesFragment.this.isHidden()) {
				Log.i("task", "Post Execute");

				((HomeActivity) HomeActivity.HOME_CONTEXT).progressBar
						.setVisibility(View.GONE);
				try {

					gridLayout.setVisibility(categoriesLayout.VISIBLE);

					if (categoryDTOs != null) {
						// Collections.sort(categoryDTOs);
						CategoryAdapter adapter = new CategoryAdapter(
								getActivity(), R.layout.category_grid_layout,
								R.id.category_name, categoryDTOs);
						categoriesGridView.setAdapter(adapter);
					} else {
						Toast.makeText(HomeActivity.HOME_CONTEXT,
								getString(R.string.loading_error),
								Toast.LENGTH_LONG).show();
					}

				} catch (Exception ex) {
					ex.printStackTrace();
					Toast.makeText(HomeActivity.HOME_CONTEXT,
							getString(R.string.loading_error),
							Toast.LENGTH_LONG).show();
				}

			}
		}

	}

	@Override
	public void onPause() {
		super.onPause();
		((HomeActivity) getActivity()).progressBar.setVisibility(View.GONE);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		((HomeActivity) getActivity()).progressBar.setVisibility(View.GONE);
	}

	class GetCategoriesImageTask extends AsyncTask<CategoryDTO, Void, Bitmap> {

		private final WeakReference<ImageView> imageViewReference;

		public GetCategoriesImageTask(ImageView imageView) {
			imageViewReference = new WeakReference<ImageView>(imageView);
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected Bitmap doInBackground(CategoryDTO... params) {
			CategoryDTO categoryDTO;
			try {
				ProductsController productsController = new ProductsController(
						getActivity().getApplicationContext());
				categoryDTO = productsController.getCategoryImage(params[0]);
				Bitmap bitmap = BitmapFactory.decodeByteArray(
						categoryDTO.getImage(), 0,
						categoryDTO.getImage().length);
				if (bitmap != null) {
					addBitmapToMemoryCache(String.valueOf(params[0].getId()),
							bitmap);
				}
				return bitmap;
			} catch (Exception ex) {
				ex.printStackTrace();
				categoryDTO = null;
			}
			return null;
		}

		@Override
		protected void onPostExecute(Bitmap result) {
			super.onPostExecute(result);

			if (imageViewReference != null) {
				ImageView imageView = imageViewReference.get();
				if (imageView != null) {
					if (result != null) {
						imageView.setImageBitmap(result);
					}
				}
			}
		}

	}

	class CategoryAdapter extends ArrayAdapter<CategoryDTO> {

		ArrayList<CategoryDTO> categoryDTOs;
		Context context;
		CategoryDTO categoryDTO;

		public CategoryAdapter(Context context, int resource,
				int textViewResourceId, List<CategoryDTO> objects) {
			super(context, resource, textViewResourceId, objects);
			this.categoryDTOs = (ArrayList<CategoryDTO>) objects;
			this.context = context;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			ViewHolder viewHolder = null;
			final int productIndex = position;

			if (convertView == null) {
				LayoutInflater inflater = (LayoutInflater) context
						.getSystemService(context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(R.layout.category_grid_layout,
						parent, false);
				viewHolder = new ViewHolder(convertView);
				viewHolder.catName = (TextView) convertView
						.findViewById(R.id.category_name);
				viewHolder.catImage = (ImageView) convertView
						.findViewById(R.id.category_image);
				convertView.setTag(viewHolder);
			} else {
				viewHolder = (ViewHolder) convertView.getTag();
			}

			viewHolder.catName.setText(categoryDTOs.get(position).getName());
			viewHolder.catName.setTextColor(Color.BLACK);

			final Bitmap bitmap = getBitmapFromMemCache(String
					.valueOf(categoryDTOs.get(position).getId()));
			if (bitmap != null) {
				viewHolder.catImage.setImageBitmap(bitmap);
			} else {
				viewHolder.catImage
						.setImageResource(R.drawable.mobile_cat_normal_icon);
				downloadImage(categoryDTOs.get(position), viewHolder.catImage);
			}
			// if (categoryDTOs.get(position).getImage() != null) {
			// Bitmap bitmap = BitmapFactory.decodeByteArray(
			// categoryDTOs.get(position).getImage(), 0, categoryDTOs
			// .get(position).getImage().length);
			// viewHolder.catImage.setImageBitmap(bitmap);
			// Drawable drawable = context.getResources().getDrawable(
			// R.drawable.categories_selector);
			// viewHolder.catImage.setBackgroundDrawable(drawable);
			// }

			viewHolder.catImage.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					categoryDTO = categoryDTOs.get(productIndex);
					Intent i = ((Activity) context).getIntent();
					i.putExtra(DataUtil.CATEGORYDTO, categoryDTO);
					CompaniesFragment companiesFragment = new CompaniesFragment();
					((Activity) context)
							.getFragmentManager()
							.beginTransaction()
							.replace(R.id.frame_container, companiesFragment,
									DataUtil.CATEGORY_FRAGMENT_TAG)
							.addToBackStack("category fragment").commit();
				}
			});

			return convertView;
		}

		public void downloadImage(CategoryDTO categoryDTO, ImageView imageView) {
			GetCategoriesImageTask getCategoriesImageTask = new GetCategoriesImageTask(
					imageView);
			getCategoriesImageTask.execute(categoryDTO);
		}

	}

	static class ViewHolder {

		View rootView;
		public TextView catName;
		public ImageView catImage;

		ViewHolder(View rootView) {
			this.rootView = rootView;
		}

	}

}
