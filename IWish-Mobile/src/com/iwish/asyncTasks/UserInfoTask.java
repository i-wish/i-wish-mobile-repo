package com.iwish.asyncTasks;

import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.iwish.businesslayer.ParseUser;
import com.iwish.controller.FriendController;
import com.iwish.controller.UserController;
import com.iwish.datalayer.FileAccess;
import com.iwish.datalayer.WSConnection;
import com.iwish.dto.UserDTO;
import com.iwish.util.DataUtil;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

public class UserInfoTask extends AsyncTask<UserController, Void, String> {
	String url = DataUtil.WS_URL+"login?accessToken=";
	UserDTO user;
	UserController userController;
	Intent intent;
	Context context;

	public UserDTO getUser() {
		return user;
	}

	@Override
	protected String doInBackground(UserController... params) {
		String json;
		try{
		
		userController = params[0];
		context = userController.context;
		if (checkNetworkConnection()) {
			WSConnection ws = new WSConnection();
			json = ws.get(url
					+ userController.getAccessToken());
			Log.i("user data", json);

			if (!json.equals("error")) {
				Gson gson = new GsonBuilder().setDateFormat("MMM dd, yyyy")
						.create();
				UserDTO userDTO = gson.fromJson(json, UserDTO.class);
				//
				// FriendController friendController = new FriendController(
				// userController.context);
				// friendController.getFriends(userDTO);
			}
		} else {
			json = "error";
		}
		}catch(Exception ex){
			ex.printStackTrace();
			json = "error";
		}

		return json;

	}

	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		try{
		if (!(result.equals("error"))) {

			Log.i("logged", "true true true");
			ParseUser parser = new ParseUser(userController.context);
			user = parser.getUser(result);
			userController.setUser(user);

			SharedPreferences pref2 = userController.context
					.getSharedPreferences(DataUtil.LOGGEDIN, 0);
			SharedPreferences.Editor editor2 = pref2.edit();
			editor2.putString(DataUtil.LOGGEDIN, "true");
			editor2.commit();

			SharedPreferences userPref = userController.context
					.getSharedPreferences(DataUtil.USER_PREF, 0);
			SharedPreferences.Editor editor1 = userPref.edit();
			editor1.putString(DataUtil.USER_FB_name, user.getName());
			editor1.putString(DataUtil.USER_FB_email, user.getEmail());
			editor1.putString(DataUtil.USER_FB_ID, user.getFacebookUserId());
			editor1.putInt(DataUtil.USER_ID, user.getId());
			editor1.putString(DataUtil.USER_JSON, result);

			String profilePic = Base64.encodeToString(user.getPicture(),
					Base64.DEFAULT);
			editor1.putString(DataUtil.USER_FB_img, profilePic);
			editor1.commit();

		}
		}catch(Exception ex){
			ex.printStackTrace();
			Toast.makeText(context, "couldn't load data", Toast.LENGTH_LONG).show();
		}
	}

	public boolean checkNetworkConnection() {
		ConnectivityManager manager = (ConnectivityManager) context
				.getSystemService(context.CONNECTIVITY_SERVICE);
		NetworkInfo wifiNetworkInfo = manager
				.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		NetworkInfo mobileNetworkInfo = manager
				.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		if (wifiNetworkInfo.isConnected() || mobileNetworkInfo.isConnected()) {
			return true;
		}
		return false;

	}

}
