package com.iwish.asyncTasks;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;

import android.widget.Toast;

import com.iwish.adapter.EventListAdapter;
import com.iwish.businesslayer.EventParser;
import com.iwish.businesslayer.ParseUser;
import com.iwish.controller.EventController;
import com.iwish.controller.UserController;
import com.iwish.datalayer.WSConnection;
import com.iwish.dto.UserDTO;
import com.iwish.dto.UserEventDTO;
import com.iwish.util.DataUtil;

public class eventTask extends AsyncTask<EventController, Void, String>{
	String url=DataUtil.WS_URL+"event/get?fb_id=";
	UserDTO user;
	EventController eventController;
	Context context;

	public UserDTO getUser() {
		return user;
	}

	@Override
	protected String doInBackground(EventController... params) {
		// TODO Auto-generated method stub
		eventController = params[0];
		this.context = eventController.context;
		if (checkNetworkConnection()) {
			WSConnection ws = new WSConnection();
			SharedPreferences pref = eventController.context
					.getSharedPreferences(DataUtil.USER_PREF, 0);
			String fb_id = pref.getString(DataUtil.USER_FB_ID, null);
			return ws.get( url + fb_id);
		}
		return "error";
	}

	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);

		List<UserEventDTO> eventList;
		if (!(result.equals("error"))) {
			EventParser parser = new EventParser(eventController.context);
			eventList = parser.parseEvents(result);
			/*
			 * eventList=new ArrayList<UserEventDTO>(); UserEventDTO e1=new
			 * UserEventDTO(); e1.setDescription("my first event");
			 * e1.setName("my birthday"); eventList.add(e1);
			 */

			eventController.eventFragment.eventList = eventList;
		} else {
			eventList = new ArrayList<UserEventDTO>();
		}
		eventController.eventFragment.adapter = new EventListAdapter(
				eventController.context, 0, eventList);
		eventController.eventFragment.list
				.setAdapter(eventController.eventFragment.adapter);

	}

	public boolean checkNetworkConnection() {
		ConnectivityManager manager = (ConnectivityManager) context
				.getSystemService(context.CONNECTIVITY_SERVICE);
		NetworkInfo wifiNetworkInfo = manager
				.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		NetworkInfo mobileNetworkInfo = manager
				.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		if (wifiNetworkInfo.isConnected() || mobileNetworkInfo.isConnected()) {
			return true;
		}
		return false;
	}

}
