package com.iwish.businesslayer;

import java.lang.reflect.Type;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.iwish.datalayer.WSConnection;
import com.iwish.dto.EventCommentsDTO;
import com.iwish.dto.EventTypeDTO;
import com.iwish.dto.GreetingDTO;
import com.iwish.dto.UserDTO;
import com.iwish.dto.UserEventDTO;
import com.iwish.util.DataUtil;

public class GreetingParser {

	
	String Greeting_SUGGESTION_URL=DataUtil.WS_URL+"greeting/suggestion?eventTypeId=";
	String SEND_GREETING_URL=DataUtil.WS_URL+"greeting/send";
	String GET_GREETINGS_URL = DataUtil.WS_URL+"greeting/getGreetings?eventId=";
	
	public String getGreetingSuggestion(EventTypeDTO eventTypeDTO) {
		WSConnection conn =new WSConnection();
		return conn.get(Greeting_SUGGESTION_URL+eventTypeDTO.getId());
	}

	public List<GreetingDTO> parseGreetingSuggestions(String result) {
		Gson json =new Gson();
		Type listType = new TypeToken<List<GreetingDTO>>() {
		}.getType();
		return json.fromJson(result, listType);
	}

	public String sendGreeting(EventCommentsDTO comment) {
		WSConnection conn =new WSConnection();
		Gson json = new GsonBuilder().setDateFormat("MMM dd, yyyy").create();
		String commentJson= json.toJson(comment);
		return conn.post(SEND_GREETING_URL,commentJson);
		
	}

	public boolean isGreetingSent(String result) {
		Gson json = new GsonBuilder().setDateFormat("MMM dd, yyyy").create();
		response res = json.fromJson(result, response.class);
		return !res.error;
		
	}

	public String getGreeting(UserEventDTO event) {
		WSConnection conn =new WSConnection();
		return conn.get(GET_GREETINGS_URL+event.getId());
	}

	public List<EventCommentsDTO> parseGreetings(String result) {
		Gson json = new GsonBuilder().setDateFormat("MMM dd, yyyy").create();
		Type listType = new TypeToken<List<EventCommentsDTO>>() {
		}.getType();
		return json.fromJson(result, listType);
	}

	

}
