package com.iwish.businesslayer;

import java.util.ArrayList;
import java.util.List;

import android.R.bool;
import android.app.Notification;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.iwish.datalayer.FileAccess;
import com.iwish.datalayer.WSConnection;
import com.iwish.dto.EventTypeDTO;
import com.iwish.dto.NotificationDTO;
import com.iwish.dto.UserDTO;
import com.iwish.dto.UserEventDTO;
import com.iwish.util.DataUtil;

import java.lang.reflect.Type;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpParams;

public class EventParser {

	String DELETE_URL = DataUtil.WS_URL+"event/remove";
	String TYPES_URL = DataUtil.WS_URL+"event/types";
	
	public String fb_id = "";
	public int id = 0;
	private String ADD_EVENT_URL = DataUtil.WS_URL+"event/add";
	private String EDIT_EVENT_URL = DataUtil.WS_URL+"event/edit";
	private String GET_EVENT_URL = DataUtil.WS_URL+"event/getEvent?id=";
	private Context context;

	public EventParser(Context context) {
		this.context = context;
	}

	public void setFb_id(String fb_id) {
		this.fb_id = fb_id;
	}

	public void setId(int id) {
		this.id = id;
	}

	

	public List<UserEventDTO> parseEvents(String result) {
		Gson json = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm")
				.create();
		Type listType = new TypeToken<List<UserEventDTO>>() {
		}.getType();
		ArrayList<UserEventDTO> eventList = json.fromJson(result, listType);
		return eventList;

	}
	
	

	public boolean deleteList(List<UserEventDTO> eventList) {
		Gson json = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm")
				.create();
		String out;
		String listJson = json.toJson(eventList);
		UserDTO user = new UserDTO();
		user.setFacebookUserId(fb_id);
		if (id != 0) {
			user.setId(id);
		}
		// user.setId(1);
		String userJson = json.toJson(user);

		if (checkNetworkConnection()) {
			WSConnection conn = new WSConnection();
			// List<NameValuePair> params=new ArrayList<NameValuePair>();
			// params.add(new BasicNameValuePair("eventList",listJson));
			// params.add(new BasicNameValuePair("user",userJson));

			if ((out = conn.post( DELETE_URL, listJson)) != null) {
				response res = json.fromJson(out, response.class);
				if (res.error)
					return false;
				else
					return true;

			}
		}
		return false;
	}

	public String getEventTypes() {

		if (checkNetworkConnection()) {
			WSConnection conn = new WSConnection();
			String out;
			if ((out = conn.get(TYPES_URL)) != null) {
				return out;
			}
		}
		return null;
	}

	public List<EventTypeDTO> parseTypes(String out) {
		// TODO Auto-generated method stub
		Gson json = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm")
				.create();
		Type listType = new TypeToken<ArrayList<EventTypeDTO>>() {
		}.getType();
		return json.fromJson(out, listType);
	}

	public boolean addEvent(UserEventDTO event) {

		if (checkNetworkConnection()) {
			WSConnection conn = new WSConnection();
			String out;
			Gson json = new GsonBuilder().setDateFormat(
					"yyyy-MM-dd'T'HH:mm").create();
			UserDTO user = new UserDTO();
			user.setFacebookUserId(fb_id);
			user.setId(id);
			String userJson = json.toJson(user);
			event.setUserDTO(user);
			String eventJson = json.toJson(event);
			if ((out = conn.post( ADD_EVENT_URL,
					eventJson)) != null) {
				response res = json.fromJson(out, response.class);
				return !res.error;
			}
		}
		return false;
	}

	public Boolean editEvent(UserEventDTO event) {
		if (checkNetworkConnection()) {
			WSConnection conn = new WSConnection();
			String out;
			Gson json = new GsonBuilder().setDateFormat(
					"yyyy-MM-dd'T'HH:mm").create();
			UserDTO user = new UserDTO();
			user.setFacebookUserId(fb_id);
			user.setId(id);
			String userJson = json.toJson(user);
			event.setUserDTO(user);
			String eventJson = json.toJson(event);
			if ((out = conn.post( EDIT_EVENT_URL,
					eventJson)) != null) {
				response res = json.fromJson(out, response.class);
				return !res.error;
			} else {
				return false;
			}
		}
		return false;
	}

	public ArrayList<NotificationDTO> getNotifications(UserDTO userDTO) {

		String url = DataUtil.WS_URL+"BroadCast/notifications?user_id="
				+ userDTO.getId();
		String result;
		ArrayList<NotificationDTO> notifications = null;

		if (checkNetworkConnection()) {

			WSConnection wsConnection = new WSConnection();
			result = wsConnection.get(url);
			Log.i("notifications", result);
			if ((!result.equals("error")) && (!result.equals("exception"))) {

				Gson gson = new GsonBuilder().setDateFormat("MMM dd, yyyy")
						.create();
				Type typeOfT = new TypeToken<ArrayList<NotificationDTO>>() {
				}.getType();
				notifications = gson.fromJson(result, typeOfT);
			}
		}

		return notifications;
	}

	public String notificationReceived(NotificationDTO notificationDTO) {

		String url = DataUtil.WS_URL+"BroadCast/received?notification_id="
				+ notificationDTO.getId();
		String result = "error";

		if (checkNetworkConnection()) {

			WSConnection wsConnection = new WSConnection();
			result = wsConnection.get(url);

			if ((!result.equals("error")) && (!result.equals("exception"))) {
				result = "success";
			}
		}
		return result;
	}

	public ArrayList<UserEventDTO> getFriendsEvents(UserDTO userDTO) {

		String url = DataUtil.WS_URL+"event/friends?user_email="
				+ userDTO.getEmail();

		if (checkNetworkConnection()) {

			WSConnection wsConnection = new WSConnection();
			String result = wsConnection.get(url);
			Log.i("kk",result);
			if (!(result.equals("exception"))) {

				Gson gson = new GsonBuilder().setDateFormat(
						"yyyy-MM-dd'T'HH:mm").create();
				Type listType = new TypeToken<ArrayList<UserEventDTO>>() {
				}.getType();
				
				return gson.fromJson(result, listType);
			}
		}

		return null;
	}

	public boolean checkNetworkConnection() {
		ConnectivityManager manager = (ConnectivityManager) context
				.getSystemService(context.CONNECTIVITY_SERVICE);
		NetworkInfo wifiNetworkInfo = manager
				.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		NetworkInfo mobileNetworkInfo = manager
				.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		if (wifiNetworkInfo.isConnected() || mobileNetworkInfo.isConnected()) {
			return true;
		}
		return false;
	}

	public String getEvent(int event_id) {
		if (checkNetworkConnection()) {
			WSConnection conn = new WSConnection();
			String out;
			if ((out = conn.get( GET_EVENT_URL+event_id)) != null) {
				return out;
			}
		}
		return null;
	}

	public UserEventDTO parseEvent(String result) {
		Gson json = new GsonBuilder().setDateFormat(
				"yyyy-MM-dd'T'HH:mm").create();
		return json.fromJson(result, UserEventDTO.class);
	}
	
	public boolean saveEvents (ArrayList<UserEventDTO> userEventDTOs,String key){
		
		FileAccess fileAccess = new FileAccess(context);
		return fileAccess.writeToFile(userEventDTOs, key);
		
	}
	
	public List<UserEventDTO> readEvents(String key){
		
		FileAccess fileAccess = new FileAccess(context);
		return fileAccess.readFromFile(null, key);
	}
	
	public boolean saveNotifications (ArrayList<NotificationDTO> notificationDTOs, String key){
		FileAccess fileAccess = new FileAccess(context);
		return fileAccess.writeToFile(notificationDTOs, key);
	}
	public List<NotificationDTO> readNotifications(String key){
		FileAccess fileAccess = new FileAccess(context);
		return fileAccess.readFromFile(null, key);
	}

}

class response {

	boolean error;
	String responseJson;
}
