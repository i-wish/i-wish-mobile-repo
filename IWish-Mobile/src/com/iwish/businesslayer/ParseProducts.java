package com.iwish.businesslayer;

import java.lang.reflect.Type;
import java.util.ArrayList;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.iwish.dto.Page;
import com.iwish.datalayer.WSConnection;
import com.iwish.dto.CategoryDTO;
import com.iwish.dto.CompanyDTO;
import com.iwish.dto.ProductDTO;
import com.iwish.util.DataUtil;

public class ParseProducts {

	Context context;

	public ParseProducts(Context context) {
		this.context = context;
	}

	public boolean checkNetworkConnection() {
		ConnectivityManager manager = (ConnectivityManager) context
				.getSystemService(context.CONNECTIVITY_SERVICE);
		NetworkInfo wifiNetworkInfo = manager
				.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		NetworkInfo mobileNetworkInfo = manager
				.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		if (wifiNetworkInfo.isConnected() || mobileNetworkInfo.isConnected()) {
			return true;
		}
		return false;
	}

	public ArrayList<CategoryDTO> parseCategories(String url) {

		if (checkNetworkConnection()) {
			ArrayList<CategoryDTO> categoryDTOs;
			WSConnection wsConnection = new WSConnection();
			String json = wsConnection.get(url);
			if (!(json.equals("exception"))) {
				Gson jsonObj = new Gson();
				Type typeOfT = new TypeToken<ArrayList<CategoryDTO>>() {
				}.getType();
				Log.i("tag", "############################   " + json);
				categoryDTOs = jsonObj.fromJson(json, typeOfT);
			} else {
				categoryDTOs = new ArrayList<CategoryDTO>();
			}
			return categoryDTOs;
		}
		return null;
	}

	public ArrayList<CompanyDTO> parseCompanyDTOs(String url) {

		ArrayList<CompanyDTO> companyDTOs;
		if (checkNetworkConnection()) {
			WSConnection wsConnection = new WSConnection();
			String json = wsConnection.get(url);
			if (!(json.equals("exception"))) {
				Gson jsonObj = new Gson();
				Type typeOfT = new TypeToken<ArrayList<CompanyDTO>>() {
				}.getType();
				Log.i("tag", "############################   " + json);
				companyDTOs = jsonObj.fromJson(json, typeOfT);
			} else {
				companyDTOs = new ArrayList<CompanyDTO>();
			}
			return companyDTOs;
		}
		return null;
	}

	public Page parseProducts(String url) {

		Page page;
		if (checkNetworkConnection()) {
			WSConnection wsConnection = new WSConnection();
			String json = wsConnection.get(url);
			if (!(json.equals("exception"))) {
				Gson jsonObj = new Gson();
				Type typeOfT = new TypeToken<Page>() {
				}.getType();
				page = jsonObj.fromJson(json, typeOfT);
			} else {
				page = new Page();
			}
			return page;

		}
		return null;
	}

	public ProductDTO getProductImage(ProductDTO productDTO) {

		String url = DataUtil.WS_URL + "products/image?product_id="
				+ productDTO.getId();
		if (checkNetworkConnection()) {

			WSConnection wsConnection = new WSConnection();
			String result = wsConnection.get(url);
			if (!result.equals("exception")) {
				Gson gson = new Gson();
				ProductDTO newProduct = gson.fromJson(result, ProductDTO.class);
				return newProduct;
			}
		}
		return null;
	}

	public CategoryDTO getCategoryImage(CategoryDTO categoryDTO) {

		String url = DataUtil.WS_URL + "products/category/image?category_id="
				+ categoryDTO.getId();
		if (checkNetworkConnection()) {

			WSConnection wsConnection = new WSConnection();
			String result = wsConnection.get(url);
			if (!result.equals("exception")) {
				Gson gson = new Gson();
				CategoryDTO newCategory = gson.fromJson(result, CategoryDTO.class);
				return newCategory;
			}
		}
		return null;

	}

}
