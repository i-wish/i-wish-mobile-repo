package com.iwish.businesslayer;

import java.lang.reflect.Type;
import java.util.ArrayList;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.iwish.datalayer.FileAccess;
import com.iwish.datalayer.WSConnection;
import com.iwish.dto.ProductDTO;
import com.iwish.dto.UserDTO;
import com.iwish.dto.UserEventDTO;
import com.iwish.util.DataUtil;

public class FriendParser {

	String url;
	Context context;

	public FriendParser(Context context) {
		this.context = context;
	}

	

	public boolean checkNetworkConnection() {
		ConnectivityManager manager = (ConnectivityManager) context
				.getSystemService(context.CONNECTIVITY_SERVICE);
		NetworkInfo wifiNetworkInfo = manager
				.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		NetworkInfo mobileNetworkInfo = manager
				.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		if (wifiNetworkInfo.isConnected() || mobileNetworkInfo.isConnected()) {
			return true;
		}
		return false;
	}

	public ArrayList<UserDTO> getFriends(String email) {
		url = DataUtil.WS_URL+"friend/getfriends?email="
				+ email;
		if (checkNetworkConnection()) {
			WSConnection wsConnection = new WSConnection();
			String jsonString = wsConnection.get(url);
			Log.i("friends", jsonString);
			Gson gson = new GsonBuilder().setDateFormat("MMM dd, yyyy")
					.create();
			Type typeOfT = new TypeToken<ArrayList<UserDTO>>() {
			}.getType();
			ArrayList<UserDTO> friends = gson.fromJson(jsonString, typeOfT);

			FileAccess fileAccess = new FileAccess(context);
			fileAccess.writeToFile(friends, DataUtil.FRIENDS);

			return friends;
		}
		return null;
	}

	public ArrayList<ProductDTO> getFriendWishlist(String email) {

		url = DataUtil.WS_URL+"products/wishlist?email="
				+ email;
		if (checkNetworkConnection()) {
			WSConnection wsConnection = new WSConnection();
			String jsonString = wsConnection.get(url);

			Gson gson = new Gson();
			Type typeOfT = new TypeToken<ArrayList<ProductDTO>>() {
			}.getType();
			ArrayList<ProductDTO> friendWishlist = gson.fromJson(jsonString,
					typeOfT);
			return friendWishlist;
		}
		return null;
	}

	public ArrayList<UserEventDTO> getFriendEvents(String facebookID) {

		url = DataUtil.WS_URL+"event/get?fb_id="
				+ facebookID;
		if (checkNetworkConnection()) {
			WSConnection wsConnection = new WSConnection();
			String jsonString = wsConnection.get(url);

			Gson gson = new GsonBuilder().setDateFormat(
					"yyyy-MM-dd'T'HH:mm").create();
			Type typeOfT = new TypeToken<ArrayList<UserEventDTO>>() {
			}.getType();
			ArrayList<UserEventDTO> friendEvents = gson.fromJson(jsonString,
					typeOfT);
			return friendEvents;
		}
		return null;
	}

	public void saveFriends(ArrayList<UserDTO> userDTOs) {
		FileAccess fileAccess = new FileAccess(context);
		fileAccess.writeToFile(userDTOs, DataUtil.FRIENDS);
	}

	public ArrayList<UserDTO> getFriends() {
		FileAccess fileAccess = new FileAccess(context);
		return (ArrayList<UserDTO>) fileAccess.readFromFile(UserDTO.class,
				DataUtil.FRIENDS);
	}

}
