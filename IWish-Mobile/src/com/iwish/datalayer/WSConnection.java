package com.iwish.datalayer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpParams;

import android.util.Log;

import com.google.gson.Gson;

public class WSConnection {

	public String get(String url) {

		String json = "";
		try {
			HttpClient client = new DefaultHttpClient();
			HttpGet get = new HttpGet(url);
			HttpResponse response = client.execute(get);

			if (response.getStatusLine().getStatusCode() == 401) {
				json = "error";
			} else {
				InputStream inputStream = null;
				final HttpEntity entity = response.getEntity();
				if (entity == null) {
					Log.w("entity", "The response has no entity.");
				} else {
					inputStream = response.getEntity().getContent();
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(inputStream));

					StringBuilder sb = new StringBuilder();
					String line = null;

					while ((line = reader.readLine()) != null) {
						sb.append(line);
					}

					json = sb.toString();
					inputStream.close();

				}
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		}

		return json;
	}

	/*
	 * public String post(String url,List<NameValuePair> params) {
	 * 
	 * DefaultHttpClient httpClient = new DefaultHttpClient(); HttpPost httpPost
	 * = new HttpPost(url ); Gson json = new Gson(); String response=""; try {
	 * httpPost.setEntity(new UrlEncodedFormEntity(params)); } catch
	 * (UnsupportedEncodingException e1) { // TODO Auto-generated catch block
	 * e1.printStackTrace(); } HttpResponse httpResponse; try { httpResponse =
	 * httpClient.execute(httpPost);
	 * 
	 * HttpEntity httpEntity = httpResponse.getEntity(); InputStream is;
	 * 
	 * is = httpEntity.getContent();
	 * 
	 * BufferedReader input = new BufferedReader(new InputStreamReader(is));
	 * String line = null;
	 * 
	 * while ((line = input.readLine()) != null) {
	 * 
	 * response+=line; }
	 * 
	 * input.close(); } catch (ClientProtocolException e) { // TODO
	 * Auto-generated catch block e.printStackTrace(); } catch (IOException e) {
	 * // TODO Auto-generated catch block e.printStackTrace(); } return
	 * response.toString();
	 * 
	 * 
	 * }
	 */

	public String post(String url, String request) {
		DefaultHttpClient httpClient = new DefaultHttpClient();
		HttpPost httpPost = new HttpPost(url);
		Gson json = new Gson();
		String response = "";

		/*
		 * HttpParams paramsReq=httpClient.getParams(); for (NameValuePair param
		 * : params) { paramsReq.setParameter(param.getName(),
		 * param.getValue());
		 * 
		 * }
		 */

		// httpPost.setParams(paramsReq);
		try {
			httpPost.setEntity(new StringEntity(request));
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		HttpResponse httpResponse;
		try {
			httpResponse = httpClient.execute(httpPost);

			HttpEntity httpEntity = httpResponse.getEntity();
			InputStream is;

			is = httpEntity.getContent();

			BufferedReader input = new BufferedReader(new InputStreamReader(is));
			String line = null;

			while ((line = input.readLine()) != null) {

				response += line;
			}

			input.close();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return response.toString();

	}

}
