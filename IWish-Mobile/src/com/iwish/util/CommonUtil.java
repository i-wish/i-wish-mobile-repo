package com.iwish.util;

import com.iwish.activities.R;

import android.app.Fragment;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView.FindListener;
import android.widget.TextView;
import android.widget.Toast;

public class CommonUtil {
	
	public static boolean checkNetworkConnection(Context context) {
		ConnectivityManager manager = (ConnectivityManager) context
				.getSystemService(
						context.CONNECTIVITY_SERVICE);
		NetworkInfo wifiNetworkInfo = manager
				.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		NetworkInfo mobileNetworkInfo = manager
				.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		if (wifiNetworkInfo.isConnected() || mobileNetworkInfo.isConnected()) {
			return true;
		}
		return false;
	}
	
	public static void showConnectionToast(final Fragment fragment , final LayoutInflater inflater, final ViewGroup container,
			final Bundle savedInstanceState){
		final Toast connectionToast = new Toast(fragment.getActivity());
		View view = inflater.inflate(R.layout.connection_toast,
				container, false);
		TextView tapToRetry = (TextView) view.findViewById(R.id.TapToRetry);
		tapToRetry.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Log.i("jjjj", "clickeeeeeeeeeeeeeeeeeeeeeeed");
				fragment.onCreateView(inflater, container, savedInstanceState);
				
				
			}
		});
		connectionToast.setDuration(Toast.LENGTH_LONG);
		connectionToast.setView(view);
		connectionToast.show();
		
	}

}
