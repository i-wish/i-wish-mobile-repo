package com.iwish.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class StoreOwnerDTO extends AbstractDTO implements GenericUser, Serializable, Comparable<UserDTO>  {
	private Integer id;
	private String name;
	private String email;
	private String firstName;
	private String lastName;
	private String password;
	private Date birthday;
	private byte[] picture;
	private Set products = new HashSet(0);

	private boolean isAdmin;

	public StoreOwnerDTO() {
	}

	public StoreOwnerDTO(String name, String email, String password) {
		this.name = name;
		this.email = email;
		this.password = password;
	}

	public StoreOwnerDTO(String name, String email, String usercol,
			String firstName, String lastName, String password, Date birthday, Set products) {
		this.name = name;
		this.email = email;
		this.firstName = firstName;
		this.lastName = lastName;
		this.password = password;
		this.birthday = birthday;
		this.products = products;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getRegisterationId() {
		return null;
	}

	@Override
	public void setRegisterationId(String registerationId) {

	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getBirthday() {
		return this.birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public byte[] getPicture() {
		return picture;
	}

	public void setPicture(byte[] picture) {
		this.picture = picture;
	}

	@Override
	public Set getUserEvents() {
		return null;
	}

	@Override
	public void setUserEvents(Set userEvents) {

	}

	@Override
	public Set getUserProducts() {
		return null;
	}

	@Override
	public void setUserProducts(Set userProducts) {

	}

	@Override
	public Set getProductReserves() {
		return null;
	}

	@Override
	public void setProductReserves(Set productReserves) {

	}

	@Override
	public Set getFriendListsForUserId() {
		return null;
	}

	@Override
	public void setFriendListsForUserId(Set friendListsForUserId) {

	}

	@Override
	public Set getFriendListsForFriendId() {
		return null;
	}

	@Override
	public void setFriendListsForFriendId(Set friendListsForFriendId) {

	}

	@Override
	public String getFacebookUserId() {
		return null;
	}

	@Override
	public void setFacebookUserId(String facebookUserId) {

	}

	@Override
	public String getFacebookUserAccessToken() {
		return null;
	}

	@Override
	public void setFacebookUserAccessToken(String facebookUserAccessToken) {

	}

	public Set getProducts() {
		return this.products;
	}

	public void setProducts(Set storeOwnerProducts) {
		this.products = storeOwnerProducts;
	}

	public boolean isAdmin() {
		return isAdmin;
	}

	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	@Override
	public String getValueString() {
		return getName();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		StoreOwnerDTO that = (StoreOwnerDTO) o;

		if (id != null ? !id.equals(that.id) : that.id != null) return false;

		return true;
	}

	@Override
	public int hashCode() {
		return id != null ? id.hashCode() : 0;
	}

	@Override
	public int compareTo(UserDTO o) {
		return name.compareTo(o.getName());
	}
}
