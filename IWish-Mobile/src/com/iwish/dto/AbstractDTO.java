package com.iwish.dto;

public abstract class AbstractDTO {

	public abstract Integer getId(); 
	
	@Override
	public String toString() {
		return "DTO : "+getClass().getName() +" - "+getValueString();
	}

	public abstract String getValueString();

	
}
