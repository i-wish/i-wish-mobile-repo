package com.iwish.dto;

import java.io.Serializable;

public class UserReserveProduct implements Serializable{
	
	UserDTO userDTO;
	Double participate;
	String comment;

	public UserDTO getUserDTO() {
		return userDTO;
	}

	public Double getParticipate() {
		return participate;
	}

	public void setUserDTO(UserDTO userDTO) {
		this.userDTO = userDTO;
	}

	public void setParticipate(Double participate) {
		this.participate = participate;
	}
	
	public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
