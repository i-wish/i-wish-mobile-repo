package com.iwish.dto;

import java.util.Date;
import java.util.Set;

/**
 * Created by eltntawy on 08/06/15.
 */
public interface GenericUser {
    public Integer getId() ;

    public void setId(Integer id) ;

    public String getName() ;

    public void setName(String name) ;

    public String getRegisterationId() ;

    public void setRegisterationId(String registerationId) ;
    public String getEmail() ;

    public void setEmail(String email) ;

    public String getFirstName();

    public void setFirstName(String firstName) ;

    public String getLastName() ;

    public void setLastName(String lastName);

    public String getPassword() ;

    public void setPassword(String password) ;

    public Date getBirthday() ;

    public void setBirthday(Date birthday) ;

    public byte[] getPicture() ;

    public void setPicture(byte[] picture) ;

    public Set getUserEvents() ;

    public void setUserEvents(Set userEvents) ;

    public Set getUserProducts() ;

    public void setUserProducts(Set userProducts) ;

    public Set getProductReserves() ;

    public void setProductReserves(Set productReserves) ;

    public Set getFriendListsForUserId() ;

    public void setFriendListsForUserId(Set friendListsForUserId) ;

    public Set getFriendListsForFriendId() ;

    public void setFriendListsForFriendId(Set friendListsForFriendId) ;

    public String getFacebookUserId() ;

    public void setFacebookUserId(String facebookUserId) ;

    public String getFacebookUserAccessToken() ;

    public void setFacebookUserAccessToken(String facebookUserAccessToken);

    public boolean isAdmin();

    public void setAdmin(boolean isAdmin);

}
