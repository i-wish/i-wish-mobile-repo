package com.iwish.controller;

import java.util.List;

import com.iwish.activities.HomeActivity;
import com.iwish.businesslayer.GreetingParser;
import com.iwish.dto.EventCommentsDTO;
import com.iwish.dto.EventTypeDTO;
import com.iwish.dto.GreetingDTO;
import com.iwish.dto.UserDTO;
import com.iwish.dto.UserEventDTO;
import com.iwish.util.DataUtil;

import android.content.Context;
import android.content.SharedPreferences;

public class GreetingController {

	

	public Context context;

	public GreetingController(Context context) {
		this.context = context;
	}

	public String getGreetingSuggestion(EventTypeDTO eventTypeDTO) {
		GreetingParser parser = new GreetingParser();
		
		return parser.getGreetingSuggestion(eventTypeDTO);

	}

	public List<GreetingDTO> parseGreetingSuggestions(String result) {
		GreetingParser parser = new GreetingParser();
		return parser.parseGreetingSuggestions(result);

	}

	public String sendGreeting(EventCommentsDTO comment) {
		GreetingParser parser = new GreetingParser();
		
		return parser.sendGreeting(comment);
	}

	public boolean parseSendGreeting(String result) {
		GreetingParser parser = new GreetingParser();
		return parser.isGreetingSent(result);
	}

	public String getGreetings(UserEventDTO event) {
		GreetingParser parser = new GreetingParser();
		
		return parser.getGreeting(event);
	}

	public List<EventCommentsDTO> parseGreetings(String result) {
		GreetingParser parser = new GreetingParser();
		return parser.parseGreetings(result);
	}

}
