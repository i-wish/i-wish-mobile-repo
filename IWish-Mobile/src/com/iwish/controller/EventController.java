package com.iwish.controller;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.SharedPreferences;

import com.iwish.activities.HomeActivity;
import com.iwish.businesslayer.EventParser;
import com.iwish.datalayer.WSConnection;
import com.iwish.dto.EventTypeDTO;
import com.iwish.dto.NotificationDTO;
import com.iwish.dto.UserDTO;
import com.iwish.dto.UserEventDTO;
import com.iwish.fragments.events;
import com.iwish.util.DataUtil;

public class EventController {

	public events eventFragment;
	public Context context;
	
	String fb_id;

	String event_url = DataUtil.WS_URL+"event/get?fb_id=";

	int id;

	public EventController(Context context) {
		this.context=context;
	}

	public EventController(events eventFragment) {
		this.eventFragment = eventFragment;
		

	}

	public String getFb_id() {
		SharedPreferences pref = context.getSharedPreferences(
				DataUtil.USER_PREF, 0);
		fb_id = pref.getString(DataUtil.USER_FB_ID, null);
		return fb_id;

	}

	public int getId() {
		SharedPreferences pref = context.getSharedPreferences(
				DataUtil.USER_PREF, 0);
		id = pref.getInt(DataUtil.USER_ID, 0);
		return id;
	}

	

	public boolean deleteEvents(List<UserEventDTO> eventList) {
		EventParser parser = new EventParser(context);
		
		parser.fb_id = getFb_id();
		return parser.deleteList(eventList);

	}

	public String getEventsType() {
		EventParser parser = new EventParser(context);
		
		parser.fb_id = getFb_id();
		return parser.getEventTypes();
	}

	public List<EventTypeDTO> ParseEventTypes(String result) {
		EventParser parser = new EventParser(context);
		return parser.parseTypes(result);
	}

	public boolean addEvent(UserEventDTO event) {
		EventParser parser = new EventParser(context);
		
		parser.fb_id = getFb_id();
		parser.id = getId();
		return parser.addEvent(event);
	}

	public Boolean editEvent(UserEventDTO event) {
		EventParser parser = new EventParser(context);
		
		parser.fb_id = getFb_id();
		parser.id = getId();
		return parser.editEvent(event);

	}

	public ArrayList<NotificationDTO> getNotifications(UserDTO userDTO) {
		EventParser eventParser = new EventParser(context);
		return eventParser.getNotifications(userDTO);
	}

	public String notificationReceived(NotificationDTO notificationDTO) {
		EventParser eventParser = new EventParser(context);
		return eventParser.notificationReceived(notificationDTO);
	}

	public String getEvents() {

		WSConnection ws = new WSConnection();
		SharedPreferences pref = context.getSharedPreferences(
				DataUtil.USER_PREF, 0);
		String fb_id = pref.getString(DataUtil.USER_FB_ID, null);
		return ws.get( event_url + fb_id);

	}

	public List<UserEventDTO> parseEvents(String result) {
		EventParser parser = new EventParser(context);
		return parser.parseEvents(result);

	}

	public ArrayList<UserEventDTO> getFriendsEvents(UserDTO userDTO) {
		EventParser eventParser = new EventParser(context);
		return eventParser.getFriendsEvents(userDTO);
	}

	public String getEvent(int event_id) {
		EventParser parser = new EventParser(context);
		
		return parser.getEvent(event_id);
	}

	public UserEventDTO parseEvent(String result) {
		EventParser eventParser = new EventParser(context);
		return eventParser.parseEvent(result);
	}

	public List<UserEventDTO> readEvent(String key) {
		EventParser eventParser = new EventParser(context);
		return eventParser.readEvents(key);
	}

	public boolean saveEvents(ArrayList<UserEventDTO> userEventDTOs, String key) {
		EventParser eventParser = new EventParser(context);
		return eventParser.saveEvents(userEventDTOs, key);
	}

	public List<NotificationDTO> readNotifications(String key) {
		EventParser eventParser = new EventParser(context);
		return eventParser.readNotifications(key);
	}

	public boolean saveNotifications(
			ArrayList<NotificationDTO> notificationDTOs, String key) {
		EventParser eventParser = new EventParser(context);
		return eventParser.saveNotifications(notificationDTOs, key);
	}

}
