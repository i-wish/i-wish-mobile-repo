package com.iwish.controller;

import java.util.List;

import com.iwish.businesslayer.PlaceParser;
import com.iwish.dto.EventTypeDTO;
import com.iwish.dto.PlaceDTO;
import com.iwish.util.DataUtil;

import android.content.Context;
import android.content.SharedPreferences;

public class PlaceController {
	
	

	Context context;
	public PlaceController(Context context) {
		this.context=context;
	}
	

	public String getPlaces(EventTypeDTO type) {
		PlaceParser parser=new PlaceParser();
		
		return parser.getPlaces(type);
		
	}

	public List<PlaceDTO> parsePlaces(String result) {
		PlaceParser parser=new PlaceParser();
		return parser.parsePlaces(result);
	}
	
	

}
