package com.iwish.controller;

import java.util.ArrayList;

import com.iwish.asyncTasks.UserInfoTask;
import com.iwish.businesslayer.ParseWishlist;
import com.iwish.dto.ProductDTO;
import com.iwish.dto.UserDTO;

import com.iwish.util.DataUtil;

import android.content.Context;
import android.content.SharedPreferences;


public class UserWishListController {
	
	public Context context;
	
	
	public UserWishListController(Context context) {
		this.context = context;
	}
	
	
	public ArrayList<ProductDTO> getWishList(UserDTO userDTO){
		ParseWishlist parseWishlist = new ParseWishlist(context);
		ArrayList<ProductDTO> userWishlist = parseWishlist.getWishList(userDTO);
		
		if (userWishlist != null){
			//write in file
			WishlistController wishlistController = new WishlistController(context);
			wishlistController.saveWishlist(userWishlist);
		}
		return userWishlist;
	}
	


}
