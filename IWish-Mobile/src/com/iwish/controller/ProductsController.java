package com.iwish.controller;

import java.util.ArrayList;

import android.content.Context;
import android.content.SharedPreferences;

import com.iwish.dto.CategoryDTO;
import com.iwish.dto.CompanyDTO;
import com.iwish.dto.Page;
import com.iwish.dto.ProductDTO;
import com.iwish.businesslayer.ParseProducts;
import com.iwish.util.DataUtil;



public class ProductsController {

	Context context;
	
	
	public ProductsController(Context context) {
		this.context = context;
	}
	
	

	public ArrayList<CategoryDTO> getCategories() {

		
		String url = DataUtil.WS_URL+"products/categories";
		
		

		ParseProducts parseProducts = new ParseProducts(context);
		ArrayList<CategoryDTO> categoryDTOs = parseProducts
				.parseCategories(url);
		return categoryDTOs;
	}

	public ArrayList<CompanyDTO> getCompanyDTOs(int categoryId) {

		
		String url =DataUtil.WS_URL+"products/companies?categoryId="
				+ categoryId;

//		String url = "http://"
//				+ "10.145.43.233"
//				+ ":8080/IWish-Enterprise-Application-ws/rest/products/companies?categoryId="
//				+ categoryId;
		
		ParseProducts parseProducts = new ParseProducts(context);
		ArrayList<CompanyDTO> companyDTOs = parseProducts.parseCompanyDTOs(url);
		return companyDTOs;

	}

	public Page getProductDTOs(int categoryId, int companyId,
			int patchStart, int patchSize) {

		
		String url = DataUtil.WS_URL+"products/productslist?categoryId="
				+ categoryId + "&companyId=" + companyId + "&patchStart="
				+ patchStart + "&patchSize=" + patchSize;
		
		
		
		ParseProducts parseProducts = new ParseProducts(context);
		Page page = parseProducts.parseProducts(url);
		return page;

	}
	
	public ProductDTO getProductImage (ProductDTO productDTO){
		ParseProducts parseProducts = new ParseProducts(context);
		return parseProducts.getProductImage(productDTO);
	}
	
	public CategoryDTO getCategoryImage(CategoryDTO categoryDTO){
		ParseProducts parseProducts = new ParseProducts(context);
		return parseProducts.getCategoryImage(categoryDTO);
	}

}
