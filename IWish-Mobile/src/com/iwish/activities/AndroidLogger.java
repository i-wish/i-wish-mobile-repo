package com.iwish.activities;

import java.io.File;
import android.os.Environment;
import de.mindpipe.android.logging.log4j.LogConfigurator;
import org.apache.log4j.Level;





public class AndroidLogger {
	 public static void configure() {
		 
	        final LogConfigurator logConfigurator = new LogConfigurator();
	                
	        logConfigurator.setFileName(Environment.getExternalStorageDirectory().toString() + File.separator + "logger.txt");
	        logConfigurator.setRootLevel(Level.DEBUG);
	        // Set log level of a specific logger
	        logConfigurator.setLevel("org.apache", Level.ERROR);
	        logConfigurator.setMaxBackupSize(5);
	        logConfigurator.setMaxFileSize(5*1024*1024);
	        logConfigurator.configure();
	     
	    }

}
