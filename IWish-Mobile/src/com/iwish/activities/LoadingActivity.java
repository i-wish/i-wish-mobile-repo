package com.iwish.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.iwish.util.DataUtil;

public class LoadingActivity extends Activity {

	String logged;
	SharedPreferences pref;
	int count = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_loading);

		pref = getSharedPreferences(DataUtil.LOGGEDIN,0 );
		logged=pref.getString(DataUtil.LOGGEDIN, "false");
		getActionBar().hide();
		new Thread(new Runnable() {

			@Override
			public void run() {
				while (!logged.equals("true") && count < 40) {
					try {
						Thread.sleep(1000);
						count++;
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					logged = pref.getString(DataUtil.LOGGEDIN, "false");

				}
				
				if (logged.equals("true")) {
					Log.i("logged", "true");
					Intent intent = new Intent(getApplicationContext(),
							HomeActivity.class);
					startActivity(intent);
					finish();
				} else {

					LoadingActivity.this.runOnUiThread(new Runnable() {

						@Override
						public void run() {
							Toast.makeText(LoadingActivity.this, "time out",
									Toast.LENGTH_LONG).show();
							Intent intent = new Intent(getApplicationContext(),
									Main_Page.class);
							intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							startActivity(intent);
							finish();
						}
					});
				}

			}
		}).start();

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.loading, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
