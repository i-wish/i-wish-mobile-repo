package com.iwish.activities;

import static com.iwish.activities.CommonUtilites.EXTRA_MESSAGE;

import java.lang.reflect.Type;
import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.facebook.Session;
import com.google.android.gcm.GCMRegistrar;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.iwish.adapter.NavDrawerListAdapter;
import com.iwish.dto.NavDrawerItem;
import com.iwish.dto.NotificationDTO;
import com.iwish.dto.UserDTO;
import com.iwish.fragments.CategoriesFragment;
import com.iwish.fragments.EventLoadingFragment;
import com.iwish.fragments.FriendListFragment;
import com.iwish.fragments.HomeFragment;
import com.iwish.fragments.NewEventFragment;
import com.iwish.fragments.WishListFragment;
import com.iwish.fragments.NotificationsFragment;
import com.iwish.fragments.ProductsFragment;
import com.iwish.fragments.events;
import com.iwish.fragments.ProfileFragment;
import com.iwish.util.DataUtil;

public class HomeActivity extends BaseActivity {

	private DrawerLayout mDrawerLayout;
	public ListView mDrawerList;
	private ActionBarDrawerToggle mDrawerToggle;
	public static Context HOME_CONTEXT;

	public boolean backFromServer;

	// nav drawer title
	public CharSequence mDrawerTitle;

	// used to store app title
	private CharSequence mTitle;

	// slide menu items
	private String[] navMenuTitles;
	private TypedArray navMenuIcons;
	private LinearLayout linearLayout;
	private ArrayList<NavDrawerItem> navDrawerItems;
	private NavDrawerListAdapter adapter;
	Fragment fragment = null;
	ImageView userImage;
	TextView userName;

	RegisterServerTask register;
	String regId;

	public boolean wishlistOpened = false;
	public boolean eventOpened = false;
	String fragmentTag;

	int eventId = 0;

	private TypedArray navMenuIconsSelected;
	private int selectedIdx=0;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);

		HOME_CONTEXT = this;
		mTitle = mDrawerTitle = getTitle();

		userName = (TextView) findViewById(R.id.user);
		userImage = (ImageView) findViewById(R.id.user_image);
		ImageView ic_edit = (ImageView) findViewById(R.id.ic_edit);
		ic_edit.setVisibility(View.GONE);

		SharedPreferences pref = getSharedPreferences(DataUtil.USER_PREF, 0);
		String userJson = pref.getString(DataUtil.USER_JSON, null);
		Gson json = new GsonBuilder().setDateFormat("MMM dd, yyyy").create();
		Type typeOfT = new TypeToken<UserDTO>() {
		}.getType();
		UserDTO userDTO = json.fromJson(userJson, typeOfT);

		userName.setText(userDTO.getName());

		if (userDTO.getPicture() != null) {
			Bitmap bm = BitmapFactory.decodeByteArray(userDTO.getPicture(), 0,
					userDTO.getPicture().length);
			if (bm != null) {
				Bitmap roundedImage = getRoundedShape(bm);
				userImage.setImageBitmap(roundedImage);
			}
		}

		// load slide menu items
		navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);

		// nav drawer icons from resources
		navMenuIcons = getResources()
				.obtainTypedArray(R.array.nav_drawer_icons);

		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.list_slidermenu);
		linearLayout = (LinearLayout) findViewById(R.id.layout);

		navDrawerItems = new ArrayList<NavDrawerItem>();
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[0], navMenuIcons
				.getResourceId(0, -1)));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[1], navMenuIcons
				.getResourceId(1, -1)));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[2], navMenuIcons
				.getResourceId(2, -1)));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[3], navMenuIcons
				.getResourceId(3, -1)));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[4], navMenuIcons
				.getResourceId(4, -1)));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[5], navMenuIcons
				.getResourceId(5, -1)));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[6], navMenuIcons
				.getResourceId(6, -1)));

		navMenuIcons.recycle();

		adapter = new NavDrawerListAdapter(getApplicationContext(), 0, 0,
				navDrawerItems);
		mDrawerList.setAdapter(adapter);
		
		// if (getIntent().getExtras() != null
		// && getIntent().getExtras().containsKey(DataUtil.EVENT_ID)) {

		Intent intent = getIntent();
		Bundle bundle = null;
		if ((bundle = intent.getExtras()) != null) {
			eventId = bundle.getInt(DataUtil.EVENT_ID);

			if (eventId != 0) {
				Log.i("Notifi", "Notification came");
				int event_id = getIntent().getExtras()
						.getInt(DataUtil.EVENT_ID);
				EventLoadingFragment loading = new EventLoadingFragment();
				loading.event_id = event_id;

				getFragmentManager().beginTransaction()
						.replace(R.id.frame_container, loading)
						.addToBackStack(null).commit();
			}
		} else {

			Log.i("Notifi", "Notification did come");
			fragment = new HomeFragment();
			ic_add.setVisibility(View.GONE);
			FragmentManager fragmentManager = getFragmentManager();
			fragmentManager.beginTransaction()
					.replace(R.id.frame_container, fragment, fragmentTag)
					.addToBackStack(fragmentTag).commit();
		}

		// update selected item and title, then close the drawer
		mDrawerList.setItemChecked(0, true);
		mDrawerList.setSelection(0);
		title.setText(navMenuTitles[0]);

		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
				R.drawable.ic_drawer, R.string.app_name, R.string.app_name) {

			@Override
			public void onDrawerClosed(View drawerView) {
				invalidateOptionsMenu();
			}

			@Override
			public void onDrawerOpened(View drawerView) {
				invalidateOptionsMenu();
			}
		};

		mDrawerLayout.setDrawerListener(mDrawerToggle);

		ic_drawer.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (mDrawerLayout.isDrawerOpen(linearLayout))
					mDrawerLayout.closeDrawer(linearLayout);
				else
					mDrawerLayout.openDrawer(linearLayout);
			}
		});

		title.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (mDrawerLayout.isDrawerOpen(linearLayout))
					mDrawerLayout.closeDrawer(linearLayout);
				else
					mDrawerLayout.openDrawer(linearLayout);
			}
		});

		mDrawerList.setOnItemClickListener(new SlideMenuListener());

		ic_notification.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				fragment = new NotificationsFragment();
				if (fragment != null) {
					android.app.FragmentManager fragmentManager = getFragmentManager();
					fragmentManager.beginTransaction()
							.replace(R.id.frame_container, fragment).commit();
				} else {
					// error in creating fragment
					Log.e("MainActivity", "Error in creating fragment");
				}
			}
		});

		// Make sure the device has the proper dependencies.
		GCMRegistrar.checkDevice(this);
		try {
			if (!pref.getBoolean(DataUtil.REGISTERED, true)) {

				registerReceiver(mHandleMessageReceiver, new IntentFilter(
						CommonUtilites.DISPLAY_MESSAGE_ACTION));
				Editor editor = pref.edit();
				editor.putBoolean(DataUtil.REGISTERED, true);
				editor.commit();
			}
		} catch (Exception ex) {

		}

		// Get GCM registration id
		regId = GCMRegistrar.getRegistrationId(this);

		RegisterServerTask register = new RegisterServerTask();
		// Toast.makeText(getApplicationContext(), regId, Toast.LENGTH_LONG);
		// Log.i("RegID", regId);
		// Check if regid already presents
		if (regId.equals("")) { // Registration is not present, register now
								// with GCM
			GCMRegistrar.register(this, CommonUtilites.SENDER_ID);
			register.execute();
		} else { // Device is alreadyregistered on GCM
			if (!GCMRegistrar.isRegisteredOnServer(this)) {
				// Toast.makeText(getApplicationContext(),
				// "Already registered with GCM", Toast.LENGTH_LONG).show();
				register.execute();
			}
			/*
			 * else { // Try to register again, but not in the UI thread. //
			 * It's also necessary to cancel the thread onDestroy(), // hence
			 * the use of AsyncTask insteadof a raw thread. final Context
			 * context = this; mRegisterTask = new AsyncTask<Void, Void, Void>()
			 * {
			 * 
			 * @Override protected Void doInBackground(Void... params) { //
			 * Registeron our server // On server creates a new user
			 * ServerUtilities.register(context, regId); return null; }
			 * 
			 * @Override protected void onPostExecute(Void result) {
			 * mRegisterTask = null; }
			 * 
			 * }; mRegisterTask.execute(null, null, null); }
			 * 
			 * // }
			 */
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.home, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {

		// set visibilty of items in action bar false
		boolean mDrawerOpen = mDrawerLayout.isDrawerOpen(linearLayout);

		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);

		Log.i("######", "@@@@@@@@@@@@@@@@@@@ New Intent");
		int event_id = getIntent().getExtras().getInt(DataUtil.EVENT_ID);
		EventLoadingFragment loading = new EventLoadingFragment();
		loading.event_id = event_id;

		getFragmentManager().beginTransaction()
				.replace(R.id.frame_container, loading).addToBackStack(null)
				.commit();
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {

		super.onPostCreate(savedInstanceState);
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {

		super.onConfigurationChanged(newConfig);
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	class SlideMenuListener implements OnItemClickListener {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {

			switch (position) {
			case 0:
				fragment = new HomeFragment();
				fragmentTag = DataUtil.WISHLIST_FRAGMENT_TAG;
				break;
			case 1:
				fragment = new CategoriesFragment();
				fragmentTag = DataUtil.CATEGORIES_FRAGMENT_TAG;
				break;
			case 2:
				fragment = new FriendListFragment();
				fragmentTag = DataUtil.FRIEND_FRAGMENT_TAG;
				break;
			case 3:
				fragment = new ProfileFragment();
				fragmentTag = DataUtil.PROFILE_FRAGMENT_TAG;
				break;
			case 4:
				fragment = new events();
				fragmentTag = DataUtil.EVENT_FRAGMENT_TAG;
				break;
			case 5:
				fragment = new WishListFragment();
				fragmentTag = DataUtil.WISHLIST_FRAGMENT_TAG;
				break;
			case 6:
				// fragment = new HomeFragment();
				if (Session.getActiveSession() != null) {
					Session.getActiveSession().closeAndClearTokenInformation();
					SharedPreferences pref = getSharedPreferences(
							DataUtil.LOGGEDIN, 0);
					Editor editor = pref.edit();
					editor.putString(DataUtil.LOGGEDIN, "false");
					editor.commit();
					Intent i = new Intent(getApplicationContext(),
							Main_Page.class);
					startActivity(i);
				}

				break;

			default:
				break;
			}

			if (fragment != null) {

				if (!(fragment instanceof WishListFragment)) {
					ic_add.setVisibility(View.GONE);
				}
				FragmentManager fragmentManager = getFragmentManager();
				fragmentManager.beginTransaction()
						.replace(R.id.frame_container, fragment, fragmentTag)
						.addToBackStack(fragmentTag).commit();

				// update selected item and title, then close the drawer
				mDrawerList.setItemChecked(position, true);
				mDrawerList.setSelection(position);
				title.setText(navMenuTitles[position]);
				mDrawerLayout.closeDrawer(linearLayout);
			} else {
				// error in creating fragment
				Log.e("MainActivity", "Error in creating fragment");
			}
		}

	}

	@Override
	public void onBackPressed() {

		FragmentManager fragmentManager = getFragmentManager();
		//if (backFromServer) {
			if (fragmentManager.getBackStackEntryCount() > 0) {
				Log.i("MainActivity", "popping backstack");
				fragmentManager.popBackStack();
			} else {
				Log.i("MainActivity", "nothing on backstack, calling super");
				super.onBackPressed();
			}
		//}
	}

	public Bitmap getRoundedShape(Bitmap scaleBitmapImage) {
		int targetWidth = 120;
		int targetHeight = 120;
		Bitmap targetBitmap = Bitmap.createBitmap(targetWidth, targetHeight,
				Bitmap.Config.ARGB_8888);

		Canvas canvas = new Canvas(targetBitmap);
		Path path = new Path();
		path.addCircle(((float) targetWidth - 1) / 2,
				((float) targetHeight - 1) / 2,
				(Math.min(((float) targetWidth), ((float) targetHeight)) / 2),
				Path.Direction.CCW);

		canvas.clipPath(path);
		Bitmap sourceBitmap = scaleBitmapImage;
		canvas.drawBitmap(sourceBitmap, new Rect(0, 0, sourceBitmap.getWidth(),
				sourceBitmap.getHeight()), new Rect(0, 0, targetWidth,
				targetHeight), null);
		return targetBitmap;
	}

	private class RegisterServerTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) { // Register on our
														// server // On server
														// creates a new user
														// creates a new user
			SharedPreferences userPref = getSharedPreferences(
					DataUtil.USER_PREF, 0);
			int userId = userPref.getInt(DataUtil.USER_ID, 0);
			ServerUtilities.register(getApplicationContext(), regId, userId);
			return null;
		}

	}

	private final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String newMessage = intent.getExtras().getString(EXTRA_MESSAGE);
			// Waking up mobile if it is sleeping
			WakeLocker.acquire(getApplicationContext());

			// Showing received message
			// lblMessage.append(newMessage + "\n");
			Toast.makeText(getApplicationContext(),
					"New Message: " + newMessage, Toast.LENGTH_LONG).show();
			// if (intent.getExtras().containsKey("event_id")) {
			// int event_id = intent.getExtras().getInt("event_id");
			// EventLoadingFragment loading = new EventLoadingFragment();
			// loading.event_id = event_id;
			//
			// getFragmentManager().beginTransaction()
			// .replace(R.id.frame_container, loading)
			// .addToBackStack(null).commit();
			// }

			// connect to server to get event data using title
			//
			// Fragment event = new events();
			// getFragmentManager().beginTransaction()
			// .replace(R.id.frame_container, event).addToBackStack(null)
			// .commit();
			// Releasing wake lock
			WakeLocker.release();
		}
	};
}
