package eg.gov.iti.jets.iwish.controller;

import java.util.ArrayList;

import eg.gov.iti.jets.iwish.businesslayer.FriendParser;


import eg.gov.iti.jets.iwish.dto.ProductDTO;
import eg.gov.iti.jets.iwish.dto.UserDTO;
import eg.gov.iti.jets.iwish.dto.UserEventDTO;

import android.content.Context;

public class FriendController {

	private Context context;
	
	public FriendController(Context context) {
		this.context = context;
	}
	
	public ArrayList<UserDTO> getFriends (UserDTO userDTO){
		
		String email = userDTO.getEmail();
		FriendParser friendParser = new FriendParser(context);
		return friendParser.getFriends(userDTO.getEmail());
	}
	
	public ArrayList<ProductDTO> getFriendWishlist(UserDTO userDTO){
		
		String email = userDTO.getEmail();
		FriendParser friendParser = new FriendParser(context);
		return friendParser.getFriendWishlist(email);
		
	}
	
	public ArrayList<UserEventDTO> getFriendEvents (UserDTO userDTO){
		
		String facebookId = userDTO.getFacebookUserId();
		FriendParser friendParser = new FriendParser(context);
		return friendParser.getFriendEvents(facebookId);
		
	}
	
	public void saveFriends (ArrayList<UserDTO> userDTOs, String key){
		FriendParser friendParser = new FriendParser(context);
		friendParser.saveFriends(userDTOs);
	}
	
	public ArrayList<UserDTO> getFriends (){
		FriendParser friendParser = new FriendParser(context);
		return friendParser.getFriends();
	}
	
}
