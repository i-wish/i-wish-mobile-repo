package eg.gov.iti.jets.iwish.activities;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.log4j.Logger;
import org.apache.log4j.Logger.*;

import eg.gov.iti.jets.iwish.asyncTasks.UserInfoTask;
import eg.gov.iti.jets.iwish.controller.FriendController;
import eg.gov.iti.jets.iwish.controller.UserController;
import eg.gov.iti.jets.iwish.util.DataUtil;
import eg.gov.iti.jets.iwish.ws.webServiceConnection;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.StaticLayout;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.Toast;

import com.facebook.FacebookException;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.Session.StatusCallback;
import com.facebook.SessionState;
import com.facebook.android.Facebook;
import com.facebook.android.Facebook.DialogListener;
import com.facebook.android.FacebookError;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;
import com.facebook.widget.LoginButton.UserInfoChangedCallback;
import com.google.android.gcm.GCMRegistrar;
import com.google.gson.Gson;
import eg.gov.iti.jets.iwish.controller.UserController;
import eg.gov.iti.jets.iwish.dto.UserDTO;
import eg.gov.iti.jets.iwish.util.DataUtil;
import eg.gov.iti.jets.iwish.ws.webServiceConnection;

public class Main_Page extends Activity {

	final String PERMISSIONS[] = { "user_friends", "email", "friends_birthday",
			"offline_access", "user_birthday", "USER_ABOUT_ME",
			"USER_LOCATION", "USER_LIKES", "USER_HOMETOWN" };

	Session session2 = null;
	LoginButton btn_facebookLogin;
	Button login;
	UserDTO loggeduser = new UserDTO();
	webServiceConnection conn = null;
	Intent intent;
	SharedPreferences sharedpref;
	public static final String MyPREFERENCES = "MyPrefs";
	boolean back = false;
	String accessToken;
	Session session1;
	ConnectionDetector cd;
	String logged;
	SharedPreferences pref;
	SharedPreferences.Editor editor;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main__page);
		pref = getSharedPreferences(DataUtil.LOGGEDIN, 0);
		logged = pref.getString(DataUtil.LOGGEDIN, "false");
		if(logged.equals("true")){
			Intent profile=new Intent(getApplicationContext(), LoadingActivity.class);
			startActivity(profile);
			finish();
		}
		getActionBar().hide();
		// AndroidLogger.configure();
		// final Logger logger = Logger.getLogger(Main_Page.class);
		// logger.debug("this is i wish project first log");

		
		AlertDialogManager alert = new AlertDialogManager();
		cd = new ConnectionDetector(getApplicationContext());

		// Check if Internet present
		if (!cd.isConnectingToInternet()) {
			// Internet Connection is not present
			alert.showAlertDialog(Main_Page.this, "Internet Connection Error",
					"Please connect to working Internet connection", false);
			// stop executing code by return
			return;
		}

		btn_facebookLogin = (LoginButton) findViewById(R.id.authButton);

		if (Session.getActiveSession() != null) {
			Log.i("session", "************* active");
			if (logged.equals("false")) {
				Session.getActiveSession().closeAndClearTokenInformation();
			} else {

				if (Session.getActiveSession().isOpened()) {
					if (cd.isConnectingToInternet()) {
						Log.i("logged", "true true");
						UserController userController = new UserController(
								getApplicationContext());
						accessToken = pref.getString(DataUtil.AccessToken_PREF,
								null);
						userController.setAccessToken(accessToken);
						userController.connectUserWS();
						Intent i = new Intent(getApplicationContext(),
								LoadingActivity.class);
						startActivity(i);
						finish();
					}

					/*else {
						Intent i = new Intent(getApplicationContext(),
								HomeActivity.class);
						startActivity(i);
						finish();
					}*/
				}
			}
		}

		// Session session=btn_facebookLogin.
		// btn_facebookLogin.setPublishPermissions(PERMISSIONS);
		btn_facebookLogin.setSessionStatusCallback(new StatusCallback() {

			@Override
			public void call(Session session, SessionState state,
					Exception exception) {
				Log.i("facebook callback",
						"session state is " + state.toString());
				if (session.isOpened()) {
					Log.i("facebook callback",
							"session state is " + state.toString());
					if ((state == SessionState.OPENED_TOKEN_UPDATED)
							&& (logged.equals("true"))) {
						// user already logged in
						Intent i = new Intent(getApplicationContext(),
								LoadingActivity.class);
						startActivity(i);

					} else {
						editor = pref.edit();
						editor.putString(DataUtil.LOGGEDIN, "false");
						accessToken = session.getAccessToken();
						editor.putString(DataUtil.AccessToken_PREF, accessToken);
						editor.commit();

						UserController userController = new UserController(
								getApplicationContext());
						userController.setAccessToken(accessToken);
						userController.connectUserWS();
						// GetFriendsTask getFriendsTask = new GetFriendsTask();
						// getFriendsTask.execute();
					}
				}
			}

		});

	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		outState.putSerializable("user", loggeduser);
	}

	@Override
	protected void onActivityResult(int arg0, int arg1, Intent arg2) {
		// TODO Auto-generated method stub
		super.onActivityResult(arg0, arg1, arg2);
		Log.i("tag", "###############Back from facebook");
		Session.getActiveSession().onActivityResult(this, arg0, arg1, arg2);

		Intent i = new Intent(getApplicationContext(),LoadingActivity.class);
		startActivity(i);
		finish();

		
		

	}

	public void printHashKey() {

		try {
			PackageInfo info = getPackageManager().getPackageInfo(
					"com.example.integrationsample",
					PackageManager.GET_SIGNATURES);

			for (Signature signature : info.signatures) {
				MessageDigest md = MessageDigest.getInstance("SHA");
				md.update(signature.toByteArray());
				// }

				// AlertDialog.Builder builder = new AlertDialog.Builder(this);
				// builder.setMessage( Base64.encodeToString(md.digest(),
				// Base64.DEFAULT)).setPositiveButton("ok", new
				// DialogInterface.OnClickListener()
				// {
				// public void onClick(DialogInterface dialog, int id) { // FIRE
				// ZE MISSILES! } })
				// .setNegativeButton("ok", new
				// DialogInterface.OnClickListener()
				// }
				// { public
				// void onClick(DialogInterface dialog, int id) { // User
				// cancelled the
				// dialog } }); // Create the AlertDialog object and return it
				// AlertDialog
				// dialog = builder.create(); dialog.show();
				//
				Toast.makeText(this,
						Base64.encodeToString(md.digest(), Base64.DEFAULT),
						Toast.LENGTH_LONG).show();
				Log.i("KEY", Base64.encodeToString(md.digest(), Base64.DEFAULT));
			}
		} catch (NameNotFoundException e) {
			e.printStackTrace();

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		// getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		/*
		 * if (id == R.id.action_settings) { return true; }
		 */
		return super.onOptionsItemSelected(item);
	}

	class GetFriendsTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {

			SharedPreferences userPref = getApplicationContext()
					.getSharedPreferences(DataUtil.USER_PREF, 0);
			SharedPreferences.Editor editor = userPref.edit();
			String userJson = userPref.getString(DataUtil.USER_JSON, null);
			Gson gson = new Gson();
			UserDTO userDTO = gson.fromJson(userJson, UserDTO.class);

			FriendController friendController = new FriendController(
					getApplicationContext());
			friendController.getFriends(userDTO);

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {

			editor.putString(DataUtil.LOGGEDIN, "true");
			editor.commit();
			super.onPostExecute(result);
		}

	}

}
