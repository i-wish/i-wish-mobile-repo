package eg.gov.iti.jets.iwish.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import eg.gov.iti.jets.iwish.activities.R;
import eg.gov.iti.jets.iwish.adapter.ReserveProductAdapter.ListBean;
import eg.gov.iti.jets.iwish.dto.UserReserveProduct;

public class ReserveProductTest extends ArrayAdapter<UserReserveProduct> {

	Context context;
	ArrayList<UserReserveProduct> usersReserve;

	public ReserveProductTest(Context context, int resource,
			int textViewResourceId, List<UserReserveProduct> objects) {
		super(context, resource, textViewResourceId, objects);
		this.context = context;
		this.usersReserve = (ArrayList<UserReserveProduct>) objects;
		this.usersReserve.add(0, new UserReserveProduct());
		this.usersReserve.add(new UserReserveProduct());
	}

	@Override
	public UserReserveProduct getItem(int position) {
		return usersReserve.get(position);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		System.out.println();
		return usersReserve.size();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ListBean list = null;
		final int productIndex = position;
		final ViewGroup parentView = parent;

		if (position == 0) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.reserve_product_first_item,
					parent, false);

		}

		else if (position == usersReserve.size() - 1) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.reserve_product_last_item,
					parent, false);

		} else {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater
					.inflate(R.layout.friends_item, parent, false);
			TextView userName = (TextView) convertView
					.findViewById(R.id.friend_name);
			ImageView userImage = (ImageView) convertView
					.findViewById(R.id.friend_image);

			if (usersReserve.get(position).getUserDTO() != null) {
				userName.setText(usersReserve.get(position).getUserDTO()
						.getName());
				userName.setTextColor(Color.BLACK);
				Bitmap bitmap = BitmapFactory
						.decodeByteArray(usersReserve.get(position)
								.getUserDTO().getPicture(), 0, usersReserve
								.get(position).getUserDTO().getPicture().length);
				userImage.setImageBitmap(bitmap);
			}
		}

		return convertView;
	}
}
