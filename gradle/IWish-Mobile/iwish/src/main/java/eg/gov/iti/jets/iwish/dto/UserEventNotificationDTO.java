package eg.gov.iti.jets.iwish.dto;


 
public class UserEventNotificationDTO extends AbstractDTO implements java.io.Serializable  {
    int id;
    UserDTO user;
    UserEventDTO userEvent;

    public Integer getId() {
        return id;
    }

    @Override
    public String getValueString() {
        return "Notification of Event type "+userEvent.getEventTypeDTO().getName() +" for User "+ user.getName();
    }

    public UserDTO getUserDTO() {
        return user;
    }

    public UserEventDTO getUserEventDTO() {
        return userEvent;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setUserDTO(UserDTO user) {
        this.user = user;
    }

    public void setUserEventDTO(UserEventDTO userEvent) {
        this.userEvent = userEvent;
    }
}
