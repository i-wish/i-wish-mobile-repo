package eg.gov.iti.jets.iwish.datalayer;

import java.util.ArrayList;
import java.util.List;

import eg.gov.iti.jets.iwish.dto.UserEventDTO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.util.Log;

public class DBAccess extends SQLiteOpenHelper {

	private static final String DataBase_Name = "iwish_db";
	private static final int DataBase_Version = 1;

	SQLiteDatabase sqlitedb;
	ContentValues contentValues;

	// event table
	private static final String event_table = "event";
	private static final String col_event_id = "id";
	private static final String col_event_name = "name";
	private static final String col_event_date = "date";
	private static final String col_event_image = "image";
	private static final String col_event_user_id = "user_id";

	// wish table
	private static final String wish_table = "wish";
	private static final String col_wish_id = "id";
	private static final String col_wish_name = "name";
	private static final String col_wish_is_recieved = "is_recieved";
	private static final String col_wish_image = "image";
	private static final String col_wish_user_id = "user_id";

	// friend table
	private static final String friend_table = "friend";
	private static final String col_friend_id = "id";
	private static final String col_friend_name = "name";
	private static final String col_friend_image = "image";

	// private static final String[] COLUMNS = { col_drug_name, col_drug_type,
	// col_drug_dose, col_drug_image, col_drug_start_date,
	// col_drug_end_date, col_drug_hour, col_drug_minute,
	// col_drug_interval, col_drug_duration };

	public DBAccess(Context context) {
		super(context, DataBase_Name, null, DataBase_Version);
		Log.i("helper", "on create table");

	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		Log.i("helper", "on create table");

		// create event table
		String dbQuery = "create table " + event_table + "(" + col_event_id + " int," 
					   + col_event_date + " string," 
				       + col_event_image + " TEXT," 
					   + col_event_name + " TEXT," 
				       + col_event_user_id + " int )";
		db.execSQL(dbQuery);

		// create wish table
		dbQuery = "create table " + wish_table + "(" + col_wish_id + " int,"
				+ col_wish_image + " TEXT,"
				+ col_wish_name + " TEXT,"
				+ col_wish_is_recieved + " int," 
				+ col_wish_user_id + " int )";
		db.execSQL(dbQuery);

		// create friend table
		dbQuery = "create table " + friend_table + "(" + col_friend_id + " int,"
				+ col_friend_image + " TEXT," 
				+ col_friend_name + " TEXT)";
		db.execSQL(dbQuery);

	}

	

	@Override
	public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub

	}
	
	}
