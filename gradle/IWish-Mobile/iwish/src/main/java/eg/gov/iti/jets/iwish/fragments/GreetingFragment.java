package eg.gov.iti.jets.iwish.fragments;

import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import eg.gov.iti.jets.iwish.activities.HomeActivity;
import eg.gov.iti.jets.iwish.activities.R;
import eg.gov.iti.jets.iwish.activities.R.id;
import eg.gov.iti.jets.iwish.activities.R.layout;
import eg.gov.iti.jets.iwish.controller.GreetingController;
import eg.gov.iti.jets.iwish.dto.EventCommentsDTO;
import eg.gov.iti.jets.iwish.dto.GreetingDTO;
import eg.gov.iti.jets.iwish.dto.UserDTO;
import eg.gov.iti.jets.iwish.dto.UserEventDTO;
import eg.gov.iti.jets.iwish.util.DataUtil;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class GreetingFragment extends Fragment {

	TextView send, suggest;
	EditText greeting;
	UserDTO user;
	UserEventDTO event;
	List<GreetingDTO> greetingList;
	ProgressDialog Pdialog;

	public GreetingFragment(UserEventDTO event) {
		this.event = event;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.activity_greeting, container,
				false);

		String userJson = getActivity().getSharedPreferences(
				DataUtil.USER_PREF, 0).getString(DataUtil.USER_JSON, null);
		Gson json = new GsonBuilder().setDateFormat("MMM dd, yyyy").create();
		user = json.fromJson(userJson, UserDTO.class);

		send = (TextView) rootView.findViewById(R.id.send);
		suggest = (TextView) rootView.findViewById(R.id.suggestion);
		greeting = (EditText) rootView.findViewById(R.id.greeting);
		new greetingSuggestionTask().execute();

		if (event != null) {
			((HomeActivity)getActivity()).title.setText(event.getName());
			suggest.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {

					if (greetingList != null) {
						String[] greetingArr = new String[greetingList.size()];
						for (int i = 0; i < greetingList.size(); i++) {
							greetingArr[i] = greetingList.get(i)
									.getDescription();
						}
						AlertDialog.Builder builder = new AlertDialog.Builder(
								getActivity());
						builder.setTitle("Greetings")
								.setItems(greetingArr,
										new DialogInterface.OnClickListener() {
											public void onClick(
													DialogInterface dialog,
													int which) {
												greeting.setText(greetingList
														.get(which)
														.getDescription());
											}

										}).create();
						builder.show();

					}

				}
			});

		}

		send.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				 Pdialog = ProgressDialog.show(getActivity(),"", "Sending...");
				
				new sendGreeting().execute();
			}
		});

		return rootView;

	}

	private class greetingSuggestionTask extends AsyncTask<Void, Void, String> {

		@Override
		protected String doInBackground(Void... params) {
			try{
			GreetingController controller = new GreetingController(
					getActivity());
			return controller.getGreetingSuggestion(event.getEventTypeDTO());
			}catch (Exception e) {
				e.printStackTrace();
				return null;
			}

		}

		@Override
		protected void onPostExecute(String result) {
			if(!GreetingFragment.this.isHidden()&&!GreetingFragment.this.isRemoving()){
			try {
				GreetingController controller = new GreetingController(
						getActivity());
				greetingList = controller.parseGreetingSuggestions(result);
			} catch (Exception ex) {
				Toast.makeText(getActivity(), "error loading data",
						Toast.LENGTH_LONG).show();
			}
			}

		}

	}

	private class sendGreeting extends AsyncTask<Void, Void, String> {

		@Override
		protected void onPreExecute() {
			
			super.onPreExecute();
			//((HomeActivity) getActivity()).backFromServer = false;
		}

		@Override
		protected String doInBackground(Void... params) {
			try{
			GreetingController controller = new GreetingController(
					getActivity());
			EventCommentsDTO comment = new EventCommentsDTO();
			comment.setContent(greeting.getText().toString());
			comment.setUserEventDTO(event);
			comment.setUser(user);
			return controller.sendGreeting(comment);
			}catch(Exception ex){
				ex.printStackTrace();
				return null;
			}

		}

		@Override
		protected void onPostExecute(String result) {
			Pdialog.dismiss();
			try{
			GreetingController controller = new GreetingController(
					getActivity());
			if (controller.parseSendGreeting(result)) {
				EventDetailsFragment eventDetails = new EventDetailsFragment();
				eventDetails.event = event;
				eventDetails.isFriend = true;
				getFragmentManager().popBackStack();
				/*
				 * getFragmentManager().popBackStack();
				 * getFragmentManager().beginTransaction
				 * ().replace(R.id.frame_container,
				 * eventDetails).addToBackStack(null) .commit();
				 */

			} else {
				Toast.makeText(HomeActivity.HOME_CONTEXT,
						"Failed to send greeting",
						Toast.LENGTH_LONG).show();
			}
			}catch(Exception ex){
				Toast.makeText(HomeActivity.HOME_CONTEXT,
						"Failed to send greeting",
						Toast.LENGTH_LONG).show();
			}
			//((HomeActivity) getActivity()).backFromServer = false;

		}

	}
}
