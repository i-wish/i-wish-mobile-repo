package eg.gov.iti.jets.iwish.fragments;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import eg.gov.iti.jets.iwish.activities.HomeActivity;
import eg.gov.iti.jets.iwish.activities.R;
import eg.gov.iti.jets.iwish.activities.R.id;
import eg.gov.iti.jets.iwish.activities.R.layout;
import eg.gov.iti.jets.iwish.adapter.EventListAdapter;
import eg.gov.iti.jets.iwish.adapter.GreetingListAdapter;
import eg.gov.iti.jets.iwish.asyncTasks.eventTask;
import eg.gov.iti.jets.iwish.controller.EventController;
import eg.gov.iti.jets.iwish.controller.GreetingController;
import eg.gov.iti.jets.iwish.controller.PlaceController;
import eg.gov.iti.jets.iwish.dto.EventCommentsDTO;
import eg.gov.iti.jets.iwish.dto.PlaceDTO;
import eg.gov.iti.jets.iwish.dto.UserEventDTO;
import eg.gov.iti.jets.iwish.util.DataUtil;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.webkit.WebView.FindListener;
import android.widget.AbsoluteLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class EventDetailsFragment extends Fragment {

	public UserEventDTO event;
	TextView date, time, desc, place, name, greeting;
	ImageView img, ic_edit, userImg, suggestPlace, suggestGift, ic_delete;
	public String eventJson;
	public events eventFragment;
	SharedPreferences pref;
	ListView greetingListView;
	List<EventCommentsDTO> greetingList = new ArrayList<EventCommentsDTO>();
	GreetingListAdapter adapter;

	public boolean isFriend = false;
	View rootView;
	LayoutInflater inflater;
	List<PlaceDTO> places = new ArrayList<PlaceDTO>();

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		if (ic_delete != null)
			ic_delete.setVisibility(View.GONE);
		if (ic_edit != null)
			ic_edit.setVisibility(View.GONE);
		((HomeActivity)getActivity()).progressBar.setVisibility(View.GONE);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (ic_delete != null)
			ic_delete.setVisibility(View.GONE);
		if (ic_edit != null)
			ic_edit.setVisibility(View.GONE);
		((HomeActivity)getActivity()).progressBar.setVisibility(View.GONE);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.activity_event, container, false);
		this.inflater = inflater;
		// greetingListView=(ListView)
		// rootView.findViewById(R.id.greetingsList);
		adapter = new GreetingListAdapter(getActivity(), 0, greetingList);
		((HomeActivity)getActivity()).title.setText("");
		// greetingListView.setAdapter(adapter);
		if (getActivity() != null)
			new getGreetings().execute();

		/*
		 * eventJson = (String) getIntent().getExtras().get("eventJson");
		 * if(getIntent().getExtras().getBoolean("isFriend")){ isFriend=true;
		 * 
		 * }
		 */

		date = (TextView) rootView.findViewById(R.id.event_date);
		time = (TextView) rootView.findViewById(R.id.event_time);
		place = (TextView) rootView.findViewById(R.id.event_place);
		desc = (TextView) rootView.findViewById(R.id.event_desc);
		name = (TextView) rootView.findViewById(R.id.event_name);
		img = (ImageView) rootView.findViewById(R.id.event_img);

		if (event != null) {

			// Gson json= new
			// GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").create();
			// Gson json = new Gson();
			// event = json.fromJson(eventJson, UserEventDTO.class);
			if (event.getImage() != null) {
				Bitmap eventImg = BitmapFactory.decodeByteArray(
						event.getImage(), 0, event.getImage().length);
				img.setImageBitmap(eventImg);
			}
			name.setText(event.getName());
			if (event.getDate() != null) {
				rootView.findViewById(R.id.date_layout).setVisibility(
						View.VISIBLE);

				date.setText(new SimpleDateFormat("yyyy-MM-dd").format(event
						.getDate()));
			}
			if (event.getTime() != null) {
				rootView.findViewById(R.id.date_layout).setVisibility(
						View.VISIBLE);

				time.setText(new SimpleDateFormat("HH:mm").format(event
						.getTime()));
			}
			if (event.getPlace() != null) {
				rootView.findViewById(R.id.location_layout).setVisibility(
						View.VISIBLE);

				place.setText(event.getPlace().getName());
			}
			if (event.getDescription() != "") {
				rootView.findViewById(R.id.about).setVisibility(View.VISIBLE);
				desc.setText(event.getDescription());
			}

			((HomeActivity) getActivity()).title.setText(event.getName());
		}

		if (!isFriend) {

			ic_edit = (ImageView) getActivity().findViewById(R.id.ic_edit);
			ic_delete = (ImageView) getActivity().findViewById(R.id.delete);
			ic_edit.setVisibility(View.VISIBLE);
			ic_delete.setVisibility(View.VISIBLE);

			ic_delete.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					AlertDialog.Builder builder = new AlertDialog.Builder(
							EventDetailsFragment.this.getActivity());
					builder.setMessage("event will be deleted")
							.setPositiveButton("Delete",
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog, int id) {

											new DeleteTask().execute();

										}
									})
							.setNegativeButton("Cancel",
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog, int id) {

										}
									});

					builder.setTitle("Delete");
					builder.create().show();

				}
			});

			ic_edit.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {

					NewEventFragment editEventFragment = new NewEventFragment();
					editEventFragment.event = event;
					getFragmentManager().beginTransaction()
							.replace(R.id.frame_container, editEventFragment)
							.addToBackStack(null).commit();

					/*
					 * Intent editEvent=new Intent(getApplicationContext(),
					 * NewEventActivity.class); editEvent.putExtra("eventJson",
					 * eventJson); startActivity(editEvent);
					 */

				}
			});
		} else {
			suggestGift = (ImageView) rootView.findViewById(R.id.suggest_gift);
			suggestPlace = (ImageView) rootView
					.findViewById(R.id.suggest_place);
			rootView.findViewById(R.id.suggest_layout).setVisibility(
					View.VISIBLE);
			rootView.findViewById(R.id.sendGreetingLayout).setVisibility(
					View.VISIBLE);
			greeting = (TextView) rootView.findViewById(R.id.greeting);
			userImg = (ImageView) rootView.findViewById(R.id.user_img);
			pref = getActivity().getSharedPreferences(DataUtil.USER_PREF, 0);
			final String userJson = pref.getString(DataUtil.USER_JSON, null);
			String img_str = pref.getString(DataUtil.USER_FB_img, null);
			if (img_str != null) {
				byte[] userImg1 = Base64.decode(img_str, Base64.DEFAULT);
				userImg.setImageBitmap(getRoundedShape(BitmapFactory
						.decodeByteArray(userImg1, 0, userImg1.length)));
			}

			greeting.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					GreetingFragment greetingFragment = new GreetingFragment(
							event);
					getFragmentManager().beginTransaction()
							.replace(R.id.frame_container, greetingFragment)
							.addToBackStack(null).commit();

				}
			});

			suggestPlace.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					new getPlaces().execute();
				}

			});

			suggestGift.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent i = getActivity().getIntent();
					i.putExtra(DataUtil.FRIENDSELECTED, event.getUserDTO());
					FriendWishlistFragment friendWishlistFragment = new FriendWishlistFragment();
					getActivity()
							.getFragmentManager()
							.beginTransaction()
							.replace(R.id.frame_container,
									friendWishlistFragment,
									DataUtil.CATEGORY_FRAGMENT_TAG)
							.addToBackStack(null).commit();

				}
			});

		}
		return rootView;

	}

	public class DeleteTask extends AsyncTask<Void, Void, Boolean> {
		List<UserEventDTO> eventsToDelete = new ArrayList<UserEventDTO>();

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// ((HomeActivity)getActivity()).backFromServer = false;
			

		}

		@Override
		protected Boolean doInBackground(Void... params) {
			try {
				EventController controller = new EventController(
						HomeActivity.HOME_CONTEXT);

				eventsToDelete.add(event);
				return controller.deleteEvents(eventsToDelete);
			} catch (Exception ex) {
				ex.printStackTrace();
				return false;
			}

		}

		@Override
		protected void onPostExecute(Boolean result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if(!EventDetailsFragment.this.isHidden()&&!EventDetailsFragment.this.isRemoving()){
			if (result) {
				try{

				for (UserEventDTO e : eventsToDelete) {
					eventFragment.eventList.remove(e);

				}
				eventFragment.adapter = new EventListAdapter(getActivity(), 0,
						eventFragment.eventList);

				eventFragment.list.setAdapter(eventFragment.adapter);
				// list.getAdapter().notify();
				eventsToDelete = new ArrayList<UserEventDTO>();

				Toast.makeText(EventDetailsFragment.this.getActivity(),
						"event deleted", Toast.LENGTH_LONG).show();
				ic_delete.setVisibility(View.INVISIBLE);
				ic_edit.setVisibility(View.INVISIBLE);
				getFragmentManager().popBackStack();
				}catch(Exception ex){
					ex.printStackTrace();
					Toast.makeText(HomeActivity.HOME_CONTEXT,
							"couldn't be deleted", Toast.LENGTH_LONG).show();
				}

			} else {
				Toast.makeText(HomeActivity.HOME_CONTEXT,
						"couldn't be deleted", Toast.LENGTH_LONG).show();
			}
			}

			// ((HomeActivity)getActivity()).backFromServer = true;

		}

	}

	public Bitmap getRoundedShape(Bitmap scaleBitmapImage) {
		int targetWidth = 120;
		int targetHeight = 120;
		Bitmap targetBitmap = Bitmap.createBitmap(targetWidth, targetHeight,
				Bitmap.Config.ARGB_8888);

		Canvas canvas = new Canvas(targetBitmap);
		Path path = new Path();
		path.addCircle(((float) targetWidth - 1) / 2,
				((float) targetHeight - 1) / 2,
				(Math.min(((float) targetWidth), ((float) targetHeight)) / 2),
				Path.Direction.CCW);

		canvas.clipPath(path);
		Bitmap sourceBitmap = scaleBitmapImage;
		canvas.drawBitmap(sourceBitmap, new Rect(0, 0, sourceBitmap.getWidth(),
				sourceBitmap.getHeight()), new Rect(0, 0, targetWidth,
				targetHeight), null);
		return targetBitmap;
	}

	class getGreetings extends AsyncTask<Void, Void, String> {

		@Override
		protected void onPreExecute() {
			
			super.onPreExecute();
			((HomeActivity) getActivity()).progressBar.setVisibility(View.VISIBLE);
		}

		@Override
		protected String doInBackground(Void... params) {
			try {
				GreetingController controller = new GreetingController(
						HomeActivity.HOME_CONTEXT);
				return controller.getGreetings(event);
			} catch (Exception ex) {
				ex.printStackTrace();
				return null;
			}
		}

		@Override
		protected void onPostExecute(String result) {
			((HomeActivity)HomeActivity.HOME_CONTEXT).progressBar.setVisibility(View.GONE);
			if (!EventDetailsFragment.this.isRemoving()
					&& !EventDetailsFragment.this.isHidden()) {
				if (result != null) {
					try {

						GreetingController controller = new GreetingController(
								getActivity());
						greetingList = controller.parseGreetings(result);
						LinearLayout greetingList_layout = (LinearLayout) rootView
								.findViewById(R.id.greetingsList);

						if (greetingList != null) {
							for (EventCommentsDTO g : greetingList) {
								View convertView = inflater.inflate(
										R.layout.greeting_cell, null);
								TextView friend_name = (TextView) convertView
										.findViewById(R.id.friend_name);
								TextView comment = (TextView) convertView
										.findViewById(R.id.comment);
								ImageView friendImg = (ImageView) convertView
										.findViewById(R.id.friend_img);

								friend_name.setText(g.getUser().getName());
								comment.setText(g.getContent());
								// eventName.setTextColor(Color.BLACK);

								if (g.getUser().getPicture() != null) {
									Bitmap bitmap = BitmapFactory
											.decodeByteArray(
													g.getUser().getPicture(),
													0,
													g.getUser().getPicture().length);
									friendImg
											.setImageBitmap(getRoundedShape(bitmap));

								}

								greetingList_layout.addView(convertView);
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
						Toast.makeText(HomeActivity.HOME_CONTEXT,
								"couldn't retrieve greetings",
								Toast.LENGTH_LONG).show();

					}
				} else {
					Toast.makeText(HomeActivity.HOME_CONTEXT,
							"couldn't retrieve greetings", Toast.LENGTH_LONG)
							.show();

				}
				/*
				 * adapter = new GreetingListAdapter(getActivity(), 0,
				 * greetingList); greetingListView =
				 * setListViewHeightBasedOnChildren(greetingListView);
				 * greetingListView.setAdapter(adapter);
				 */
				// ((HomeActivity)getActivity()).backFromServer = true;

			}
		}

	}

	public static ListView setListViewHeightBasedOnChildren(ListView listView) {
		ListAdapter listAdapter = listView.getAdapter();
		if (listAdapter == null) {
			// pre-condition
			return null;
		}

		int totalHeight = 0;
		int desiredWidth = MeasureSpec.makeMeasureSpec(listView.getWidth(),
				MeasureSpec.AT_MOST);
		for (int i = 0; i < listAdapter.getCount(); i++) {
			View listItem = listAdapter.getView(i, null, listView);
			listItem.measure(desiredWidth, MeasureSpec.UNSPECIFIED);
			totalHeight += listItem.getMeasuredHeight();
		}

		ViewGroup.LayoutParams params = listView.getLayoutParams();
		params.height = totalHeight
				+ (listView.getDividerHeight() * (listAdapter.getCount() - 1));
		listView.setLayoutParams(params);
		listView.requestLayout();
		return listView;
	}

	private class getPlaces extends AsyncTask<Void, Void, String> {
		String[] placesArr;

		@Override
		protected String doInBackground(Void... params) {
			try {
				PlaceController controller = new PlaceController(getActivity());
				return controller.getPlaces(event.getEventTypeDTO());
			} catch (Exception ex) {
				ex.printStackTrace();
				return null;
			}

		}

		@Override
		protected void onPostExecute(String result) {

			
			super.onPostExecute(result);
			if(result!=null){
				try{
			PlaceController controller = new PlaceController(getActivity());
			places = controller.parsePlaces(result);
			placesArr = new String[places.size()];
			for (int i = 0; i < places.size(); i++) {
				placesArr[i] = places.get(i).getName();
			}
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			builder.setTitle("Location").setItems(placesArr, null).create();
			builder.show();
				}catch(Exception ex){
					Toast.makeText(HomeActivity.HOME_CONTEXT,
							"couldn't retrieve places", Toast.LENGTH_LONG)
							.show();
				}
			}else{
				Toast.makeText(HomeActivity.HOME_CONTEXT,
						"couldn't retrieve places", Toast.LENGTH_LONG)
						.show();
			}

		}
	}

	@Override
	public void onDestroyView() {
		// TODO Auto-generated method stub
		super.onDestroyView();
	}

}
