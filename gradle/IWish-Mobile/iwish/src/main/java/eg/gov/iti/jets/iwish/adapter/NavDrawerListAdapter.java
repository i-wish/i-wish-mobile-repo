package eg.gov.iti.jets.iwish.adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;
import eg.gov.iti.jets.iwish.dto.NavDrawerItem;
import eg.gov.iti.jets.iwish.activities.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class NavDrawerListAdapter extends ArrayAdapter<NavDrawerItem> {

	
	private ArrayList<NavDrawerItem> navDrawerItems;
	private Context context;
	
	public NavDrawerListAdapter(Context context, int resource,
			int textViewResourceId, List<NavDrawerItem> objects) {
		super(context, resource, textViewResourceId, objects);
		this.navDrawerItems = (ArrayList<NavDrawerItem>) objects;
		this.context = context;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
	
		ViewHolder viewHolder = null;
		
		if (convertView == null){
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.drawer_list_item, parent, false);
			viewHolder = new ViewHolder(convertView);
			viewHolder.title = (TextView) convertView.findViewById(R.id.title);
			viewHolder.icon = (ImageView) convertView.findViewById(R.id.icon);
			convertView.setTag(viewHolder);
		}
		else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		
		viewHolder.title.setText(navDrawerItems.get(position).getTitle());
		viewHolder.icon.setImageResource(navDrawerItems.get(position).getIcon());
		
		return convertView;
	}

	static class ViewHolder {
		ImageView icon;
		TextView title;
		View rootView;
		
		public ViewHolder(View rootView) {
			this.rootView = rootView;
		}
	}
}
