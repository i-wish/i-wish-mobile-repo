package eg.gov.iti.jets.iwish.dto;

import java.util.ArrayList;



public class FriendReservedProduct {
	
    int product_id;
    double participatePercent;
    ArrayList<UserDTO> userDTOs;

    public double getParticipatePercent() {
        return participatePercent;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public void setParticipatePercent(double participatePercent) {
        this.participatePercent = participatePercent;
    }

	public void setUserDTOs(ArrayList<UserDTO> userDTOs) {
		this.userDTOs = userDTOs;
	}

	public ArrayList<UserDTO> getUserDTOs() {
		return userDTOs;
	}

}