package eg.gov.iti.jets.iwish.adapter;

import java.util.ArrayList;
import java.util.List;

import eg.gov.iti.jets.iwish.activities.R;
import eg.gov.iti.jets.iwish.adapter.CategoriesAdapter.ListBean;
import eg.gov.iti.jets.iwish.dto.UserReserveProduct;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ReserveProductAdapter extends ArrayAdapter<UserReserveProduct> {

	Context context;
	ArrayList<UserReserveProduct> usersReserve;

	public ReserveProductAdapter(Context context, int resource,
			int textViewResourceId, List<UserReserveProduct> objects) {
		super(context, resource, textViewResourceId, objects);
		this.context = context;
		this.usersReserve = (ArrayList<UserReserveProduct>) objects;
	}

	@Override
	public UserReserveProduct getItem(int position) {
		return usersReserve.get(position);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return super.getCount();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ListBean list = null;
		final int productIndex = position;
		final ViewGroup parentView = parent;
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater
					.inflate(R.layout.friends_item, parent, false);
			list = new ListBean(convertView);
			convertView.setTag(list);

		} else {
			list = (ListBean) convertView.getTag();
		}

		TextView userName = (TextView) convertView
				.findViewById(R.id.friend_name);
		ImageView userImage = (ImageView) convertView
				.findViewById(R.id.friend_image);
		TextView userComment = (TextView) convertView
				.findViewById(R.id.friend_comment);

		if (usersReserve.get(position).getUserDTO() != null) {
			userName.setText(usersReserve.get(position).getUserDTO().getName());
			userName.setTextColor(Color.BLACK);
			if (usersReserve.get(position).getUserDTO().getPicture() != null) {
				Bitmap bitmap = BitmapFactory
						.decodeByteArray(usersReserve.get(position)
								.getUserDTO().getPicture(), 0, usersReserve
								.get(position).getUserDTO().getPicture().length);
				userImage.setImageBitmap(bitmap);
			}
			userComment.setText(usersReserve.get(position).getComment());

		}

		return convertView;
	}

	public class ListBean {

		View rowView;

		ListBean(View rowView) {
			this.rowView = rowView;
		}
	}
}
