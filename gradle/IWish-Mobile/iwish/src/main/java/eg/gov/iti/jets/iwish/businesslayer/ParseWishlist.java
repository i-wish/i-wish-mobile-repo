package eg.gov.iti.jets.iwish.businesslayer;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import eg.gov.iti.jets.iwish.dto.FriendReservedProduct;
import eg.gov.iti.jets.iwish.dto.ProductDTO;
import eg.gov.iti.jets.iwish.dto.UserDTO;
import eg.gov.iti.jets.iwish.dto.UserReserveProduct;
import eg.gov.iti.jets.iwish.datalayer.FileAccess;
import eg.gov.iti.jets.iwish.datalayer.WSConnection;
import eg.gov.iti.jets.iwish.util.DataUtil;

public class ParseWishlist {

	Context context;
	private String DELETE_URL = DataUtil.WS_URL+"wishlist/delete";

	public ParseWishlist(Context context) {
		this.context = context;
	}

	public boolean checkNetworkConnection() {
		ConnectivityManager manager = (ConnectivityManager) context
				.getSystemService(context.CONNECTIVITY_SERVICE);
		NetworkInfo wifiNetworkInfo = manager
				.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		NetworkInfo mobileNetworkInfo = manager
				.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		if (wifiNetworkInfo.isConnected() || mobileNetworkInfo.isConnected()) {
			return true;
		}
		return false;
	}

	public boolean addToWishlist(String url, ArrayList<Integer> wishlistItems,
			UserDTO user) {

		String result;
		Gson json = new Gson();
		String wishListItemsJSON = json.toJson(wishlistItems);
		url += "?ids=" + wishListItemsJSON + "&email=" + user.getEmail();

		if (checkNetworkConnection()) {
			WSConnection wsConnection = new WSConnection();
			result = wsConnection.get(url);
		} else {
			result = "network error";
		}

		if (result.equals("Successful")) {
			return true;
		} else {
			return false;
		}
	}

	public ArrayList<ProductDTO> getWishList(UserDTO userDTO) {

		String url = DataUtil.WS_URL+"products/wishlist?email="
				+ userDTO.getEmail();
		WSConnection wsConnection = new WSConnection();
		String result = wsConnection.get(url);
		if (result != "exception") {
			Type typeOfT = new TypeToken<ArrayList<ProductDTO>>() {
			}.getType();
			Gson gson = new Gson();
			ArrayList<ProductDTO> wishlist = gson.fromJson(result, typeOfT);
			return wishlist;
		}
		return null;
	}

	public boolean reserveGift(ProductDTO productDTO, UserDTO userDTO,
			UserDTO friendDTO) {

		String result;
		String url =DataUtil.WS_URL+"wishlist/giftreserve?product_id="
				+ productDTO.getId() + "&user_id=" + userDTO.getId()
				+ "&friend_id=" + friendDTO.getId();

		if (checkNetworkConnection()) {
			WSConnection wsConnection = new WSConnection();
			result = wsConnection.get(url);
		} else {
			result = "network error";
		}

		if (result.equals("Successful")) {
			return true;
		} else
			return false;
	}

	public boolean reserveWithOthers(ProductDTO productDTO, UserDTO userDTO,
			UserDTO friendDTO, double participate, String comment) {

		String result;
		String url = DataUtil.WS_URL+"wishlist/reserve/withothers?product_id="
				+ productDTO.getId() + "&user_id=" + userDTO.getId()
				+ "&friend_id=" + friendDTO.getId() + "&participate="
				+ participate + "&comment=" + comment;

		if (checkNetworkConnection()) {
			WSConnection wsConnection = new WSConnection();
			result = wsConnection.get(url);
		} else {
			result = "network error";
		}

		if (result.equals("Successful")) {
			return true;
		} else
			return false;
	}

	public ArrayList<FriendReservedProduct> getReservedProducts(UserDTO userDTO) {

		String result;
		String url =DataUtil.WS_URL+"wishlist/participate?friend_id="
				+ userDTO.getId();
		if (checkNetworkConnection()) {
			WSConnection wsConnection = new WSConnection();
			result = wsConnection.get(url);

			Log.i("reserved products", result);
			if (result != null) {
				Gson gson = new GsonBuilder().setDateFormat("MMM dd, yyyy").create();
				Type typeOfT = new TypeToken<ArrayList<FriendReservedProduct>>() {
				}.getType();
				ArrayList<FriendReservedProduct> reservedProducts = gson
						.fromJson(result, typeOfT);
				return reservedProducts;
			}
		}
		return null;
	}

	public Boolean deleteList(List<ProductDTO> wishesToDelete) {
		Gson json = new Gson();
		String out;
		String listJson = json.toJson(wishesToDelete);
		UserDTO user = new UserDTO();

		user.setFacebookUserId(getFb_id());
		requestJson req = new requestJson();
		req.user = user;
		req.products = wishesToDelete;
		String reqJson = json.toJson(req);
		String userJson = json.toJson(user);

		if (checkNetworkConnection()) {
			WSConnection conn = new WSConnection();

			if ((out = conn.post(DELETE_URL,
					reqJson)) != null) {
				response res = json.fromJson(out, response.class);
				if (res.error)
					return false;
				else
					return true;

			}
		}
		return false;
	}

	public String getFb_id() {
		SharedPreferences pref = context.getSharedPreferences(
				DataUtil.USER_PREF, 0);
		String fb_id = pref.getString(DataUtil.USER_FB_ID, null);
		return fb_id;

	}

	

	public boolean confirmGiftReceived(UserDTO userDTO, ProductDTO productDTO) {

		String result;
		String url = DataUtil.WS_URL+"wishlist/confirm?user_id="
				+ userDTO.getId() + "&product_id=" + productDTO.getId();
		if (checkNetworkConnection()) {
			WSConnection wsConnection = new WSConnection();
			result = wsConnection.get(url);
		} else {
			result = "network error";
		}
		if (result.equals("success")) {
			return true;
		} else {
			return false;
		}
	}

	public ArrayList<UserReserveProduct> getFriendsReservedProduct(
			UserDTO friend, ProductDTO productDTO) {

		String url = DataUtil.WS_URL+"wishlist/reserving/friends?friend_id="
				+ friend.getId() + "&product_id=" + productDTO.getId();
		if (checkNetworkConnection()) {
			WSConnection wsConnection = new WSConnection();
			String result = wsConnection.get(url);
			Log.i("reserve", result);
			if (result != null) {
				Gson gson = new GsonBuilder().setDateFormat("MMM dd, yyyy")
						.create();
				Type typeOfT = new TypeToken<ArrayList<UserReserveProduct>>() {
				}.getType();
				return gson.fromJson(result, typeOfT);
			}
		}
		return null;
	}

	class requestJson {
		UserDTO user;
		List<ProductDTO> products = new ArrayList<ProductDTO>();
	}

	public ArrayList<ProductDTO> readWishlist() {

		FileAccess fileAccess = new FileAccess(context);
		return (ArrayList<ProductDTO>) fileAccess.readFromFile(
				ProductDTO.class, DataUtil.WISHLIST);
	}

	public boolean saveWishlist(ArrayList<ProductDTO> productDTOs) {
		FileAccess fileAccess = new FileAccess(context);
		return fileAccess.writeToFile(productDTOs, DataUtil.WISHLIST);
	}
	
	public boolean cancelReservation(UserDTO user, ProductDTO product, UserDTO friend){
		
		String url = DataUtil.WS_URL+"wishlist/cancel?user_id="
				+ user.getId() + "&product_id=" + product.getId()+"&friend_id="+friend.getId();
		String result;
		if (checkNetworkConnection()){
			WSConnection wsConnection = new WSConnection();
			result = wsConnection.get(url);
			if (result.equals("success")){
				return true;
			}
		}
		return false;
	}

}
