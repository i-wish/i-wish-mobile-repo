package eg.gov.iti.jets.iwish.util;

public class DataUtil {
	
	/*public static final String IPADDRESS = "ipAdress";
	public static final String SHARED_PREFERENCES_IP = "sharedPrefIP";*/
	public static final String SELECTED_PRODUCT_ID = "selectedProductId";
	public static final String AccessToken_PREF = "accessToken";
	
	public static final String USER_PREF="user";
	public static final String USER_FB_ID="fb_id";
	public static final String USER_FB_name="user_name";
	public static final String USER_FB_email="user_email";
	public static final String USER_FB_img="user_img";
	public static final String USER_ID= "user_id";
	public static final String USER_JSON = "user_json";

	//Shared pref from ws
	public static final String WS_SHARED_PREFERENCES = "WS_sharedPref";
	public static final String LOGINUSER = "loginuser";
	public static final String LOGGEDIN = "loggedin";
	//data from ws
	public static final String PRODUCTS_HASHMAP = "products_hashmap";
	
	public static final String CATEGORIES = "categoryDTOs";
	public static final String COMPANIES = "companyDTOs";
	public static final String PRODUCTS = "productDTOs";
	
	public static final String CATEGORYID = "categoryId";
	public static final String COMPANYID = "companyId";
	
	public static final String CATEGORYDTO = "categoryDTO";
	public static final String COMPANYDTO = "companyDTO";
	
	public static final String CATEGORY_FRAGMENT_TAG = "category_fragment_tag";
	
	public static final String PRODUCTSSIZE = "productsSize";	
	public static final String PRODUCTDETAILS = "productDetails";
	
	
	public static final String FRIENDSELECTED = "friendSelected"; 
	public static final String FRIENDWISHLIST = "friendWishlist";
	public static final String FRIENDEVENTS = "friendEvents";
	public static final String FRIEND_EMAIL = "friend_email";
	
	public static final String UserReserveProduct = "user_reserve_product";
	public static final String PRODUCT_TO_RESERVE = "product_to_reserve";
	public static final String USER = "user";
	public static final String FRIEND = "friend";
	
	//caching
	public static final String WISHLIST = "wishlist";
	public static final String EVENTS = "events_caching";
	public static final String MY_EVENTS = "my_events";
	public static final String FRIENDS = "friends";
	public static final String NOTIFICATIONS = "notifications";
	
	public static final String WISHLIST_FRAGMENT_TAG = "wishlistTag";
	public static final String EVENT_FRAGMENT_TAG = "eventTag";
	public static final String CATEGORIES_FRAGMENT_TAG = "catTag";
	public static final String FRIEND_FRAGMENT_TAG = "friendTag";
	public static final String PROFILE_FRAGMENT_TAG = "profileTag";
	public static final String REGISTERED = "registered";
	
	//Notification data
	public static final String NOTIFICATION_PREF = "notification_pref";
	public static final String NOTIFICATION_DATA = "notification";
	
	//push not.
	public static final String EVENT_ID = "eventId";
	
	//ws url
	public static  String ipAddress = "";
	public static String WS_URL="http://"+ipAddress+":8080/IWish-Enterprise-ws/rest/";
	
}
