package eg.gov.iti.jets.iwish.controller;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.SharedPreferences;

import eg.gov.iti.jets.iwish.dto.FriendReservedProduct;
import eg.gov.iti.jets.iwish.dto.ProductDTO;
import eg.gov.iti.jets.iwish.dto.UserDTO;
import eg.gov.iti.jets.iwish.dto.UserReserveProduct;
import eg.gov.iti.jets.iwish.businesslayer.EventParser;
import eg.gov.iti.jets.iwish.businesslayer.ParseWishlist;
import eg.gov.iti.jets.iwish.util.DataUtil;



public class WishlistController {

	Context context = null;
	

	public WishlistController(Context context) {
		this.context = context;
	}


	

	public boolean addToWishlist(ArrayList<Integer> wishlistItems, UserDTO user) {

		
		String url = DataUtil.WS_URL+"wishlist/add";

		ParseWishlist parseWishlist = new ParseWishlist(context);
		return parseWishlist.addToWishlist(url, wishlistItems, user);
	}
	
	public boolean reserveGift(ProductDTO productDTO, UserDTO userDTO, UserDTO friendDTO){
		ParseWishlist parseWishlist = new ParseWishlist(context);
		return parseWishlist.reserveGift(productDTO, userDTO, friendDTO);
	}
	
	public boolean reserveWithOthers(ProductDTO productDTO, UserDTO userDTO, UserDTO friendDTO, double participate, String comment){
		ParseWishlist parseWishlist = new ParseWishlist(context);
		return parseWishlist.reserveWithOthers(productDTO, userDTO, friendDTO, participate, comment);
	}
	
	
	public ArrayList<FriendReservedProduct> getFriendReservedProducts (UserDTO userDTO){
		ParseWishlist parseWishlist = new ParseWishlist(context);
		return parseWishlist.getReservedProducts(userDTO);
	}

	public Boolean deleteWishes(List<ProductDTO> wishesToDelete) {
		ParseWishlist parser=new ParseWishlist(context);
		
		return parser.deleteList(wishesToDelete);
	}
	
	public boolean confirmGiftReceived (UserDTO userDTO, ProductDTO productDTO){
		ParseWishlist parseWishlist = new ParseWishlist(context);
		return parseWishlist.confirmGiftReceived(userDTO, productDTO);
	}
	
	public ArrayList<UserReserveProduct> getFriendsReservedProduct (UserDTO friend, ProductDTO productDTO){
		ParseWishlist parseWishlist = new ParseWishlist(context);
		return parseWishlist.getFriendsReservedProduct(friend, productDTO);
	}
	
	public ArrayList<ProductDTO> readWishlist (){
		ParseWishlist parseWishlist = new ParseWishlist(context);
		return parseWishlist.readWishlist();
	}
	
	public boolean saveWishlist (ArrayList<ProductDTO> productDTOs){
		ParseWishlist parseWishlist = new ParseWishlist(context);
		return parseWishlist.saveWishlist(productDTOs);
	}
	public boolean cancelReservation (UserDTO user, ProductDTO product, UserDTO friend){
		ParseWishlist parseWishlist = new ParseWishlist(context);
		return parseWishlist.cancelReservation(user, product, friend);
	}
}
