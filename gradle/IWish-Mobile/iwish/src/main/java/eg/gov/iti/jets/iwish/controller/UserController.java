package eg.gov.iti.jets.iwish.controller;

import eg.gov.iti.jets.iwish.asyncTasks.UserInfoTask;
import eg.gov.iti.jets.iwish.dto.UserDTO;
import eg.gov.iti.jets.iwish.util.DataUtil;

import android.content.Context;
import android.content.SharedPreferences;

public class UserController {
	
	public Context context;
	
	String accessToken;
	private UserDTO user;
	
	public String getAccessToken() {
		return accessToken;
	}
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	public void setUser(UserDTO user) {
		//store in sharedprefrences
		this.user = user;
	}
	
	public UserController(Context context) {
		this.context=context;
		
	}
	
	public void connectUserWS(){
		UserInfoTask userTask=new UserInfoTask();
		userTask.execute(this);
		
	}

}
