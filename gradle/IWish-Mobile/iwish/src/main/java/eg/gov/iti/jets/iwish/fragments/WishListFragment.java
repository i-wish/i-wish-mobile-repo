package eg.gov.iti.jets.iwish.fragments;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import eg.gov.iti.jets.iwish.activities.R;
import eg.gov.iti.jets.iwish.adapter.ListAdapter;
import eg.gov.iti.jets.iwish.controller.UserWishListController;
import eg.gov.iti.jets.iwish.controller.WishlistController;
import eg.gov.iti.jets.iwish.dto.ProductDTO;
import eg.gov.iti.jets.iwish.dto.UserDTO;
import eg.gov.iti.jets.iwish.util.DataUtil;
import eg.gov.iti.jets.iwish.activities.HomeActivity;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class WishListFragment extends Fragment {

	public List<ProductDTO> userWishList;
	List<ProductDTO> wishesToDelete;
	LinearLayout progressLayout;
	ListView wishlistListView;
	LinearLayout rootView;
	ListAdapter adapter;
	UserDTO userDTO;
	TextView noWishes;
	LinearLayout noWishesLayout;

	ImageView ic_add;
	ImageView ic_delete;

	boolean selected = false;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		userWishList = new ArrayList<ProductDTO>();
		rootView = (LinearLayout) inflater.inflate(R.layout.fragment_home,
				container, false);
		progressLayout = (LinearLayout) rootView
				.findViewById(R.id.mywishlist_progress_layout);
		wishlistListView = (ListView) rootView
				.findViewById(R.id.user_wishlist_list);
		noWishes = (TextView) rootView.findViewById(R.id.no_wishes);
		noWishesLayout = (LinearLayout) rootView.findViewById(R.id.no_wishes_layout);
		userWishList = new ArrayList<ProductDTO>();
		wishesToDelete = new ArrayList<ProductDTO>();
		

		// adapter = new ListAdapter(getActivity(), 0, 0, userWishList);
		// wishlistListView.setAdapter(adapter);

		ic_add = (ImageView) getActivity().findViewById(R.id.add);
		ic_delete = (ImageView) getActivity().findViewById(R.id.delete);

		ic_add.setVisibility(rootView.VISIBLE);
		
		
		wishlistListView
				.setOnItemLongClickListener(new OnItemLongClickListener() {

					@Override
					public boolean onItemLongClick(AdapterView<?> parent,
							View view, int position, long id) {

						if (!selected) {
							selected = true;
							ic_delete.setVisibility(view.VISIBLE);
							ic_add.setVisibility(view.INVISIBLE);

						}
						if (!wishesToDelete.contains(userWishList.get(position))) {
							wishesToDelete.add(userWishList.get(position));
							// view.findViewById(R.id.select).setVisibility(
							// view.VISIBLE);
							view.setSelected(true);

						} else {
							wishesToDelete.remove(userWishList.get(position));
							// view.findViewById(R.id.select).setVisibility(
							// view.INVISIBLE);
							view.setSelected(false);
							if (wishesToDelete.isEmpty()) {
								ic_delete.setVisibility(view.INVISIBLE);
								ic_add.setVisibility(view.VISIBLE);
								selected = false;
							}
						}

						return true;
					}
				});

		wishlistListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				if (selected) {
					if (!wishesToDelete.contains(userWishList.get(position))) {
						wishesToDelete.add(userWishList.get(position));
						// view.findViewById(R.id.select).setVisibility(view.VISIBLE);
						view.setSelected(true);

					} else {
						wishesToDelete.remove(userWishList.get(position));
						// view.findViewById(R.id.select).setVisibility(
						// view.INVISIBLE);
						view.setSelected(false);
						if (wishesToDelete.isEmpty()) {
							((HomeActivity) getActivity()).ic_delete
									.setVisibility(view.INVISIBLE);
							((HomeActivity) getActivity()).ic_add
									.setVisibility(view.VISIBLE);
							selected = false;
						}

					}
				} else {
					// go to wish detail
					Intent i = getActivity().getIntent();
					i.putExtra(DataUtil.PRODUCTDETAILS, userWishList.get(position));

					ProductDetailsFragment productDetailsFragment = new ProductDetailsFragment();
					productDetailsFragment.wishlist = true;
					getActivity()
							.getFragmentManager()
							.beginTransaction()
							.replace(R.id.frame_container, productDetailsFragment,
									DataUtil.CATEGORY_FRAGMENT_TAG)
							.addToBackStack("category fragment").commit();
				}
			}
		});

		ic_add.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				((HomeActivity) getActivity()).mDrawerList.setItemChecked(1,
						true);
				((HomeActivity) getActivity()).mDrawerList.setSelection(1);
				((HomeActivity) getActivity()).title.setText("Product");
				ic_add.setVisibility(View.GONE);

				CategoriesFragment fragment = new CategoriesFragment();
				FragmentManager fragmentManager = getFragmentManager();
				fragmentManager.beginTransaction()
						.replace(R.id.frame_container, fragment)
						.addToBackStack(null).commit();

			}
		});

		ic_delete.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				AlertDialog.Builder builder = new AlertDialog.Builder(
						getActivity());
				builder.setMessage("wishes will be deleted")
						.setPositiveButton("Delete",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {

										new DeleteTask().execute();

									}
								})
						.setNegativeButton("Cancel",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {

									}
								});
				// Create the AlertDialog object and return it
				builder.setTitle("Delete");
				builder.create().show();

			}
		});

		GetWishlistTask getWishlistTask = new GetWishlistTask();
		getWishlistTask.execute();
		return rootView;
	}

	class GetWishlistTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			((HomeActivity)getActivity()).backFromServer = false;
			progressLayout.setVisibility(rootView.VISIBLE);
			wishlistListView.setVisibility(rootView.GONE);
		}

		@Override
		protected Void doInBackground(Void... params) {

			SharedPreferences pref = getActivity().getSharedPreferences(
					DataUtil.USER_PREF, 0);
			String userJson = pref.getString(DataUtil.USER_JSON, null);
			Gson gson = new GsonBuilder().setDateFormat("MMM dd, yyyy")
					.create();
			userDTO = gson.fromJson(userJson, UserDTO.class);
			UserWishListController userWishListController = new UserWishListController(
					getActivity().getApplicationContext());
			WishlistController wishlistController = new WishlistController(
					getActivity().getApplicationContext());

			if (checkNetworkConnection()) {
				userWishList = userWishListController.getWishList(userDTO);
				if (userWishList != null) {
					if (wishlistController
							.saveWishlist((ArrayList<ProductDTO>) userWishList)) {
						Log.i("wishlist", "Caching done");
					} else {
						Log.i("wishlist", "Caching not done");
					}
				}
			} else {
				// return cached wishlist
				userWishList = wishlistController.readWishlist();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);

			progressLayout.setVisibility(rootView.GONE);
			if (userWishList == null || userWishList.size() == 0) {
				rootView.setGravity(Gravity.CENTER);
				noWishesLayout.setVisibility(View.VISIBLE);
			}

			else if (userWishList != null) {
				wishlistListView.setVisibility(rootView.VISIBLE);
				adapter = new ListAdapter(
						getActivity(), 0, 0,
						userWishList, true, userDTO);
				wishlistListView.setAdapter(adapter);
			} else {
				Toast.makeText(getActivity(), "Network Error",
						Toast.LENGTH_LONG).show();
			}
			((HomeActivity)getActivity()).backFromServer = true;
		}
	}

	public class DeleteTask extends AsyncTask<Void, Void, Boolean> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			((HomeActivity)getActivity()).backFromServer = false;
		}
		
		@Override
		protected Boolean doInBackground(Void... params) {
			WishlistController controller = new WishlistController(
					getActivity().getApplicationContext());
			boolean result = controller.deleteWishes(wishesToDelete);
			if (result) {
				ArrayList<ProductDTO> productDTOs = controller.readWishlist();
				if (productDTOs != null) {
					for (int i = 0; i < productDTOs.size(); i++) {
						for (ProductDTO wish : wishesToDelete) {
							if (wish.getId().equals(productDTOs.get(i).getId())) {
								productDTOs.remove(wish);
							}
						}
					}
					controller.saveWishlist(productDTOs);
				}
			}
			return result;

		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			if (result) {
				for (ProductDTO p : wishesToDelete) {
					userWishList.remove(p);

				}

				adapter = new ListAdapter(
						getActivity(), 0, 0,
						userWishList, true, userDTO);
				wishlistListView.setAdapter(adapter);
				// list.getAdapter().notify();
				wishesToDelete = new ArrayList<ProductDTO>();
				selected = false;
				((HomeActivity) getActivity()).ic_delete
						.setVisibility(View.INVISIBLE);
				((HomeActivity) getActivity()).ic_add
						.setVisibility(View.VISIBLE);
				Toast.makeText(getActivity(), "deleted", Toast.LENGTH_LONG)
						.show();
			} else {

				Toast.makeText(getActivity(), "couldn't be deleted",
						Toast.LENGTH_LONG).show();
			}
			((HomeActivity) getActivity()).ic_delete.setVisibility(View.GONE);
			((HomeActivity)getActivity()).backFromServer = true;
		}

	}

	public boolean checkNetworkConnection() {
		ConnectivityManager manager = (ConnectivityManager) getActivity()
				.getApplicationContext()
				.getSystemService(
						getActivity().getApplicationContext().CONNECTIVITY_SERVICE);
		NetworkInfo wifiNetworkInfo = manager
				.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		NetworkInfo mobileNetworkInfo = manager
				.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		if (wifiNetworkInfo.isConnected() || mobileNetworkInfo.isConnected()) {
			return true;
		}
		return false;
	}
}
