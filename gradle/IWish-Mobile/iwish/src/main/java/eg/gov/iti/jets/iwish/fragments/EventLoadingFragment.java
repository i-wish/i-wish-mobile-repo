package eg.gov.iti.jets.iwish.fragments;

import java.lang.reflect.Type;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import eg.gov.iti.jets.iwish.activities.R;
import eg.gov.iti.jets.iwish.activities.R.id;
import eg.gov.iti.jets.iwish.activities.R.layout;
import eg.gov.iti.jets.iwish.controller.EventController;
import eg.gov.iti.jets.iwish.dto.UserDTO;
import eg.gov.iti.jets.iwish.dto.UserEventDTO;
import eg.gov.iti.jets.iwish.util.DataUtil;

import android.app.Activity;
import android.app.Fragment;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

public class EventLoadingFragment extends Fragment{
	
	public int event_id;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.event_loading_fragment,container , false);
		new getEvent().execute();
		return rootView;
		
	}
	
	class getEvent extends AsyncTask<Void, Void, String>{

		@Override
		protected String doInBackground(Void... params) {
			try{
			EventController controller =new EventController(getActivity());
			return controller.getEvent(event_id);
			}catch(Exception ex){
				ex.printStackTrace();
				return null;
			}
		}
		
		@Override
		protected void onPostExecute(String result) {
			if(result !=null){
			try{
				
			EventController controller =new EventController(getActivity());
			UserEventDTO event = controller.parseEvent(result);
			EventDetailsFragment eventDetails =new EventDetailsFragment();
			SharedPreferences pref = getActivity().getSharedPreferences(
					DataUtil.USER_PREF, 0);
			String userJson = pref.getString(DataUtil.USER_JSON, null);
			Gson json = new GsonBuilder().setDateFormat("MMM dd, yyyy")
					.create();
			Type typeOfT = new TypeToken<UserDTO>() {
			}.getType();
			UserDTO userDTO = json.fromJson(userJson, typeOfT);
			eventDetails.event=event;
			if(event.getUserDTO().getId() != userDTO.getId()){
				eventDetails.isFriend=true;
			}
			getFragmentManager().popBackStack();
			getFragmentManager().beginTransaction().replace(R.id.frame_container, eventDetails).addToBackStack(null).commit();
			}catch(Exception ex){
				ex.printStackTrace();
				getFragmentManager().popBackStack();
			}
			}else{
				getFragmentManager().popBackStack();
			}
		}
		
	}

}
