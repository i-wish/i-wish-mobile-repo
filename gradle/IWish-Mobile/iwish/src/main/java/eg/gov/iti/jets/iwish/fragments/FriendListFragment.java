package eg.gov.iti.jets.iwish.fragments;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.R.layout;
import android.app.ActionBar.LayoutParams;
import android.app.Fragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.HeaderViewListAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import eg.gov.iti.jets.iwish.activities.HomeActivity;
import eg.gov.iti.jets.iwish.activities.R;
import eg.gov.iti.jets.iwish.adapter.FriendsAdapter;
import eg.gov.iti.jets.iwish.controller.FriendController;
import eg.gov.iti.jets.iwish.dto.UserDTO;
import eg.gov.iti.jets.iwish.util.DataUtil;

public class FriendListFragment extends Fragment {

	ArrayList<UserDTO> friendsList;
	FriendsAdapter adapter;
	EditText friends_search;
	List<UserDTO> searchFriendsResult;
	View header;
	UserDTO friend;
	//LinearLayout progressLayout;
	ListView friends;
	RelativeLayout layout;
	TextView noFriends;
	LinearLayout noFriendsLayout;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		friendsList = new ArrayList<UserDTO>();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		layout = (RelativeLayout) inflater.inflate(R.layout.friends_fragment,
				container, false);
		friends = (ListView) layout.findViewById(R.id.list);
		header = inflater.inflate(R.layout.header, friends, false);
		friends_search = (EditText) header.findViewById(R.id.friend_search);
//		progressLayout = (LinearLayout) layout
//				.findViewById(R.id.friend_progress_layout);
		noFriends = (TextView) layout.findViewById(R.id.no_friends);
		noFriendsLayout = (LinearLayout) layout
				.findViewById(R.id.no_friends_layout);

		// FriendController friendController = new
		// FriendController(getActivity()
		// .getApplicationContext());
		// friendsList = friendController.getFriends();
		if (friendsList.size() == 0) {
			GetFriendsTask getFriendsTask = new GetFriendsTask();
			getFriendsTask.execute();
		}
		// if (friendsList == null) {
		//
		// } else {
		searchFriendsResult = new ArrayList<UserDTO>();
		searchFriendsResult.addAll(friendsList);

		adapter = new FriendsAdapter(getActivity(), R.layout.friends_item,
				R.id.friend_name, friendsList);
		friends.addHeaderView(header);
		friends.setAdapter(adapter);

		// }

		friends.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				friend = friendsList.get(position - 1);
				Intent i = getActivity().getIntent();
				i.putExtra(DataUtil.FRIENDSELECTED, friend);

				FriendProfileFragment friendProfileFragment = new FriendProfileFragment();
				getActivity()
						.getFragmentManager()
						.beginTransaction()
						.replace(R.id.frame_container, friendProfileFragment,
								DataUtil.CATEGORY_FRAGMENT_TAG)
						.addToBackStack(null).commit();
			}
		});
		//
		// if (friendsList != null) {
		friends_search.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// FriendListFragment.this.adapter.getFilter().filter(s);
				String text = friends_search.getText().toString()
						.toLowerCase(Locale.getDefault());
				friendsList.clear();

				if (text.length() == 0) {
					friendsList.addAll(searchFriendsResult);
				} else {
					for (UserDTO userDTO : searchFriendsResult) {
						if (userDTO.getName().toLowerCase().contains(text)) {
							friendsList.add(userDTO);

						}
					}
				}
				adapter.notifyDataSetChanged();
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {

			}

			@Override
			public void afterTextChanged(Editable s) {
				// String text =
				// friends_search.getText().toString().toLowerCase(Locale.getDefault());
				// adapter.getFilter().filter(text);
			}
		});
		// }
		return layout;
	}

	class GetFriendsTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			
			((HomeActivity) getActivity()).progressBar.setVisibility(View.VISIBLE);
			friends.setVisibility(View.GONE);
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {

			try{
			SharedPreferences pref = getActivity().getSharedPreferences(
					DataUtil.USER_PREF, 0);
			String userJson = pref.getString(DataUtil.USER_JSON, null);
			Gson json = new GsonBuilder().setDateFormat("MMM dd, yyyy")
					.create();
			Type typeOfT = new TypeToken<UserDTO>() {
			}.getType();
			UserDTO userDTO = json.fromJson(userJson, typeOfT);
			FriendController friendController = new FriendController(
					getActivity().getApplicationContext());
			if (checkNetworkConnection()) {
				friendsList.addAll(friendController.getFriends(userDTO));
				if (friendsList != null) {
					friendController.saveFriends(friendsList, DataUtil.FRIENDS);
				}
			} else {
				friendsList.addAll(friendController.getFriends());
			}
			}catch(Exception ex){
				ex.printStackTrace();
				friendsList= null;
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			((HomeActivity) getActivity()).progressBar.setVisibility(View.GONE);

			if (friendsList == null){
				Toast.makeText(getActivity(), "couldn't retrieve friends",
						Toast.LENGTH_LONG).show();
				
			}else if (friendsList.size() == 0) {
				noFriendsLayout.setGravity(Gravity.CENTER);
				noFriends.setVisibility(View.VISIBLE);

			} else {
				friends.setVisibility(layout.VISIBLE);
				// FriendsAdapter adapter2 =
				// (FriendsAdapter)((HeaderViewListAdapter)friends.getAdapter()).getWrappedAdapter();
				searchFriendsResult.addAll(friendsList);
				adapter.notifyDataSetChanged();

			}
		}
	}

	public boolean checkNetworkConnection() {
		ConnectivityManager manager = (ConnectivityManager) getActivity()
				.getApplicationContext()
				.getSystemService(
						getActivity().getApplicationContext().CONNECTIVITY_SERVICE);
		NetworkInfo wifiNetworkInfo = manager
				.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		NetworkInfo mobileNetworkInfo = manager
				.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		if (wifiNetworkInfo.isConnected() || mobileNetworkInfo.isConnected()) {
			return true;
		}
		return false;
	}
}
