package eg.gov.iti.jets.iwish.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import eg.gov.iti.jets.iwish.activities.R;
import eg.gov.iti.jets.iwish.dto.ProductDTO;



public class ProductsListAdapter extends BaseAdapter{
	List<ProductDTO> adapterList = new ArrayList<ProductDTO>();
	Context context;
	
	public ProductsListAdapter(Context context, ArrayList<ProductDTO> adapterList) {
		this.adapterList = adapterList;
		this.context = context;
	}

	@Override
	public int getCount() {
		return adapterList.size();
	}

	@Override
	public Object getItem(int position) {
		return adapterList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View rootView = LayoutInflater.from(context)
				.inflate(R.layout.product_cell, parent, false);
		TextView text = (TextView) rootView.findViewById(R.id.product_name);
		text.setText(adapterList.get(position).getName());
		text.setTextColor(Color.BLACK);
		ImageView productImage = (ImageView) rootView
				.findViewById(R.id.product_image);
		if (adapterList.get(position).getImage() != null) {
			Bitmap bitmap = BitmapFactory.decodeByteArray(
					adapterList.get(position).getImage(), 0, adapterList
							.get(position).getImage().length);
			productImage.setImageBitmap(bitmap);
		}

		TextView price = (TextView) rootView
				.findViewById(R.id.product_price);
		price.setTextColor(Color.BLACK);
		price.setText(String.valueOf(adapterList.get(position).getPrice()));
		return rootView;
	}

	public void appendMoredata(List<ProductDTO> productDTOs) {
		Log.i("appendMoredata", "appendMoredata in");
		this.adapterList.addAll(productDTOs);
		notifyDataSetChanged();
	}

}
