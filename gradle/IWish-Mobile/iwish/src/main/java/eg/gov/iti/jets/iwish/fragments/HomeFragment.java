package eg.gov.iti.jets.iwish.fragments;

import java.util.ArrayList;
import java.util.Collections;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import eg.gov.iti.jets.iwish.activities.HomeActivity;
import eg.gov.iti.jets.iwish.activities.R;
import eg.gov.iti.jets.iwish.adapter.EventListAdapter;
import eg.gov.iti.jets.iwish.controller.EventController;
import eg.gov.iti.jets.iwish.dto.UserDTO;
import eg.gov.iti.jets.iwish.dto.UserEventDTO;
import eg.gov.iti.jets.iwish.util.DataUtil;

public class HomeFragment extends Fragment {

	LinearLayout layout;
	// LinearLayout progressLayout;
	ProgressBar progressBar;
	ListView eventsListView;
	ArrayList<UserEventDTO> userEventDTO;
	UserDTO userDTO;
	EventListAdapter adapter;
	int eventId;
	LinearLayout list_layout;
	TextView noData;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		userEventDTO = new ArrayList<UserEventDTO>();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		layout = (LinearLayout) inflater.inflate(
				R.layout.home_page_fragment_layout, container, false);
		// progressLayout = (LinearLayout) layout
		// .findViewById(R.id.home_progress_layout);
		progressBar = (ProgressBar) getActivity()
				.findViewById(R.id.progressBar);
		progressBar.setVisibility(View.VISIBLE);

		eventsListView = (ListView) layout.findViewById(R.id.home_list);
		list_layout = (LinearLayout) layout.findViewById(R.id.list_layout);
		noData = (TextView) layout.findViewById(R.id.no_events);

		adapter = new EventListAdapter(getActivity(), 0, userEventDTO, true);
		adapter.isFriendsEvent = true;

		eventsListView.setAdapter(adapter);
		if (userEventDTO.size() == 0) {
			GetFriendsEvents getFriendsTask = new GetFriendsEvents();
			getFriendsTask.execute();
		}

		eventsListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				EventDetailsFragment eventDetailsFragment = new EventDetailsFragment();
				eventDetailsFragment.event = userEventDTO.get(position);
				eventDetailsFragment.isFriend = true;
				getFragmentManager().beginTransaction()
						.replace(R.id.frame_container, eventDetailsFragment)
						.addToBackStack(null).commit();
			}
		});

		return layout;
	}

	class GetFriendsEvents extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// ((HomeActivity)getActivity()).backFromServer = false;
			// progressLayout.setVisibility(View.VISIBLE);
			// list_layout.setVisibility(View.GONE);
		}

		@Override
		protected Void doInBackground(Void... params) {
			try {

				SharedPreferences pref = getActivity().getSharedPreferences(
						DataUtil.USER_PREF, 0);
				String userJson = pref.getString(DataUtil.USER_JSON, null);
				Gson gson = new GsonBuilder().setDateFormat("MMM dd, yyyy")
						.create();
				userDTO = gson.fromJson(userJson, UserDTO.class);
				EventController eventController = new EventController(
						getActivity().getApplicationContext());
				if (checkNetworkConnection()) {
					userEventDTO = eventController.getFriendsEvents(userDTO);
					if (userEventDTO != null && userEventDTO.size() > 0) {
						if (eventController.saveEvents(userEventDTO,
								DataUtil.EVENTS)) {
							Log.i("event", "Caching done");
						} else {
							Log.i("event", "Caching not done");
						}
					}
				} else {
					userEventDTO = (ArrayList<UserEventDTO>) eventController
							.readEvent(DataUtil.EVENTS);
				}
			} catch (Exception ex) {
				ex.printStackTrace();
				return null;
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (!HomeFragment.this.isHidden()
					&& !HomeFragment.this.isRemoving()) {
				progressBar.setVisibility(View.GONE);

				if (userEventDTO != null) {
					if (userEventDTO.size() == 0) {

						list_layout.setVisibility(View.VISIBLE);
						list_layout.setGravity(Gravity.CENTER);
						eventsListView.setVisibility(View.GONE);
						noData.setVisibility(View.VISIBLE);
					} else if (userEventDTO.size() > 0) {
						list_layout.setVisibility(View.VISIBLE);
						if (userEventDTO.size() > 1)
							Collections.sort(userEventDTO);
						adapter = new EventListAdapter(getActivity()
								.getApplicationContext(), 0, userEventDTO, true);
						eventsListView.setAdapter(adapter);
					}
					// ((HomeActivity)getActivity()).backFromServer = true;

				} else {
					list_layout.setVisibility(View.VISIBLE);
					list_layout.setGravity(Gravity.CENTER);
					eventsListView.setVisibility(View.GONE);
					Toast.makeText(HomeActivity.HOME_CONTEXT,
							"couldn't retrieve feeds", Toast.LENGTH_LONG)
							.show();
				}
			}
		}
	}

	public boolean checkNetworkConnection() {
		ConnectivityManager manager = (ConnectivityManager) getActivity()
				.getApplicationContext()
				.getSystemService(
						getActivity().getApplicationContext().CONNECTIVITY_SERVICE);
		NetworkInfo wifiNetworkInfo = manager
				.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		NetworkInfo mobileNetworkInfo = manager
				.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		if (wifiNetworkInfo.isConnected() || mobileNetworkInfo.isConnected()) {
			return true;
		}
		return false;
	}

}
