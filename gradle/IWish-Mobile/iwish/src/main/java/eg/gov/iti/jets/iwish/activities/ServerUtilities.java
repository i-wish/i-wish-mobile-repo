package eg.gov.iti.jets.iwish.activities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import com.google.android.gcm.GCMRegistrar;
import eg.gov.iti.jets.iwish.activities.R;
import eg.gov.iti.jets.iwish.controller.EventController;
import eg.gov.iti.jets.iwish.util.DataUtil;

import eg.gov.iti.jets.iwish.dto.UserDTO;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import static eg.gov.iti.jets.iwish.activities.CommonUtilites.TAG;
import static eg.gov.iti.jets.iwish.activities.CommonUtilites.displayMessage;



public class ServerUtilities {

	private static final int MAX_ATTEMPTS = 5;
    private static final int BACKOFF_MILLI_SECONDS = 2000;
    private static final Random random = new Random();
    static SharedPreferences pref;
 
    /**
     * Register this account/device pair within the server.
     *
     */
    public static void register(final Context context, final String regId , final int userId) {
        Log.i(TAG, "registering device (regId = " + regId + ")");
         //pref = context.getSharedPreferences(DataUtil.SHARED_PREFERENCES_IP, 0);
		
        //Map<String, String> params = new HashMap<String, String>();
        //params.put("regId", regId);
       // params.put("name", name);
       // params.put("email", email);
         
        long backoff = BACKOFF_MILLI_SECONDS + random.nextInt(1000);
        // Once GCM returns a registration id, we need to register on our server
        // As the server might be down, we will retry it a couple
        // times.
        for (int i = 1; i <= MAX_ATTEMPTS; i++) {
            Log.d(TAG, "Attempt #" + i + " to register");
            try {
                displayMessage(context, context.getString(R.string.server_registering, i, MAX_ATTEMPTS));
                post(userId ,regId);
                GCMRegistrar.setRegisteredOnServer(context, true);
                String message = context.getString(R.string.server_registered);
                CommonUtilites.displayMessage(context, message);
                
                return;
            } catch (IOException e) {
                // Here we are simplifying and retrying on any error; in a real
                // application, it should retry only on unrecoverable errors
                // (like HTTP error code 503).
                Log.e(TAG, "Failed to register on attempt " + i + ":" + e);
                if (i == MAX_ATTEMPTS) {
                    break;
                }
                try {
                    Log.d(TAG, "Sleeping for " + backoff + " ms before retry");
                    Thread.sleep(backoff);
                } catch (InterruptedException e1) {
                    // Activity finished before we complete - exit.
                    Log.d(TAG, "Thread interrupted: abort remaining retries!");
                    Thread.currentThread().interrupt();
                    return;
                }
                // increase backoff exponentially
                backoff *= 2;
            }
        }
        String message = context.getString(R.string.server_register_error,MAX_ATTEMPTS);
        CommonUtilites.displayMessage(context, message);
        
    }
 
    /**
     * Unregister this account/device pair within the server.
     */
   public static void unregister(final Context context, final String regId ,final int userId) {
        Log.i(TAG, "unregistering device (regId = " + regId + ")");
        //String serverUrl = SERVER_URL + "/unregister";
        Map<String, String> params = new HashMap<String, String>();
        params.put("regId", regId);
        try {
            post(userId,regId);
            GCMRegistrar.setRegisteredOnServer(context, false);
            String message = context.getString(R.string.server_unregistered);
            CommonUtilites.displayMessage(context, message);
            
        } catch (IOException e) {
            // At this point the device is unregistered from GCM, but still
            // registered in the server.
            // We could try to unregister again, but it is not necessary:
            // if the server tries to send a message to the device, it will get
            // a "NotRegistered" error message and should unregister the device.
            String message = context.getString(R.string.server_unregister_error,e.getMessage());
            CommonUtilites.displayMessage(context, message);
            
        }
    }
 
    /**
     * Issue a POST request to the server.
     *
     * @param endpoint POST address.
     * @param params request parameters.
     *
     * @throws IOException propagated from POST.
     */
    private static void post(final int userId ,final String regId)
            throws IOException {    
         
       /* URL url;
        try {
            url = new URL(endpoint+"="+regId);
        } catch (MalformedURLException e) {
            throw new IllegalArgumentException("invalid url: " + endpoint);
        }
        StringBuilder bodyBuilder = new StringBuilder();
        Iterator<Entry<String, String>> iterator = params.entrySet().iterator();
        // constructs the POST body using the parameters
        while (iterator.hasNext()) {
            Entry<String, String> param = iterator.next();
            bodyBuilder.append(param.getKey()).append('=').append(param.getValue());
            if (iterator.hasNext()) {
                bodyBuilder.append('&');
            }
        }
        String body = bodyBuilder.toString();
        Log.v(TAG, "Posting '" + body + "' to " + url);
        byte[] bytes = body.getBytes();
        HttpURLConnection conn = null;
        try {
            Log.e("URL", "> " + url);
            conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setUseCaches(false);
            conn.setFixedLengthStreamingMode(bytes.length);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type","application/x-www-form-urlencoded;charset=UTF-8");*/
    		String response = "";
    		//String ipAddress = pref.getString(DataUtil.IPADDRESS, null);
    		String SERVER_URL =DataUtil.WS_URL+"BroadCast?regId="
    				+ regId + "&userId=" + userId;
    		try{
            DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpGet httpget = new HttpGet(SERVER_URL);
			Log.i("POST MESSAGE", "after HTTP POST");
			Log.i("POST MESSAGE", regId);
			HttpResponse httpResponse = httpClient.execute(httpget);

				HttpEntity httpEntity = httpResponse.getEntity();
				InputStream is;

				is = httpEntity.getContent();
				
				BufferedReader input = new BufferedReader(new InputStreamReader(is));
				String line = null;

				while ((line = input.readLine()) != null) {
					
					response+=line;
				}
				Log.i("POST MESSAGE", response);
				input.close();
    } catch (ClientProtocolException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
            // post the request
           /* OutputStream out = conn.getOutputStream();
            out.write(bytes);
            out.close();
            // handle the response
            int status = conn.getResponseCode();
            if (status != 200) {
              throw new IOException("Post failed with error code " + status);
            }
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
      }*/
			}
    
}
