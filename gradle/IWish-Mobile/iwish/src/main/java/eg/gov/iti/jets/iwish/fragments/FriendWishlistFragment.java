package eg.gov.iti.jets.iwish.fragments;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import eg.gov.iti.jets.iwish.activities.HomeActivity;
import eg.gov.iti.jets.iwish.activities.R;
import eg.gov.iti.jets.iwish.adapter.ListAdapter;
import eg.gov.iti.jets.iwish.controller.FriendController;
import eg.gov.iti.jets.iwish.controller.WishlistController;
import eg.gov.iti.jets.iwish.dto.FriendReservedProduct;
import eg.gov.iti.jets.iwish.dto.NotificationDTO;
import eg.gov.iti.jets.iwish.dto.ProductDTO;
import eg.gov.iti.jets.iwish.dto.UserDTO;
import eg.gov.iti.jets.iwish.dto.UserReserveProduct;
import eg.gov.iti.jets.iwish.util.DataUtil;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

public class FriendWishlistFragment extends Fragment {

	View layout;
	ListView wishlistListView;
	//ProgressBar progressBar;
	//LinearLayout progressLayout;

	List<ProductDTO> userWishList;
	ArrayList<FriendReservedProduct> friendReservedProducts;
	String email;
	ListAdapter adapter;
	UserDTO friend;
	ProductDTO product;
	ArrayList<UserReserveProduct> userReserveProducts;
	LinearLayout noWishes;
	UserDTO userDTO;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		userWishList = new ArrayList<ProductDTO>();
		friendReservedProducts = new ArrayList<FriendReservedProduct>();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		layout = inflater.inflate(R.layout.friend_wishlist_fragment, container,
				false);
		wishlistListView = (ListView) layout
				.findViewById(R.id.friend_wishlist_list);
//		progressLayout = (LinearLayout) layout
//				.findViewById(R.id.wishlist_progress_layout);
//		progressBar = (ProgressBar) layout.findViewById(R.id.progressBar3);
		noWishes = (LinearLayout) layout
				.findViewById(R.id.no_friend_wishes_layout);

		Intent i = getActivity().getIntent();
		if (i.getSerializableExtra(DataUtil.FRIENDSELECTED) != null) {
			friend = (UserDTO) i.getSerializableExtra(DataUtil.FRIENDSELECTED);
		}
//		if (userWishList.size() == 0 && friendReservedProducts.size() == 0) {
			FriendWishListTask friendWishListTask = new FriendWishListTask();
			friendWishListTask.execute();
//		}
		wishlistListView.setAdapter(new ListAdapter(getActivity(),
				R.layout.product_cell, R.id.product_name, userWishList,
				friendReservedProducts, false, friend, userDTO));

		wishlistListView
				.setOnItemClickListener(new AdapterView.OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> parent, View view,
							int position, long id) {
						product = userWishList.get(position);
						GetFriendsReservedProduct getFriendsReservedProduct = new GetFriendsReservedProduct();
						getFriendsReservedProduct.execute();

					}
				});

		return layout;
	}

	class FriendWishListTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
//			((HomeActivity)getActivity()).backFromServer = false;
//			progressLayout.setVisibility(layout.VISIBLE);
			((HomeActivity) getActivity()).progressBar.setVisibility(View.VISIBLE);
			wishlistListView.setVisibility(layout.GONE);
		}

		@Override
		protected Void doInBackground(Void... params) {

			try{
			FriendController friendController = new FriendController(
					HomeActivity.HOME_CONTEXT);
			userWishList = friendController.getFriendWishlist(friend);

			WishlistController wishlistController = new WishlistController(
					HomeActivity.HOME_CONTEXT);
			friendReservedProducts = wishlistController
					.getFriendReservedProducts(friend);
			}catch(Exception ex){
				ex.printStackTrace();
				friendReservedProducts=null;
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {

			
			((HomeActivity) getActivity()).progressBar.setVisibility(View.GONE);
			try{
			SharedPreferences pref = HomeActivity.HOME_CONTEXT
					.getSharedPreferences(DataUtil.USER_PREF, 0);
			String userJson = pref.getString(DataUtil.USER_JSON, null);
			Gson json = new GsonBuilder().setDateFormat("MMM dd, yyyy")
					.create();
			Type typeOfT = new TypeToken<UserDTO>() {
			}.getType();
			userDTO = json.fromJson(userJson, typeOfT);

			if (userWishList == null || userWishList.size() == 0) {
				noWishes.setVisibility(View.VISIBLE);
			}

			else if (friendReservedProducts != null && userWishList != null) {
				wishlistListView.setVisibility(layout.VISIBLE);
				wishlistListView.setAdapter(new ListAdapter(getActivity(),
						R.layout.product_cell, R.id.product_name, userWishList,
						friendReservedProducts, false, friend, userDTO));
			}
			if (friendReservedProducts == null || userWishList == null) {
				Toast.makeText(getActivity(),
						"Please, check network connection", Toast.LENGTH_LONG)
						.show();
			}
			((HomeActivity)getActivity()).backFromServer = true;
		}catch(Exception ex){
			ex.printStackTrace();
		}}
		

	}

	class GetFriendsReservedProduct extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			//((HomeActivity)getActivity()).backFromServer = false;
			((HomeActivity) getActivity()).progressBar.setVisibility(View.VISIBLE);
		}

		@Override
		protected Void doInBackground(Void... params) {
			WishlistController wishlistController = new WishlistController(
					getActivity().getApplicationContext());
			userReserveProducts = wishlistController.getFriendsReservedProduct(
					friend, product);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			((HomeActivity) getActivity()).progressBar.setVisibility(View.VISIBLE);
			if (userReserveProducts != null) {
				SharedPreferences pref = HomeActivity.HOME_CONTEXT
						.getSharedPreferences(DataUtil.USER_PREF, 0);
				String userJson = pref.getString(DataUtil.USER_JSON, null);
				Gson json = new GsonBuilder().setDateFormat("MMM dd, yyyy")
						.create();
				Type typeOfT = new TypeToken<UserDTO>() {
				}.getType();
				UserDTO user = json.fromJson(userJson, typeOfT);

				Intent i = getActivity().getIntent();
				i.putExtra(DataUtil.UserReserveProduct, userReserveProducts);
				i.putExtra(DataUtil.PRODUCT_TO_RESERVE, product);
				i.putExtra(DataUtil.USER, friend);
				i.putExtra(DataUtil.FRIEND, user);

				ReserveProductFragment reserveProductFragment = new ReserveProductFragment();
				getActivity().getFragmentManager().beginTransaction()
						.replace(R.id.frame_container, reserveProductFragment)
						.addToBackStack("reserveFrag").commit();
			} else {
				Toast.makeText(getActivity(), "Network Error",
						Toast.LENGTH_LONG).show();
			}
			((HomeActivity)getActivity()).backFromServer = true;
		}
	}
}
