package eg.gov.iti.jets.iwish.controller;

import java.util.List;

import eg.gov.iti.jets.iwish.activities.HomeActivity;
import eg.gov.iti.jets.iwish.businesslayer.GreetingParser;
import eg.gov.iti.jets.iwish.dto.EventCommentsDTO;
import eg.gov.iti.jets.iwish.dto.EventTypeDTO;
import eg.gov.iti.jets.iwish.dto.GreetingDTO;
import eg.gov.iti.jets.iwish.dto.UserDTO;
import eg.gov.iti.jets.iwish.dto.UserEventDTO;
import eg.gov.iti.jets.iwish.util.DataUtil;

import android.content.Context;
import android.content.SharedPreferences;

public class GreetingController {

	

	public Context context;

	public GreetingController(Context context) {
		this.context = context;
	}

	public String getGreetingSuggestion(EventTypeDTO eventTypeDTO) {
		GreetingParser parser = new GreetingParser();
		
		return parser.getGreetingSuggestion(eventTypeDTO);

	}

	public List<GreetingDTO> parseGreetingSuggestions(String result) {
		GreetingParser parser = new GreetingParser();
		return parser.parseGreetingSuggestions(result);

	}

	public String sendGreeting(EventCommentsDTO comment) {
		GreetingParser parser = new GreetingParser();
		
		return parser.sendGreeting(comment);
	}

	public boolean parseSendGreeting(String result) {
		GreetingParser parser = new GreetingParser();
		return parser.isGreetingSent(result);
	}

	public String getGreetings(UserEventDTO event) {
		GreetingParser parser = new GreetingParser();
		
		return parser.getGreeting(event);
	}

	public List<EventCommentsDTO> parseGreetings(String result) {
		GreetingParser parser = new GreetingParser();
		return parser.parseGreetings(result);
	}

}
