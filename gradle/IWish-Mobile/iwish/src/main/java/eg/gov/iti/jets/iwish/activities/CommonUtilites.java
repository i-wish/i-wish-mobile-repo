package eg.gov.iti.jets.iwish.activities;

import android.content.Context;
import android.content.Intent;

public class CommonUtilites {

	//public static final String SERVER_URL = "http://10.145.68.200:8080/IWish-Enterprise-Application-ws/rest/BroadCast/register?RegId";
	 
    // Google project id
    public static final String SENDER_ID = "708450115677"; 
 
    /**
     * Tag used on log messages.
     */
    static final String TAG = "Android GCM";
 
    public static final String DISPLAY_MESSAGE_ACTION ="com.example.pushnotificationtest.DISPLAY_MESSAGE";
 
    public static final String EXTRA_MESSAGE = "message";
 
    /**
     * Notifies UI to display a message.
     * <p>
     * This method is defined in the common helper because it's used both by
     * the UI and the background service.
     *
     * @param context application's context.
     * @param message message to be displayed.
     */
    static void displayMessage(Context context, String message) {
        Intent intent = new Intent(DISPLAY_MESSAGE_ACTION);
        intent.putExtra(EXTRA_MESSAGE, message);
        context.sendBroadcast(intent);
    }
}
