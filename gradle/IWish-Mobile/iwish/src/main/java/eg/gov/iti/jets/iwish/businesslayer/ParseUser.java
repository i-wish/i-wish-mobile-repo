package eg.gov.iti.jets.iwish.businesslayer;

import java.lang.reflect.Type;
import java.util.ArrayList;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import eg.gov.iti.jets.iwish.controller.UserController;
import eg.gov.iti.jets.iwish.dto.UserDTO;
import eg.gov.iti.jets.iwish.util.DataUtil;


public class ParseUser {

	Context context;

	public ParseUser() {
	}

	public ParseUser(Context context) {
		this.context = context;
	}

	public UserDTO getUser(String response) {

		Gson json = new GsonBuilder().setDateFormat("MMM dd, yyyy").create();
//		SharedPreferences pref = context.getSharedPreferences(
//				DataUtil.USER_PREF, 0);
//		Editor editor = pref.edit();
//		editor.putString(DataUtil.USER_JSON, response);
//		editor.commit();
		Type typeOfT = new TypeToken<UserDTO>() {}.getType();
		//String userJson = json.fromJson(response, UserDTO.class);
		UserDTO user=json.fromJson(response, UserDTO.class);
		return user;
	}

}
