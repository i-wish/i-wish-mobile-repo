package eg.gov.iti.jets.iwish.fragments;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.util.LruCache;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.commonsware.cwac.endless.EndlessAdapter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import eg.gov.iti.jets.iwish.activities.R;
import eg.gov.iti.jets.iwish.controller.ProductsController;
import eg.gov.iti.jets.iwish.controller.WishlistController;
import eg.gov.iti.jets.iwish.dto.Page;
import eg.gov.iti.jets.iwish.dto.ProductDTO;
import eg.gov.iti.jets.iwish.dto.UserDTO;
import eg.gov.iti.jets.iwish.util.DataUtil;

public class ProductsFragment extends Fragment {

	LinearLayout products_layout;
	GridView productsList;
	// LinearLayout progressLayout;
	ArrayList<ProductDTO> productDTOs;
	ArrayList<ProductDTO> searchProducts;
	ArrayList<Integer> wishListItems;
	UserDTO user;
	EditText productSearch;
	ListEndlessAdapter adapter;
	ProductDTO productDTO;
	ProgressDialog Pdialog;
	private LruCache<String, Bitmap> mMemoryCache;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		productDTOs = new ArrayList<ProductDTO>();
		SharedPreferences pref = getActivity().getSharedPreferences(
				DataUtil.USER_PREF, 0);
		String user_json = pref.getString(DataUtil.USER_JSON, null);
		Gson gson = new GsonBuilder().setDateFormat("MMM dd, yyyy").create();
		user = gson.fromJson(user_json, UserDTO.class);
		wishListItems = new ArrayList<Integer>();

		// Get max available VM memory, exceeding this amount will throw an
		// OutOfMemory exception. Stored in kilobytes as LruCache takes an
		// int in its constructor.
		final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);

		// Use 1/8th of the available memory for this memory cache.
		final int cacheSize = maxMemory / 8;

		mMemoryCache = new LruCache<String, Bitmap>(cacheSize) {
			@Override
			protected int sizeOf(String key, Bitmap bitmap) {
				// The cache size will be measured in kilobytes rather than
				// number of items.
				return bitmap.getByteCount() / 1024;
			}
		};
	}

	public void addBitmapToMemoryCache(String key, Bitmap bitmap) {
		if (getBitmapFromMemCache(key) == null) {
			mMemoryCache.put(key, bitmap);
		}
	}

	public Bitmap getBitmapFromMemCache(String key) {
		return mMemoryCache.get(key);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		products_layout = (LinearLayout) inflater.inflate(
				R.layout.products_fragment_layout, container, false);
		productsList = (GridView) products_layout
				.findViewById(R.id.product_listview);
		// progressLayout = (LinearLayout) products_layout
		// .findViewById(R.id.prod_progress_layout);

		productDTOs = (ArrayList<ProductDTO>) getActivity().getIntent()
				.getSerializableExtra(DataUtil.PRODUCTS);

		adapter = new ListEndlessAdapter(getActivity(), new ProductListAdapter(
				productDTOs), R.layout.progress_bar);
		productsList.setAdapter(adapter);

		productsList.setOnItemClickListener(new OnItemClickListener() {

			int index;

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				index = position;
				Log.i("product", "################"
						+ productDTOs.get(position).getName() + "  "
						+ productDTOs.get(position).getId());

				Intent i = getActivity().getIntent();
				i.putExtra(DataUtil.PRODUCTDETAILS, productDTOs.get(position));

				ProductDetailsFragment productDetailsFragment = new ProductDetailsFragment();
				getActivity()
						.getFragmentManager()
						.beginTransaction()
						.replace(R.id.frame_container, productDetailsFragment,
								DataUtil.CATEGORY_FRAGMENT_TAG)
						.addToBackStack("category fragment").commit();
			}

			// private void OnClickListener() {
			//
			// wishListItems.add(productDTOs.get(index).getId());
			// WishListAddTask wishListAddTask = new WishListAddTask();
			// wishListAddTask.execute(wishListItems);
			// }

		});

		return products_layout;
	}

	class GetProductsTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
		}
	}

	class ProductListAdapter extends BaseAdapter {

		List<ProductDTO> adapterList = new ArrayList<ProductDTO>();

		public ProductListAdapter(List<ProductDTO> productDTOs) {
			this.adapterList = productDTOs;
		}

		@Override
		public int getCount() {
            if (adapterList == null)
                return 0;

            return adapterList.size();
		}

		@Override
		public Object getItem(int position) {
			return adapterList.get(position);
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			final int index = position;

			ViewHolder viewHolder = null;

			if (convertView == null) {
				convertView = LayoutInflater.from(
						getActivity().getApplicationContext()).inflate(
						R.layout.product_list_item, parent, false);

				viewHolder = new ViewHolder(convertView);
				viewHolder.itemName = (TextView) convertView
						.findViewById(R.id.item_name);

				viewHolder.itemImage = (ImageView) convertView
						.findViewById(R.id.item_image);
				viewHolder.itemPrice = (TextView) convertView
						.findViewById(R.id.item_price);
				viewHolder.addWish = (Button) convertView
						.findViewById(R.id.add_item_to_wishlist);
				convertView.setTag(viewHolder);
			} else {
				viewHolder = (ViewHolder) convertView.getTag();
			}

			// if (adapterList.get(position).getImage() != null) {
			// Bitmap bitmap = BitmapFactory.decodeByteArray(
			// adapterList.get(position).getImage(), 0, adapterList
			// .get(position).getImage().length);
			// productImage.setImageBitmap(bitmap);
			//
			// }
			if (adapterList.get(position).getName().length() > 20) {
				viewHolder.itemName.setText(adapterList.get(position).getName()
						.substring(0, 10)
						+ "...");
			} else {
				viewHolder.itemName
						.setText(adapterList.get(position).getName());
			}
			viewHolder.itemName.setTextColor(Color.BLACK);

			final Bitmap bitmap = getBitmapFromMemCache(String
					.valueOf(adapterList.get(position).getId()));
			if (bitmap != null) {
				viewHolder.itemImage.setImageBitmap(bitmap);
			} else {
				viewHolder.itemImage.setImageResource(R.drawable.default_product);
				downloadImage(adapterList.get(position), viewHolder.itemImage);
			}
			viewHolder.itemPrice.setTextColor(Color.BLACK);
			viewHolder.itemPrice.setText(String.valueOf(adapterList.get(
					position).getPrice()));

			viewHolder.addWish.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {

					Pdialog = ProgressDialog.show(getActivity(), "",
							"Adding wish...");
					productDTO = productDTOs.get(index);
					wishListItems = new ArrayList<Integer>();
					wishListItems.add(productDTO.getId());
					WishListAddTask wishListAddTask = new WishListAddTask();
					wishListAddTask.execute(wishListItems);
				}
			});

			return convertView;
		}

		public void appendMoredata(List<ProductDTO> productDTOs) {
			Log.i("appendMoredata", "appendMoredata in");
			this.adapterList.addAll(productDTOs);
			notifyDataSetChanged();
		}

		public void downloadImage(ProductDTO productDTO, ImageView imageView) {

			GetProductImageTask getProductImageTask = new GetProductImageTask(
					imageView);
			getProductImageTask.execute(productDTO);
		}

	}

	static class ViewHolder {

		View rootView;
		public TextView itemName;
		public ImageView itemImage;
		public TextView itemPrice;
		public Button addWish;

		ViewHolder(View rootView) {
			this.rootView = rootView;
		}

	}

	class GetProductImageTask extends AsyncTask<ProductDTO, Void, Bitmap> {

		WeakReference<ImageView> imageViewReference;

		public GetProductImageTask(ImageView imageView) {
			imageViewReference = new WeakReference<ImageView>(imageView);
		}

		@Override
		protected Bitmap doInBackground(ProductDTO... params) {
			ProductDTO productDTO;
			try {
				ProductsController productsController = new ProductsController(
						getActivity().getApplicationContext());
				productDTO = productsController.getProductImage(params[0]);
				Bitmap bitmap = BitmapFactory.decodeByteArray(
						productDTO.getImage(), 0, productDTO.getImage().length);
				if (bitmap != null) {
					addBitmapToMemoryCache(String.valueOf(params[0].getId()),
							bitmap);
				}
				return bitmap;
			} catch (Exception ex) {
				ex.printStackTrace();
				productDTO = null;
			}
			return null;
		}

		@Override
		protected void onPostExecute(Bitmap result) {
			super.onPostExecute(result);

			if (imageViewReference != null) {
				ImageView imageView = imageViewReference.get();
				if (imageView != null) {
					if (result != null) {
						imageView.setImageBitmap(result);
					}
				}
			}
		}

	}

	class ListEndlessAdapter extends EndlessAdapter {

		private List<ProductDTO> tempList = new ArrayList<ProductDTO>();
		private int size = 10;
		private int patchStart = 0;
		private int patchSize = 10;
		private Page page;
		private int categoryId;
		private int companyId;
		private Context context;
		private int productsSize;

		public ListEndlessAdapter(Context context,
				ProductListAdapter productListAdapter, int pendingResource) {

			super(context, productListAdapter, pendingResource);
			productsSize = getActivity().getIntent().getIntExtra(
					DataUtil.PRODUCTSSIZE, 0);
			companyId = getActivity().getIntent().getIntExtra(
					DataUtil.COMPANYID, 0);
			categoryId = getActivity().getIntent().getIntExtra(
					DataUtil.CATEGORYID, 0);

			if (patchStart == 0) {
				patchStart = 10;
			}
		}

		@Override
		protected void appendCachedData() {

			Log.i("appendCachedData", "appendCachedData Out");
			if (getWrappedAdapter() != null) {
				Log.i("appendCachedData", "appendCachedData In");
				((ProductListAdapter) getWrappedAdapter())
						.appendMoredata(tempList);
			}
		}

		@Override
		protected boolean cacheInBackground() throws Exception {

			tempList = new ArrayList<ProductDTO>();
			ProductsController productsController = new ProductsController(
					getActivity().getApplicationContext());
			page = productsController.getProductDTOs(categoryId, companyId,
					patchStart, patchSize);

			if (page != null) {
				patchStart = page.getPatchStart() + page.getPatchSize();
                ArrayList<ProductDTO> products = (ArrayList<ProductDTO>) page.getProductDTOs();

                if( products!= null )
                tempList.addAll(products);

                size += tempList.size();
				if (tempList.size() == 0) {
					size++;
				}
				Log.i("tempList Size", "tempList " + tempList.size());
				Log.i("total Size", "products " + page.getSize());
				Log.i("Patch Size", "patch " + page.getPatchSize());
				Log.i("Patch Start", "patch start " + page.getPatchStart());
				return (size <= productsSize);
			} else {
				return false;
			}

		}

	}

	class WishListAddTask extends AsyncTask<ArrayList<Integer>, Void, Boolean> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			// ((HomeActivity)getActivity()).backFromServer = false;
		}

		@Override
		protected Boolean doInBackground(ArrayList<Integer>... params) {

			try {
				Boolean result = false;

				WishlistController wishlistController = new WishlistController(
						getActivity().getApplicationContext());
				ArrayList<ProductDTO> wishlist = wishlistController
						.readWishlist();

				// check if product is added before
				boolean productSaved = false;
				if (wishlist != null) {
					for (ProductDTO savedProduct : wishlist) {
						Log.i("saved wishes",
								"######## " + savedProduct.getName());
						for (int j = 0; j < params[0].size(); j++) {
							if (params[0].get(j).equals(savedProduct.getId())) {
								productSaved = true;
							}
						}
					}
				}

				if (!productSaved) {

					if (result = wishlistController.addToWishlist(params[0],
							user)) {
						// cache added products

						for (int i = 0; i < params[0].size(); i++) {
							if (params[0].get(i).equals(productDTO.getId())) {

								Log.i("added product", "" + productDTO.getId());
								if (wishlist == null) {
									wishlist = new ArrayList<ProductDTO>();
								}

								wishlist.add(productDTO);
							}
						}

						wishlistController.saveWishlist(wishlist);
					}
				}
				return result;
			} catch (Exception ex) {
				ex.printStackTrace();
				return false;
			}
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);

			Pdialog.dismiss();
			if (result) {
				Toast.makeText(getActivity().getApplicationContext(),
						"added to your wishList", Toast.LENGTH_LONG).show();
			} else {
				Toast.makeText(getActivity().getApplicationContext(),
						"couldn't add wish", Toast.LENGTH_LONG).show();
			}
			// ((HomeActivity)getActivity()).backFromServer = true;
		}

	}
}
