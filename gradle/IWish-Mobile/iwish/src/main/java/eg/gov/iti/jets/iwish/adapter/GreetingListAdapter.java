package eg.gov.iti.jets.iwish.adapter;

import java.util.List;

import eg.gov.iti.jets.iwish.activities.R;
import eg.gov.iti.jets.iwish.dto.EventCommentsDTO;
import eg.gov.iti.jets.iwish.dto.GreetingDTO;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Path;
import android.graphics.Rect;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class GreetingListAdapter extends ArrayAdapter<EventCommentsDTO> {

	Context context;
	List<EventCommentsDTO> greetingList;
	public GreetingListAdapter(Context context, int resource,
			List<EventCommentsDTO> greetingList) {
		super(context, resource, greetingList);
		this.context=context;
		this.greetingList=greetingList;
		
	}
	
	@Override
	public EventCommentsDTO getItem(int position) {
		// TODO Auto-generated method stub
		return greetingList.get(position);
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return greetingList.size();
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
LayoutInflater inflater=(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		convertView=inflater.inflate(R.layout.greeting_cell ,parent,false);
		
		
		TextView friend_name= (TextView) convertView
				.findViewById(R.id.friend_name);
		TextView comment = (TextView) convertView
				.findViewById(R.id.comment);
		ImageView friendImg = (ImageView) convertView
				.findViewById(R.id.friend_img);
		
		
		friend_name.setText(getItem(position).getUser().getName());
		comment.setText(getItem(position).getContent());
		//eventName.setTextColor(Color.BLACK);

		if (getItem(position).getUser().getPicture() != null) {
			Bitmap bitmap = BitmapFactory.decodeByteArray(
					getItem(position).getUser().getPicture(), 0,
					getItem(position).getUser().getPicture().length);
			friendImg.setImageBitmap(getRoundedShape(bitmap));
		}
		return convertView;
		
	}
	
	public Bitmap getRoundedShape(Bitmap scaleBitmapImage) {
		int targetWidth = 120;
		int targetHeight = 120;
		Bitmap targetBitmap = Bitmap.createBitmap(targetWidth, targetHeight,
				Bitmap.Config.ARGB_8888);

		Canvas canvas = new Canvas(targetBitmap);
		Path path = new Path();
		path.addCircle(((float) targetWidth - 1) / 2,
				((float) targetHeight - 1) / 2,
				(Math.min(((float) targetWidth), ((float) targetHeight)) / 2),
				Path.Direction.CCW);

		canvas.clipPath(path);
		Bitmap sourceBitmap = scaleBitmapImage;
		canvas.drawBitmap(sourceBitmap, new Rect(0, 0, sourceBitmap.getWidth(),
				sourceBitmap.getHeight()), new Rect(0, 0, targetWidth,
				targetHeight), null);
		return targetBitmap;
	}
	

}
