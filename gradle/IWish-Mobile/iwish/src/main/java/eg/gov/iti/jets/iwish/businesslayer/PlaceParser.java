package eg.gov.iti.jets.iwish.businesslayer;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import eg.gov.iti.jets.iwish.datalayer.WSConnection;
import eg.gov.iti.jets.iwish.dto.EventTypeDTO;
import eg.gov.iti.jets.iwish.dto.PlaceDTO;
import eg.gov.iti.jets.iwish.dto.UserEventDTO;
import eg.gov.iti.jets.iwish.util.DataUtil;

public class PlaceParser {

	

	public String getPlaces(EventTypeDTO type) {
		String GET_PLACES_URL = DataUtil.WS_URL+"place/get?eventTypeId=";
		WSConnection conn = new WSConnection();
		String out;
		if ((out = conn.get( GET_PLACES_URL
				+ type.getId())) != null) {
			return out;
		}
		return null;
	}

	public List<PlaceDTO> parsePlaces(String result) {
		try {
			Gson json = new Gson();
			Type listType = new TypeToken<List<PlaceDTO>>() {
			}.getType();
			ArrayList<PlaceDTO> placesList = json.fromJson(result, listType);
			return placesList;
		} catch (Exception ex) {
			return null;
		}

	}

}
