package eg.gov.iti.jets.iwish.activities;

import java.lang.reflect.Type;
import java.util.ArrayList;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

import eg.gov.iti.jets.iwish.dto.NotificationDTO;
import eg.gov.iti.jets.iwish.dto.UserDTO;
import eg.gov.iti.jets.iwish.util.DataUtil;
import com.google.android.gcm.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import eg.gov.iti.jets.iwish.activities.R;
import eg.gov.iti.jets.iwish.activities.Main_Page.GetFriendsTask;

import static eg.gov.iti.jets.iwish.activities.CommonUtilites.SENDER_ID;
import static eg.gov.iti.jets.iwish.activities.CommonUtilites.displayMessage;

public class GCMIntentService extends GCMBaseIntentService {

	private static notificationJson notif;
	private static final String TAG = "GCMIntentService";
	private static String recievedJson;
	ArrayList<notificationJson> notifJsons;
	int id;

	public GCMIntentService() {
		super(SENDER_ID);
		notifJsons = new ArrayList<GCMIntentService.notificationJson>();
		id = 0;
	}

	/**
	 * Method called on device registered
	 **/
	@Override
	protected void onRegistered(Context context, String registrationId) {
		Log.i(TAG, "Device registered: regId = " + registrationId);
		displayMessage(context, "Your device registred with GCM");
		// Log.d("NAME", MainActivity.name);
		notifJsons = new ArrayList<GCMIntentService.notificationJson>();
		SharedPreferences pref = getApplicationContext().getSharedPreferences(
				DataUtil.USER_PREF, 0);
		int userId = pref.getInt(DataUtil.USER_ID, 0);
		ServerUtilities.register(context, registrationId, userId);
	}

	/**
	 * Method called on device un registred
	 * */
	@Override
	protected void onUnregistered(Context context, String registrationId) {
		Log.i(TAG, "Device unregistered");
		displayMessage(context, getString(R.string.gcm_unregistered));
		SharedPreferences pref = getApplicationContext().getSharedPreferences(
				DataUtil.USER_PREF, 0);
		int userId = pref.getInt(DataUtil.USER_ID, 0);
		ServerUtilities.unregister(context, registrationId, userId);
	}

	/**
	 * Method called on Receiving a new message
	 * */
	@Override
	protected void onMessage(Context context, Intent intent) {
		Log.i(TAG, "Received message");

		Gson json = new GsonBuilder().setDateFormat("MMM dd, yyyy")
				.create();
		String message = intent.getExtras().getString("message");

		recievedJson = message;
		Log.i("notif", message + "");
		notif = json.fromJson(message, notificationJson.class);
		notifJsons.add(notif);
		String result = json.toJson(notifJsons);
		SharedPreferences pref = getSharedPreferences(
				DataUtil.NOTIFICATION_PREF, 0);
		Editor editor = pref.edit();
		editor.putString(DataUtil.NOTIFICATION_DATA, result);
		editor.commit();

		if (notif != null) {
			displayMessage(context, notif.message);
			// notifies user
			generateNotification(context, notif.message);
		}
	}

	/**
	 * Method called on receiving a deleted message
	 * */
	@Override
	protected void onDeletedMessages(Context context, int total) {
		Log.i(TAG, "Received deleted messages notification");
		String message = getString(R.string.gcm_deleted, total);
		displayMessage(context, message);
		// notifies user
		generateNotification(context, message);
	}

	/**
	 * Method called on Error
	 * */
	@Override
	public void onError(Context context, String errorId) {
		Log.i(TAG, "Received error: " + errorId);
		displayMessage(context, getString(R.string.gcm_error, errorId));
	}

	@Override
	protected boolean onRecoverableError(Context context, String errorId) {
		// log message
		Log.i(TAG, "Received recoverable error: " + errorId);
		displayMessage(context,
				getString(R.string.gcm_recoverable_error, errorId));
		return super.onRecoverableError(context, errorId);
	}

	/**
	 * Issues a notification to inform the user that server has sent a message.
	 */
	private static void generateNotification(Context context, String message) {
		int icon = R.drawable.logo_icon;
		long when = System.currentTimeMillis();
		NotificationManager notificationManager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);
		Notification notification = new Notification(icon, message, when);

		String title = context.getString(R.string.app_name);

		Intent notificationIntent = new Intent(context, HomeActivity.class);
		notificationIntent.putExtra(DataUtil.EVENT_ID, notif.eventId);
		// set intent so it does not start a new activity
//		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
//				| Intent.FLAG_ACTIVITY_SINGLE_TOP);
		PendingIntent intent = PendingIntent.getActivity(context, 0,
				notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);
		notification.setLatestEventInfo(context, title, message, intent);
		notification.flags |= Notification.FLAG_AUTO_CANCEL;

		// Play default notification sound
		notification.defaults |= Notification.DEFAULT_SOUND;

		// Vibrate if vibrate is enabled
		notification.defaults |= Notification.DEFAULT_VIBRATE;
		int notificationId = notif.notif_id; 
		notificationManager.notify(notificationId, notification);

	}

	class notificationJson {
		String message;
		int eventId;
		int notif_id;
	}


}
