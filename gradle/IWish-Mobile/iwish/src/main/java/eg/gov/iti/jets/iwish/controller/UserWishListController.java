package eg.gov.iti.jets.iwish.controller;

import java.util.ArrayList;

import eg.gov.iti.jets.iwish.asyncTasks.UserInfoTask;
import eg.gov.iti.jets.iwish.businesslayer.ParseWishlist;
import eg.gov.iti.jets.iwish.dto.ProductDTO;
import eg.gov.iti.jets.iwish.dto.UserDTO;

import eg.gov.iti.jets.iwish.util.DataUtil;

import android.content.Context;
import android.content.SharedPreferences;


public class UserWishListController {
	
	public Context context;
	
	
	public UserWishListController(Context context) {
		this.context = context;
	}
	
	
	public ArrayList<ProductDTO> getWishList(UserDTO userDTO){
		ParseWishlist parseWishlist = new ParseWishlist(context);
		ArrayList<ProductDTO> userWishlist = parseWishlist.getWishList(userDTO);
		
		if (userWishlist != null){
			//write in file
			WishlistController wishlistController = new WishlistController(context);
			wishlistController.saveWishlist(userWishlist);
		}
		return userWishlist;
	}
	


}
