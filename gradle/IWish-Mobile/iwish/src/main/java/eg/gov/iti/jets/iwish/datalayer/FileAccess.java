package eg.gov.iti.jets.iwish.datalayer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import eg.gov.iti.jets.iwish.dto.AbstractDTO;
import eg.gov.iti.jets.iwish.dto.NotificationDTO;
import eg.gov.iti.jets.iwish.dto.ProductDTO;
import eg.gov.iti.jets.iwish.dto.UserDTO;
import eg.gov.iti.jets.iwish.dto.UserEventDTO;
import eg.gov.iti.jets.iwish.util.DataUtil;

public class FileAccess {

	Context context;
	Properties prop;

	public FileAccess(Context context) {
		this.context = context;
		prop = new Properties();
	}

	public <T extends AbstractDTO> boolean writeToFile(List<T> data, String key) {

		
		String propertiesPath = context.getFilesDir().getPath().toString()
				+ "/app.properties";

		
		Gson json = new Gson();
		String jsonStr = json.toJson(data);

		try {
			FileOutputStream out = new FileOutputStream(propertiesPath, true);
			prop.setProperty(key, jsonStr);
			prop.store(out, null);
			out.close();

		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public <T extends AbstractDTO> List<T> readFromFile(Class<T> tClass,
			String key) {
		
		InputStream input = null;
		File file = new File(context.getFilesDir().getPath().toString()
				+ "/app.properties");

		try {
			input = new FileInputStream(file);
			if (input == null) {
				// Try loading from classpath
				input = getClass().getResourceAsStream(
						context.getFilesDir().getPath().toString()
								+ "/app.properties");
			}

			// Try loading properties from the file (if found)
			prop.load(input);
			String jsonStr = prop.getProperty(key);
			Log.i("json", " " + jsonStr);
			Gson json = new GsonBuilder().setDateFormat("MMM dd, yyyy")
					.create();
			Type typeOfT = null;

			if (key.equals(DataUtil.FRIENDS)) {
				typeOfT = new TypeToken<ArrayList<UserDTO>>() {
				}.getType();
			} else if (key.equals(DataUtil.WISHLIST)) {
				typeOfT = new TypeToken<ArrayList<ProductDTO>>() {
				}.getType();
			} else if (key.equals(DataUtil.EVENTS)){
				typeOfT = new TypeToken<ArrayList<UserEventDTO>>() {
				}.getType();
			} else if (key.equals(DataUtil.NOTIFICATIONS)){
				typeOfT = new TypeToken<ArrayList<NotificationDTO>>() {
				}.getType();
			}

			if (jsonStr != null) {
				return json.fromJson(jsonStr, typeOfT);
			}
			else {
				return null;
			}
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}
