package eg.gov.iti.jets.iwish.adapter;

import java.util.ArrayList;
import java.util.List;

import eg.gov.iti.jets.iwish.activities.R;
import eg.gov.iti.jets.iwish.adapter.ListAdapter.ListBean;
//import eg.gov.iti.jets.iwish.bean.ProductDTO;
//import eg.gov.iti.jets.iwish.bean.UserEventDTO;

import eg.gov.iti.jets.iwish.dto.UserEventDTO;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Path;
import android.graphics.Rect;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class EventListAdapter extends ArrayAdapter<UserEventDTO> {

	private Context context;
	private List<UserEventDTO> eventList;
	public boolean isFriendsEvent = false;

	public EventListAdapter(Context context, int resource,
			List<UserEventDTO> list) {
		super(context, resource, list);
		this.context = context;
		eventList = list;

	}

	public EventListAdapter(Context context, int resource,
			List<UserEventDTO> list, boolean isFriendsEvent) {
		super(context, resource, list);
		this.context = context;
		eventList = list;
		this.isFriendsEvent = isFriendsEvent;

	}

	@Override
	public UserEventDTO getItem(int position) {
		// TODO Auto-generated method stub
		return eventList.get(position);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		convertView = inflater.inflate(R.layout.event_item, parent, false);

		TextView eventName = (TextView) convertView
				.findViewById(R.id.event_name);
		TextView eventDate = (TextView) convertView
				.findViewById(R.id.event_date);
		ImageView eventImage = (ImageView) convertView
				.findViewById(R.id.event_img);
		TextView friendName = (TextView) convertView
				.findViewById(R.id.friend_name);
		ImageView friendImg = (ImageView) convertView
				.findViewById(R.id.friend_img);
		if (getItem(position).getDate() != null) {
			eventDate.setText(new DateFormat().format("dd-MM-yyyy",
					getItem(position).getDate()));
		}
		if (isFriendsEvent) {
			convertView.findViewById(R.id.friendEvent_layout).setVisibility(
					View.VISIBLE);
			if (getItem(position).getUserDTO().getPicture() != null) {
				Bitmap bitmap = BitmapFactory.decodeByteArray(getItem(position)
						.getUserDTO().getPicture(), 0, getItem(position)
						.getUserDTO().getPicture().length);
				friendImg.setImageBitmap(getRoundedShape(bitmap));
			}

			friendName.setText(getItem(position).getUserDTO().getName());
		}
		eventName.setText(getItem(position).getName());
		eventName.setTextColor(Color.BLACK);

		if (getItem(position).getImage() != null) {
			Bitmap bitmap = BitmapFactory.decodeByteArray(getItem(position)
					.getImage(), 0, getItem(position).getImage().length);
			eventImage.setImageBitmap(bitmap);
		}
		return convertView;

	}
	
	public Bitmap getRoundedShape(Bitmap scaleBitmapImage) {
		int targetWidth = 120;
		int targetHeight = 120;
		Bitmap targetBitmap = Bitmap.createBitmap(targetWidth, targetHeight,
				Bitmap.Config.ARGB_8888);

		Canvas canvas = new Canvas(targetBitmap);
		Path path = new Path();
		path.addCircle(((float) targetWidth - 1) / 2,
				((float) targetHeight - 1) / 2,
				(Math.min(((float) targetWidth), ((float) targetHeight)) / 2),
				Path.Direction.CCW);

		canvas.clipPath(path);
		Bitmap sourceBitmap = scaleBitmapImage;
		canvas.drawBitmap(sourceBitmap, new Rect(0, 0, sourceBitmap.getWidth(),
				sourceBitmap.getHeight()), new Rect(0, 0, targetWidth,
				targetHeight), null);
		return targetBitmap;
	}


}
