package eg.gov.iti.jets.iwish.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import eg.gov.iti.jets.iwish.activities.R;
import eg.gov.iti.jets.iwish.dto.CategoryDTO;

public class CategoriesAdapter extends ArrayAdapter<CategoryDTO> {

	Context context;
	ArrayList<CategoryDTO> categoryDTOs;

	public CategoriesAdapter(Context context, int resource,
			int textViewResourceId, List<CategoryDTO> objects) {
		super(context, resource, textViewResourceId, objects);
		this.context = context;
		this.categoryDTOs = (ArrayList<CategoryDTO>) objects;
	}

	public int getCount() {
		return categoryDTOs.size();
	}

	public CategoryDTO getItem(int position) {
		return categoryDTOs.get(position);
	}

	public long getItemId(int position) {
		return categoryDTOs.get(position).getId();
	}

	public void appendMoreData(List<CategoryDTO> categoryDTOs) {
		this.categoryDTOs.addAll(categoryDTOs);
		notifyDataSetChanged();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ListBean list = null;
		final int productIndex = position;
		final ViewGroup parentView = parent;
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater
					.inflate(R.layout.product_cell, parent, false);
			list = new ListBean(convertView);
			convertView.setTag(list);

		} else {
			list = (ListBean) convertView.getTag();
		}

		TextView productName = (TextView) convertView
				.findViewById(R.id.product_name);
		ImageView productImage = (ImageView) convertView
				.findViewById(R.id.product_image);
		Log.i("#######", "" + position);
		productName.setText(categoryDTOs.get(position).getName());
		productName.setTextColor(Color.BLACK);

		if (categoryDTOs.get(position).getImage() != null) {
			Bitmap bitmap = BitmapFactory.decodeByteArray(
					categoryDTOs.get(position).getImage(), 0,
					categoryDTOs.get(position).getImage().length);
			productImage.setImageBitmap(bitmap);
		}
		return convertView;
	}

	public class ListBean {

		View rowView;

		ListBean(View rowView) {
			this.rowView = rowView;
		}
	}
}
