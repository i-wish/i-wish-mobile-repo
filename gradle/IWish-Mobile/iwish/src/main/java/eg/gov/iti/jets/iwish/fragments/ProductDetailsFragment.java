package eg.gov.iti.jets.iwish.fragments;

import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import eg.gov.iti.jets.iwish.activities.HomeActivity;
import eg.gov.iti.jets.iwish.activities.R;
import eg.gov.iti.jets.iwish.controller.ProductsController;
import eg.gov.iti.jets.iwish.controller.WishlistController;
import eg.gov.iti.jets.iwish.dto.ProductDTO;
import eg.gov.iti.jets.iwish.dto.UserDTO;
import eg.gov.iti.jets.iwish.fragments.ProductsFragment.WishListAddTask;
import eg.gov.iti.jets.iwish.util.DataUtil;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

public class ProductDetailsFragment extends Fragment {

	LinearLayout productDetailsLayout;
	ProductDTO productDTO;
	ProductDTO productDTO2;
	TextView productName;
	ImageView productImage;
	TextView productPrice;
	TextView productDetails;
	ProgressDialog Pdialog;

	public boolean wishlist = false;
	ScrollView scroll;
	LinearLayout progressLayout;
	Button addWish;
	ArrayList<Integer> wishListItems;

	TextView productCompany;
	TextView productCategory;
	TextView productStoreownerName;
	TextView productStoreownerEmail;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		productDetailsLayout = (LinearLayout) inflater.inflate(
				R.layout.product_details_fragment, container, false);

		scroll = (ScrollView) productDetailsLayout
				.findViewById(R.id.scrollView1);
		progressLayout = (LinearLayout) productDetailsLayout
				.findViewById(R.id.details_progress_layout);

		productName = (TextView) productDetailsLayout
				.findViewById(R.id.item_details_name);
		productImage = (ImageView) productDetailsLayout
				.findViewById(R.id.item_details_image);
		productDetails = (TextView) productDetailsLayout
				.findViewById(R.id.item_details);
		productPrice = (TextView) productDetailsLayout
				.findViewById(R.id.item_details_price);
		addWish = (Button) productDetailsLayout.findViewById(R.id.add_wish);
		productCategory = (TextView) productDetailsLayout
				.findViewById(R.id.item_category);
		productCompany = (TextView) productDetailsLayout
				.findViewById(R.id.item_company);
		productStoreownerEmail = (TextView) productDetailsLayout
				.findViewById(R.id.item_storeowner_email);
		productStoreownerName = (TextView) productDetailsLayout
				.findViewById(R.id.item_storeowner_name);

		productDTO2 = (ProductDTO) getActivity().getIntent()
				.getSerializableExtra(DataUtil.PRODUCTDETAILS);
		productDTO = new ProductDTO();

		if (wishlist){
			addWish.setVisibility(View.GONE);
		}
		
		GetProductImage getProductImage = new GetProductImage();
		getProductImage.execute();

		addWish.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Pdialog = ProgressDialog.show(getActivity(),"", "Adding wish...");
				wishListItems = new ArrayList<Integer>();
				wishListItems.add(productDTO.getId());
				WishListAddTask wishListAddTask = new WishListAddTask();
				wishListAddTask.execute(wishListItems);
			}
		});

		return productDetailsLayout;
	}

	class WishListAddTask extends AsyncTask<ArrayList<Integer>, Void, Boolean> {

		@Override
		protected Boolean doInBackground(ArrayList<Integer>... params) {

			Boolean result = false;

			SharedPreferences pref = getActivity().getSharedPreferences(
					DataUtil.USER_PREF, 0);
			String user_json = pref.getString(DataUtil.USER_JSON, null);
			Gson gson = new GsonBuilder().setDateFormat("MMM dd, yyyy")
					.create();
			UserDTO user = gson.fromJson(user_json, UserDTO.class);

			WishlistController wishlistController = new WishlistController(
					getActivity().getApplicationContext());
			ArrayList<ProductDTO> wishlist = wishlistController.readWishlist();

			// check if product is added before
			boolean productSaved = false;
			if (wishlist != null) {
				for (ProductDTO savedProduct : wishlist) {
					Log.i("saved wishes", "######## " + savedProduct.getName());
					for (int j = 0; j < params[0].size(); j++) {
						if (params[0].get(j).equals(savedProduct.getId())) {
							productSaved = true;
						}
					}
				}
			}

			if (!productSaved) {

				if (result = wishlistController.addToWishlist(params[0], user)) {
					// cache added products

					for (int i = 0; i < params[0].size(); i++) {
						if (params[0].get(i).equals(productDTO.getId())) {

							Log.i("added product", "" + productDTO.getId());
							if (wishlist == null) {
								wishlist = new ArrayList<ProductDTO>();
							}

							wishlist.add(productDTO);
						}
					}

					wishlistController.saveWishlist(wishlist);
				}
			}
			return result;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			Pdialog.dismiss();

			if (result) {
				Toast.makeText(HomeActivity.HOME_CONTEXT,
						"Success", Toast.LENGTH_LONG).show();
			} else {
				Toast.makeText(getActivity().getApplicationContext(),
						"couldn't add wish", Toast.LENGTH_LONG).show();
			}
		}

	}

	class GetProductImage extends AsyncTask<Void, Void, String> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			((HomeActivity)getActivity()).backFromServer = false;
			progressLayout.setVisibility(productDetailsLayout.VISIBLE);
			scroll.setVisibility(productDetailsLayout.GONE);
		}

		@Override
		protected String doInBackground(Void... params) {

			ProductsController productsController = new ProductsController(
					getActivity().getApplicationContext());
			Log.i("$$$$$$$$", "   " + productDTO2.getId());
			productDTO = productsController.getProductImage(productDTO2);
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			progressLayout.setVisibility(productDetailsLayout.GONE);
			scroll.setVisibility(productDetailsLayout.VISIBLE);

			if (productDTO == null) {
				Toast.makeText(getActivity(), "Network Failed",
						Toast.LENGTH_LONG).show();
			} else {
				productName.setText(productDTO.getName());
				productCategory.setText("category:  "
						+ productDTO.getCategoryDTO().getName());
				productCompany.setText("company:  "
						+ productDTO.getCompanyDTO().getName());
				if (productDTO.getStoreOwnerDTO() != null) {
					productStoreownerEmail.setText(productDTO
							.getStoreOwnerDTO().getEmail());
					productStoreownerName.setText(productDTO.getStoreOwnerDTO()
							.getName());
				} else {
					productStoreownerName.setText("No Storeowner");
				}
				if (productDTO.getImage() != null) {
					Bitmap bitmap = BitmapFactory.decodeByteArray(
							productDTO.getImage(), 0,
							productDTO.getImage().length);
					productImage.setImageBitmap(bitmap);
				}
				productPrice.setText("price:  " + productDTO.getPrice() + "");
				if (productDTO.getDescription() != null)
					productDetails.setText("description:  "
							+ productDTO.getDescription());
				else 
					productDetails.setText("description:  no description");
			}
			
			((HomeActivity)getActivity()).backFromServer = true;
		}

	}

}
