package eg.gov.iti.jets.iwish.fragments;

import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.http.util.ByteArrayBuffer;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import eg.gov.iti.jets.iwish.activities.HomeActivity;
import eg.gov.iti.jets.iwish.activities.R;
import eg.gov.iti.jets.iwish.activities.R.id;
import eg.gov.iti.jets.iwish.activities.R.layout;
import eg.gov.iti.jets.iwish.controller.EventController;
import eg.gov.iti.jets.iwish.controller.PlaceController;
import eg.gov.iti.jets.iwish.dto.EventTypeDTO;
import eg.gov.iti.jets.iwish.dto.PlaceDTO;
import eg.gov.iti.jets.iwish.dto.UserEventDTO;

import android.R.plurals;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.YuvImage;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager.OnActivityResultListener;
import android.provider.MediaStore;
import android.text.format.Time;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

public class NewEventFragment extends Fragment {

	EditText name, location, date, time, desc, occasion;
	List<EventTypeDTO> eventTypes = new ArrayList<EventTypeDTO>();
	List<PlaceDTO> places = new ArrayList<PlaceDTO>();
	UserEventDTO event;
	// Date newDate;
	// Time newTime;
	Calendar calendar;
	SimpleDateFormat format;
	TextView done;
	ImageView img, ic_notif;

	boolean newEvent = true;
	static final int REQUEST_IMAGE_CAPTURE = 0;
	static final int REQUEST_IMAGE_GALLERY = 1;
	Context context;
	ProgressDialog pDilaog;

	
	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		done.setVisibility(View.GONE);
		ic_notif.setVisibility(View.VISIBLE);

	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		done.setVisibility(View.GONE);
		ic_notif.setVisibility(View.VISIBLE);
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		context = activity;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.activity_new_event,
				container, false);
		ic_notif = (ImageView) getActivity().findViewById(R.id.options);
		ic_notif.setVisibility(View.GONE);
		done = (TextView) getActivity().findViewById(R.id.Done);
		name = (EditText) rootView.findViewById(R.id.eventName);
		location = (EditText) rootView.findViewById(R.id.eventLocation);
		date = (EditText) rootView.findViewById(R.id.eventDate);
		time = (EditText) rootView.findViewById(R.id.eventTime);
		desc = (EditText) rootView.findViewById(R.id.eventDesc);
		occasion = (EditText) rootView.findViewById(R.id.eventType);
		img = (ImageView) rootView.findViewById(R.id.eventImg);

		done.setVisibility(View.VISIBLE);
		if (savedInstanceState != null) {
			event = (UserEventDTO) savedInstanceState.get("event");
			if (event.getImage() != null) {
				img.setImageBitmap(BitmapFactory.decodeByteArray(
						event.getImage(), 0, event.getImage().length));
			}
		}

		// getActivity().title.setText("Add Event");
		done.setVisibility(View.VISIBLE);
		calendar = Calendar.getInstance();

		// Bundle bundle = getIntent().getExtras();
		if (event != null) {
			newEvent = false;
			if (event.getImage() != null) {
				img.setImageBitmap(BitmapFactory.decodeByteArray(
						event.getImage(), 0, event.getImage().length));
			}
			name.setText(event.getName());
			occasion.setText(event.getEventTypeDTO().getName());
			if (event.getPlace() != null)
				location.setText(event.getPlace().getName());
			desc.setText(event.getDescription());
			if (event.getDate() != null) {
				date.setText(new SimpleDateFormat("yyyy-MM-dd").format(event
						.getDate()));
				calendar.setTime(event.getDate());
			}
			if (event.getTime() != null) {
				time.setText(new SimpleDateFormat("HH:mm").format(event
						.getDate()));
				calendar.set(calendar.HOUR_OF_DAY, event.getTime().getHours());
				calendar.set(calendar.MINUTE, event.getTime().getMinutes());

			}

		} else {

			event = new UserEventDTO();
			/*
			 * event.setDate(newDate); event.setTime(newDate); date.setText(new
			 * SimpleDateFormat("yyyy-MM-dd").format(event.getDate()));
			 * time.setText(new
			 * SimpleDateFormat("HH:mm").format(event.getDate()));
			 */
		}
		new getEventType().execute();

		done.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				
				
				event.setName(name.getText().toString());
				if (event.getName() == null || event.getName().equals("")
						|| event.getDate() == null || event.getTime() == null
						|| event.getEventTypeDTO() == null) {
					Toast.makeText(getActivity(), "Incomplete data",
							Toast.LENGTH_LONG).show();

				} else if (newEvent) {
					event.setDescription(desc.getText().toString());
					pDilaog=ProgressDialog.show(HomeActivity.HOME_CONTEXT,"", "wait...");
					
					new addEvent().execute();
				} else {
					event.setDescription(desc.getText().toString());
					pDilaog=ProgressDialog.show(HomeActivity.HOME_CONTEXT,"", "wait...");
					
					new editEvent().execute();
				}

			}
		});
		date.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				DatePickerDialog dialog = new DatePickerDialog(getActivity(),
						new OnDateSetListener() {

							@Override
							public void onDateSet(DatePicker view, int year,
									int monthOfYear, int dayOfMonth) {
								calendar.set(year, monthOfYear, dayOfMonth);
								format = new SimpleDateFormat("yyyy-MM-dd");
								date.setText(format.format(calendar.getTime()));
								event.setDate(calendar.getTime());

							}
						}, calendar.get(calendar.YEAR), calendar
								.get(calendar.MONTH), calendar
								.get(calendar.DATE));

				dialog.show();

			}
		});

		time.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				TimePickerDialog dialog = new TimePickerDialog(getActivity(),
						new OnTimeSetListener() {

							@Override
							public void onTimeSet(TimePicker view,
									int hourOfDay, int minute) {

								calendar.set(calendar.HOUR_OF_DAY, hourOfDay);
								calendar.set(calendar.MINUTE, minute);

								format = new SimpleDateFormat("HH:mm");
								time.setText(format.format(calendar.getTime()));
								event.setTime(calendar.getTime());

							}
						}, calendar.get(calendar.HOUR_OF_DAY), calendar
								.get(calendar.MINUTE), true);
				dialog.show();

			}
		});

		occasion.setOnClickListener(new OnClickListener() {
			String typesArr[];

			@Override
			public void onClick(View v) {
				typesArr = new String[eventTypes.size()];
				for (int i = 0; i < eventTypes.size(); i++) {
					typesArr[i] = eventTypes.get(i).getName();
				}
				AlertDialog.Builder builder = new AlertDialog.Builder(
						getActivity());
				builder.setTitle("Occassion")
						.setItems(typesArr,
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int which) {
										occasion.setText(typesArr[which]);
										if (eventTypes.get(which) != event
												.getEventTypeDTO()) {
											location.setText("");
											event.setPlace(null);
										}
										event.setEventTypeDTO(eventTypes
												.get(which));
									}
								}).create();
				builder.show();

			}
		});

		location.setOnClickListener(new OnClickListener() {
			String placesArr[];

			@Override
			public void onClick(View v) {
				if (event.getEventTypeDTO() == null) {
					Toast.makeText(getActivity(),
							"please choose occassion first", Toast.LENGTH_LONG)
							.show();
				} else {

					new getPlaces().execute();
				}

			}
		});

		img.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				String photoList[] = { "Upload photo", "Take photo" };

				AlertDialog.Builder builder = new AlertDialog.Builder(
						getActivity());
				builder.setItems(photoList,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								if (which == 0) {
									Intent galleryIntent = new Intent(
											Intent.ACTION_PICK,
											android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

									startActivityForResult(galleryIntent,
											REQUEST_IMAGE_GALLERY);

								} else if (which == 1) {
									Intent takePictureIntent = new Intent(
											MediaStore.ACTION_IMAGE_CAPTURE);
									if (takePictureIntent
											.resolveActivity(getActivity()
													.getPackageManager()) != null) {
										startActivityForResult(
												takePictureIntent,
												REQUEST_IMAGE_CAPTURE);
									}
								}
							}
						}).create();
				builder.show();

			}
		});

		return rootView;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		ic_notif.setVisibility(View.GONE);
		done.setVisibility(View.VISIBLE);
		if (requestCode == REQUEST_IMAGE_CAPTURE
				&& resultCode == getActivity().RESULT_OK) {
			Bundle extras = data.getExtras();
			Bitmap imageBitmap = (Bitmap) extras.get("data");
			img.setImageBitmap(imageBitmap);

			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			imageBitmap.compress(Bitmap.CompressFormat.JPEG, 70,
					byteArrayOutputStream);
			byte[] byteImage = byteArrayOutputStream.toByteArray();

			// int bytes = imageBitmap.getByteCount();
			// ByteBuffer buffer = ByteBuffer.allocate(bytes);
			// imageBitmap.copyPixelsToBuffer(buffer);
			// byte[] byteImage = buffer.array();

			event.setImage(byteImage);
		} else if (requestCode == REQUEST_IMAGE_GALLERY
				&& resultCode == getActivity().RESULT_OK && null != data) {
			// Get the Image from data

			Uri selectedImage = data.getData();
			String[] filePathColumn = { MediaStore.Images.Media.DATA };

			// Get the cursor
			Cursor cursor = getActivity().getContentResolver().query(
					selectedImage, filePathColumn, null, null, null);
			// Move to first row
			cursor.moveToFirst();

			int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
			String imgDecodableString = cursor.getString(columnIndex);
			cursor.close();

			img.buildDrawingCache();
			img.getDrawingCache().recycle();
			Bitmap imageBitmap = BitmapFactory.decodeFile(imgDecodableString);

			// int bytes = imageBitmap.getByteCount();
			//
			// ByteBuffer buffer = ByteBuffer.allocate(bytes);
			// imageBitmap.copyPixelsToBuffer(buffer);
			// byte[] byteImage = buffer.array();

			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

			imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100,
					byteArrayOutputStream);
			byte[] byteImage = byteArrayOutputStream.toByteArray();

			event.setImage(byteImage);
			img.setImageBitmap(imageBitmap);

		}
	}

	public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
		int width = image.getWidth();
		int height = image.getHeight();

		float bitmapRatio = (float) width / (float) height;
		if (bitmapRatio > 0) {
			width = maxSize;
			height = (int) (width / bitmapRatio);
		} else {
			height = maxSize;
			width = (int) (height * bitmapRatio);
		}
		return Bitmap.createScaledBitmap(image, width, height, true);
	}

	private class getEventType extends AsyncTask<Void, Void, String> {

		@Override
		protected String doInBackground(Void... params) {
			try {
				if (context == null) {
					Log.i("context", "nullllllllllll");
				} else {
					Log.i("context", "not nullllllllllll");
				}

				EventController controller = new EventController(context);
				return controller.getEventsType();
			} catch (Exception e) {
				return null;
			}

		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (result != null) {

				try {
					EventController controller = new EventController(
							getActivity());
					eventTypes = controller.ParseEventTypes(result);
				} catch (Exception ex) {
					Toast.makeText(getActivity(), "error loading data",
							Toast.LENGTH_LONG).show();
				}
			}

		}

	}

	private class addEvent extends AsyncTask<Void, Void, Boolean> {

		@Override
		protected void onPreExecute() {
			
			super.onPreExecute();
			//((HomeActivity)getActivity()).backFromServer = false;
			

		}
		
		@Override
		protected Boolean doInBackground(Void... params) {
			// TODO Auto-generated method stub
			try{
			EventController controller = new EventController(getActivity()
					.getApplicationContext());
			return controller.addEvent(event);
			}catch(Exception ex){
				ex.printStackTrace();
				return false;
			}

		}

		@Override
		protected void onPostExecute(Boolean result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			pDilaog.dismiss();
			
			if (result) {
				try{
					
					events evenstFragment =new events();
					

				EventDetailsFragment detailsFragment = new EventDetailsFragment();
				detailsFragment.isFriend = false;
				detailsFragment.event = event;

				getFragmentManager().popBackStack();
				android.app.FragmentManager fragmentManager = getFragmentManager();
				fragmentManager.beginTransaction()
						.replace(R.id.frame_container, evenstFragment)
						.addToBackStack(null).commit();
			}catch(Exception ex){
				ex.printStackTrace();
			}

			} else {
				Toast.makeText(HomeActivity.HOME_CONTEXT,
						"event hasn't been created",
						Toast.LENGTH_LONG).show();
			}
			
			//((HomeActivity)getActivity()).backFromServer = true;

		}

	}

	private class editEvent extends AsyncTask<Void, Void, Boolean> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			((HomeActivity)getActivity()).backFromServer = false;

		}
		
		@Override
		protected Boolean doInBackground(Void... params) {
			try {
				
				EventController controller = new EventController(getActivity());
				return controller.editEvent(event);
			} catch (Exception ex) {
				return false;
			}
		}

		@Override
		protected void onPostExecute(Boolean result) {
			pDilaog.dismiss();
			if (!NewEventFragment.this.isHidden()) {
				if (result) {

					try{
					EventDetailsFragment detailsFragment = new EventDetailsFragment();
					detailsFragment.isFriend = false;
					detailsFragment.event = event;
					getFragmentManager().popBackStack();
					getFragmentManager().popBackStack();
					android.app.FragmentManager fragmentManager = getFragmentManager();
					fragmentManager.beginTransaction()
							.replace(R.id.frame_container, detailsFragment)
							.addToBackStack(null).commit();

					/*
					 * Intent eventDetail = new Intent(getApplicationContext(),
					 * EventDetailsActivity.class); Gson json = new
					 * GsonBuilder().setDateFormat(
					 * "yyyy-MM-dd'T'HH:mm:ssZ").create(); String eventJson =
					 * json.toJson(event); eventDetail.putExtra("eventJson",
					 * eventJson); startActivity(eventDetail); //finish();
					 */
					}catch(Exception ex){
						ex.printStackTrace();
					}
				} else {
					Toast.makeText(HomeActivity.HOME_CONTEXT, "Failed to update event",
							Toast.LENGTH_LONG).show();
				}

			}
			//((HomeActivity)getActivity()).backFromServer = true;

		}

	}

	private class getPlaces extends AsyncTask<Void, Void, String> {
		String[] placesArr;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			//((HomeActivity)getActivity()).backFromServer = false;
		}
		
		@Override
		protected String doInBackground(Void... params) {
			try{
			PlaceController controller = new PlaceController(getActivity());
			return controller.getPlaces(event.getEventTypeDTO());
			}catch(Exception ex){
				ex.printStackTrace();
				return null;
			}

		}

		@Override
		protected void onPostExecute(String result) {

			super.onPostExecute(result);
			if (!NewEventFragment.this.isHidden()) {
				try {
					PlaceController controller = new PlaceController(
							getActivity());
					places = controller.parsePlaces(result);
					placesArr = new String[places.size()];
					for (int i = 0; i < places.size(); i++) {
						placesArr[i] = places.get(i).getName();
					}
					AlertDialog.Builder builder = new AlertDialog.Builder(
							getActivity());
					builder.setTitle("Location")
							.setItems(placesArr,
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog,
												int which) {
											location.setText(placesArr[which]);
											event.setPlace(places.get(which));
										}
									}).create();
					builder.show();
				} catch (Exception ex) {
					Toast.makeText(HomeActivity.HOME_CONTEXT, "couldn't retrieve locations",
							Toast.LENGTH_LONG).show();
				}

			}
			
			//((HomeActivity)getActivity()).backFromServer = true;

		}

	}
}
