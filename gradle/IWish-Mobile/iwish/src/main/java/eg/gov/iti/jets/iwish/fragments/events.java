package eg.gov.iti.jets.iwish.fragments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import eg.gov.iti.jets.iwish.activities.HomeActivity;
import eg.gov.iti.jets.iwish.activities.R;
import eg.gov.iti.jets.iwish.adapter.EventListAdapter;
import eg.gov.iti.jets.iwish.adapter.ListAdapter;
import eg.gov.iti.jets.iwish.controller.EventController;
import eg.gov.iti.jets.iwish.controller.FriendController;
import eg.gov.iti.jets.iwish.dto.UserDTO;
import eg.gov.iti.jets.iwish.dto.UserEventDTO;
import eg.gov.iti.jets.iwish.util.DataUtil;

import android.app.Fragment;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.R.color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;

public class events extends Fragment {

	public List<UserEventDTO> eventList;
	public List<UserEventDTO> eventsToDelete;
	public EventListAdapter adapter;

	private boolean selected;

	public events eventFragment;

	public ListView list;
	boolean isFriend = false;
	ListView wishlist;
	UserDTO user;
	Button reserve;
	ListView eventsList;
	ImageView ic_addEvent;
	LinearLayout noEventsLayout;
	//LinearLayout progressLayout;

	public events() {
		isFriend = false;
	}

	public events(boolean isFriend) {
		this.isFriend = isFriend;
	}

	public events(boolean isFriend, UserDTO user) {
		this.isFriend = isFriend;
		this.user = user;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.activity_event_list,
				container, false);

		eventList = new ArrayList<UserEventDTO>();
		eventsToDelete = new ArrayList<UserEventDTO>();
		list = (ListView) rootView.findViewById(R.id.eventList);
		noEventsLayout = (LinearLayout) rootView
				.findViewById(R.id.no_events_layout);
//		progressLayout = (LinearLayout) rootView
//				.findViewById(R.id.events_progress_layout);

		eventFragment = this;

		if (isFriend) {
			((HomeActivity) getActivity()).title.setText(user.getName());
			FriendEventsTask friendEventsTask = new FriendEventsTask();
			friendEventsTask.execute();
			list.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					EventDetailsFragment eventDetailsFragment = new EventDetailsFragment();
					eventDetailsFragment.event = eventList.get(position);
					eventDetailsFragment.isFriend = true;
					getFragmentManager()
							.beginTransaction()
							.replace(R.id.frame_container, eventDetailsFragment)
							.addToBackStack(null).commit();

					/*
					 * Intent eventDetail = new Intent(getActivity(),
					 * EventDetailsActivity.class);
					 * 
					 * Gson json = new GsonBuilder().setDateFormat(
					 * "yyyy-MM-dd'T'HH:mm:ssZ").create();
					 * 
					 * String eventJson = json.toJson(eventList.get(position));
					 * eventDetail.putExtra("eventJson", eventJson);
					 * eventDetail.putExtra("isFriend", true);
					 * startActivity(eventDetail);
					 */

				}
			});

		} else {
			((HomeActivity) getActivity()).title.setText("My Events");
			ic_addEvent = (ImageView) getActivity().findViewById(R.id.add);
			ic_addEvent.setVisibility(View.VISIBLE);
			ic_addEvent.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					NewEventFragment newEvent = new NewEventFragment();
					android.app.FragmentManager fragmentManager = getFragmentManager();
					fragmentManager.beginTransaction()
							.replace(R.id.frame_container, newEvent)
							.addToBackStack(null).commit();

				}
			});

			// EventController controller=new EventController(this);
			// controller.connectEventWS();
			new getEvents().execute();
			adapter = new EventListAdapter(getActivity()
					.getApplicationContext(), 0, eventList);
			list.setAdapter(adapter);

			/*
			 * list.setOnItemLongClickListener(new OnItemLongClickListener() {
			 * 
			 * @Override public boolean onItemLongClick(AdapterView<?> parent,
			 * View view, int position, long id) { // TODO Auto-generated method
			 * stub if (!selected) { selected = true; ((ProfileActivity)
			 * getActivity()).ic_delete .setVisibility(view.VISIBLE);
			 * ((ProfileActivity) getActivity()).ic_add
			 * .setVisibility(view.INVISIBLE);
			 * 
			 * } if (!eventsToDelete.contains(eventList.get(position))) {
			 * eventsToDelete.add(eventList.get(position));
			 * view.findViewById(R.id.select).setVisibility( view.VISIBLE);
			 * 
			 * } else { eventsToDelete.remove(eventList.get(position));
			 * view.findViewById(R.id.select).setVisibility( view.INVISIBLE); if
			 * (eventsToDelete.isEmpty()) { ((ProfileActivity)
			 * getActivity()).ic_delete .setVisibility(view.INVISIBLE);
			 * ((ProfileActivity) getActivity()).ic_add
			 * .setVisibility(view.VISIBLE); selected = false; } }
			 * 
			 * return true; }
			 * 
			 * });
			 * 
			 * list.setOnItemClickListener(new OnItemClickListener() {
			 * 
			 * @Override public void onItemClick(AdapterView<?> parent, View
			 * view, int position, long id) { // TODO Auto-generated method stub
			 * if (selected) { if
			 * (!eventsToDelete.contains(eventList.get(position))) {
			 * eventsToDelete.add(eventList.get(position));
			 * view.findViewById(R.id.select).setVisibility( view.VISIBLE);
			 * 
			 * } else { eventsToDelete.remove(eventList.get(position));
			 * view.findViewById(R.id.select).setVisibility( view.INVISIBLE); if
			 * (eventsToDelete.isEmpty()) { ((ProfileActivity)
			 * getActivity()).ic_delete .setVisibility(view.INVISIBLE);
			 * ((ProfileActivity) getActivity()).ic_add
			 * .setVisibility(view.VISIBLE); selected = false; }
			 * 
			 * } }else{ Intent eventDetail=new Intent(getActivity(),
			 * EventDetailsActivity.class);
			 * 
			 * Gson json= new
			 * GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").create();
			 * 
			 * String eventJson = json.toJson(eventList.get(position));
			 * eventDetail.putExtra("eventJson", eventJson);
			 * startActivity(eventDetail); } } });
			 */

			list.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {

					EventDetailsFragment detailsFragment = new EventDetailsFragment();
					detailsFragment.isFriend = false;
					detailsFragment.eventFragment = events.this;
					detailsFragment.event = eventList.get(position);

					android.app.FragmentManager fragmentManager = getFragmentManager();
					fragmentManager.beginTransaction()
							.replace(R.id.frame_container, detailsFragment)
							.addToBackStack(null).commit();

					// update selected item and title, then close the drawer

				}

			});

		}
		return rootView;

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	class FriendEventsTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			((HomeActivity) getActivity()).progressBar.setVisibility(View.VISIBLE);
			list.setVisibility(View.GONE);
		}

		@Override
		protected Void doInBackground(Void... params) {
			try {
				FriendController friendController = new FriendController(
						getActivity().getApplicationContext());
				eventList = friendController.getFriendEvents(user);
			} catch (Exception ex) {
				ex.printStackTrace();
				eventList = null;
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {

			((HomeActivity) getActivity()).progressBar.setVisibility(View.GONE);
			if (!events.this.isHidden() && !events.this.isRemoving()) {

				if (eventList == null) {
					Toast.makeText(HomeActivity.HOME_CONTEXT,
							"couldnt retrieve events", Toast.LENGTH_LONG)
							.show();
				} else if (eventList.size() == 0) {

					noEventsLayout.setVisibility(View.VISIBLE);
				}

				else if (eventList != null) {
					list.setVisibility(View.VISIBLE);
					adapter = new EventListAdapter(getActivity()
							.getApplicationContext(), 0, eventList);
					TextView friendName = new TextView(HomeActivity.HOME_CONTEXT);
					friendName.setText("EVENTS");
					
					//list.addHeaderView(friendName);
					

					list.setAdapter(adapter);
				}
			}
			// ((HomeActivity)getActivity()).backFromServer = true;
		}

	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		if (ic_addEvent != null) {
			ic_addEvent.setVisibility(View.GONE);
		}
		((HomeActivity) getActivity()).progressBar.setVisibility(View.GONE);
	}
	
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		if (ic_addEvent != null) {
			ic_addEvent.setVisibility(View.GONE);
		}
		((HomeActivity) getActivity()).progressBar.setVisibility(View.GONE);
	}

	class getEvents extends AsyncTask<Void, Void, String> {
		EventController controller = new EventController(getActivity());

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			((HomeActivity) getActivity()).progressBar.setVisibility(View.VISIBLE);
			list.setVisibility(View.GONE);
		}

		@Override
		protected String doInBackground(Void... params) {

			
			try {
				if (checkNetworkConnection()) {
					String result = controller.getEvents();
					eventList = controller.parseEvents(result);
					controller.saveEvents((ArrayList<UserEventDTO>) eventList,
							DataUtil.MY_EVENTS);
				} else {
					eventList = controller.readEvent(DataUtil.MY_EVENTS);
				}
			} catch (Exception ex) {
				ex.printStackTrace();
				eventList = null;
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {

			((HomeActivity) getActivity()).progressBar.setVisibility(View.GONE);
			if (!events.this.isRemoving() && !events.this.isHidden()) {
				try {
					if (eventList == null) {
						Toast.makeText(HomeActivity.HOME_CONTEXT,
								"couldn't retrieve events", Toast.LENGTH_LONG)
								.show();
					} else if (eventList.size() == 0) {
						noEventsLayout.setVisibility(View.VISIBLE);
					} else if (eventList != null) {
						list.setVisibility(View.VISIBLE);
						adapter = new EventListAdapter(getActivity(), 0,
								eventList);
						list.setAdapter(adapter);
					}
					// ((HomeActivity)getActivity()).backFromServer = true;
				} catch (Exception ex) {
					ex.printStackTrace();
					Toast.makeText(HomeActivity.HOME_CONTEXT,
							"couldn't retrieve events", Toast.LENGTH_LONG)
							.show();
				}
			}
		}

	}

	public boolean checkNetworkConnection() {
		ConnectivityManager manager = (ConnectivityManager) getActivity()
				.getApplicationContext()
				.getSystemService(
						getActivity().getApplicationContext().CONNECTIVITY_SERVICE);
		NetworkInfo wifiNetworkInfo = manager
				.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		NetworkInfo mobileNetworkInfo = manager
				.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		if (wifiNetworkInfo.isConnected() || mobileNetworkInfo.isConnected()) {
			return true;
		}
		return false;
	}

}
