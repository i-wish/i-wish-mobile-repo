package eg.gov.iti.jets.iwish.controller;

import java.util.List;

import eg.gov.iti.jets.iwish.businesslayer.PlaceParser;
import eg.gov.iti.jets.iwish.dto.EventTypeDTO;
import eg.gov.iti.jets.iwish.dto.PlaceDTO;
import eg.gov.iti.jets.iwish.util.DataUtil;

import android.content.Context;
import android.content.SharedPreferences;

public class PlaceController {
	
	

	Context context;
	public PlaceController(Context context) {
		this.context=context;
	}
	

	public String getPlaces(EventTypeDTO type) {
		PlaceParser parser=new PlaceParser();
		
		return parser.getPlaces(type);
		
	}

	public List<PlaceDTO> parsePlaces(String result) {
		PlaceParser parser=new PlaceParser();
		return parser.parsePlaces(result);
	}
	
	

}
