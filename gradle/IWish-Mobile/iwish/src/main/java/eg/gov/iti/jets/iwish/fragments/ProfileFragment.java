package eg.gov.iti.jets.iwish.fragments;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import eg.gov.iti.jets.iwish.activities.R;
import eg.gov.iti.jets.iwish.dto.UserDTO;
import eg.gov.iti.jets.iwish.util.DataUtil;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class ProfileFragment extends Fragment {

	ImageView profilePic;
	TextView userName;
	TextView userEmail;
	TextView userBirthday;
	SharedPreferences userPref;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_profile, container,
				false);
		profilePic = (ImageView) rootView.findViewById(R.id.profilePic);
		userName = (TextView) rootView.findViewById(R.id.userName);
		userEmail = (TextView) rootView.findViewById(R.id.userEmail);
		userBirthday = (TextView) rootView.findViewById(R.id.user_birthday);
		SharedPreferences userPref = getActivity().getSharedPreferences(
				DataUtil.USER_PREF, 0);
		String userJson = userPref.getString(DataUtil.USER_JSON, null);
		Gson gson = new GsonBuilder().setDateFormat("MMM dd, yyyy").create();
		UserDTO userDTO = gson.fromJson(userJson, UserDTO.class);

		userName.setText(userDTO.getName());
		if (userDTO.getEmail().substring(0, 1).matches("[0-9]+")) {
			userEmail.setText("");
			userEmail.setTextColor(Color.GRAY);
			userEmail.setTextSize(15);
		} else {
			userEmail.setText(userDTO.getEmail());
		}
		// userBirthday.setText(userDTO.getBirthday()+"");
		profilePic.setImageBitmap(BitmapFactory.decodeByteArray(
				userDTO.getPicture(), 0, userDTO.getPicture().length));
		return rootView;

	}

}
