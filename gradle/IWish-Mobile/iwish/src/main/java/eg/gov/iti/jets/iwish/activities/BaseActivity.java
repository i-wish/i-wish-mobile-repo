package eg.gov.iti.jets.iwish.activities;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class BaseActivity extends FragmentActivity {	
	
	public ImageView ic_notification,ic_add,ic_delete,ic_drawer;	
	private FrameLayout contentLayout;
	public TextView title;
	public ProgressBar progressBar;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		super.requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.setContentView(R.layout.activity_base);
		//testIcon = (ImageButton)findViewById(R.id.imageButton1);
		contentLayout = (FrameLayout) findViewById(R.id.base_activity_content);
		ic_add=(ImageView) findViewById(R.id.add);
		ic_notification=(ImageView) findViewById(R.id.options);
		ic_delete=(ImageView) findViewById(R.id.delete);
		ic_add.setVisibility(View.INVISIBLE);
		ic_delete.setVisibility(View.INVISIBLE);
		ic_drawer = (ImageView) findViewById(R.id.open_drawer);
		title = (TextView) findViewById(R.id.title);
		progressBar = (ProgressBar) findViewById(R.id.progressBar);
	}
	
	
	
	
	
	
	
	@Override
	public void setContentView(int layoutResID) {
		View content = LayoutInflater.from(this).inflate(layoutResID,
				contentLayout, false);
		contentLayout.addView(content);
	}

	@Override
	public void setContentView(View view) {
		contentLayout.addView(view);
	}

}
